
function paddingLeft(str, size) {
    str = str + "";
    if(str.length >= size)
        return str;
    else
        return paddingLeft("0" + str, size);
}

function updateVersion() {

    const redis = require('redis');
    const VERSION = "0.9";
    const STAGE = "alpha";

    const REDIS_HOST = "10.205.49.9";
    const REDIS_PORT = 6379;
    const client = redis.createClient(REDIS_PORT, REDIS_HOST);
    const KEY_VERSION = "silkbot-version-" + VERSION;
    // client.del(KEY_VERSION, function(err, value) {
    //     if (err) {
    //         console.error(err);
    //         return;
    //     }
    //     console.log(`Key ${KEY_VERSION} has been deleted!`);
    //     process.exit(0);
    // })
    client.incr(KEY_VERSION, function(err, value) {
        if (err) {
            console.error(err);
            return;
        }
        console.log('Increment build version:', value);

        const fs = require('fs');
        const today = new Date();
        const versionNum = `${VERSION}.${value}`;
        const versionInfo = `export const version = "v${VERSION}.${value}` +
            ` ${STAGE}` +
            ` ${today.getFullYear()}/${paddingLeft(today.getMonth()+1, 2)}/${paddingLeft(today.getDate(), 2)}` +
            ` ${paddingLeft(today.getHours(), 2)}:${paddingLeft(today.getMinutes(), 2)}:${paddingLeft(today.getSeconds(), 2)}";`;

        fs.writeFileSync("./src/version.js", versionInfo);
        process.exit(0);
    });
}

updateVersion();

