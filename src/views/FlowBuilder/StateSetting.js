import React from 'react';

import { Button, Dropdown, Input, Switch } from 'element-react';
import 'element-theme-default';
import LinearLayout from "./LinearLayout";
import { Logger } from "../../utils/Logger";
import { flowHelper } from "./FlowHelper";

import TransitionSetting from "./TransitionSetting";

class StateSetting extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("StateSetting");
    const {selectedState, serializedDiagram,  skillNodes, stateNodes} = this.props;

    const apiInfo = flowHelper.getAPIInfo(selectedState);

    this.state = {
      name: selectedState && selectedState.name ? selectedState.name : '',
      selectedState: selectedState,
      message: flowHelper.getNodeLabel(selectedState),
      url: apiInfo ? apiInfo.url:"",
      httpMethod: apiInfo ? apiInfo.httpMethod:"",
      httpBody: apiInfo ? apiInfo.httpBody:"",
      serializedDiagram,
      skillNodes,
      stateNodes,
      widget: flowHelper.getNodeInfoValue(selectedState, 'widget'),
      isStart: flowHelper.getNodeInfoValue(selectedState, 'isStart') === true,
      isEnd: flowHelper.getNodeInfoValue(selectedState, 'isEnd') === true,
    }
  }

  setup(props) {
    const { selectedState, serializedDiagram, skillNodes, stateNodes } = props;
    if (!selectedState) {
      this.setState({
        name: '',
        selectedState: undefined,
        message: '',
        serializedDiagram: undefined,
        skillNodes: [],
        stateNodes: [],
        widget: undefined,
        isStart: false,
        isEnd: false,
      });
      return;
    }

    this.setState({
      name: selectedState.name,
      selectedState: selectedState,
      message: flowHelper.getNodeLabel(selectedState),
      serializedDiagram,
      skillNodes,
      stateNodes,
      widget: flowHelper.getNodeInfoValue(selectedState, 'widget'),
      isStart: flowHelper.getNodeInfoValue(selectedState, 'isStart') === true,
      isEnd: flowHelper.getNodeInfoValue(selectedState, 'isEnd') === true,
    })
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setup(nextProps);
  }

  onMessageChanged(value) {

    const { selectedState } = this.state;

    flowHelper.setNodeLabel(selectedState, value);

    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }

    this.setState({
      message: value
    });
  }

  onAPIUrlChanged(value) {

    const { selectedState } = this.state;

    const apiInfo = flowHelper.getAPIInfo(selectedState);
    apiInfo["url"] = value;
    flowHelper.setAPIInfo(selectedState, apiInfo);

    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }

    this.setState({
      url: value
    });
  }

  onBodyChanged(value) {

    const { selectedState } = this.state;

    const apiInfo = flowHelper.getAPIInfo(selectedState);
    apiInfo["httpBody"] = value;
    flowHelper.setAPIInfo(selectedState, apiInfo);

    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }

    this.setState({
      httpBody: value
    });
  }

  removeState() {
    if (this.props.removeState) {
      this.props.removeState();
    }
  }

  onInputSwitchEnd() {
    const { selectedState, isEnd } = this.state;
    const value = !isEnd;
    flowHelper.setNodeInfo(selectedState, 'isEnd', value);
    this.setState({
      isEnd: value
    });
    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }
  }

  onInputSwitch() {
    const { selectedState, isStart, stateNodes } = this.state;
    const value = !isStart;

    if (value) {
      stateNodes.forEach(node => {
        flowHelper.setNodeInfo(node, "isStart", false);
      });
    }

    flowHelper.setNodeInfo(selectedState, 'isStart', value);

    this.setState({
      isStart: value
    });

    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }
  }

  renderSwitchStart() {

    const { selectedState, isStart } = this.state;
    this.logger.log("renderSwitchStart#selectedState", selectedState.name);

    return <LinearLayout orientation={'horizontal'} align={"left"} style={{ marginTop: "10px" }}>
      <div>设为开始状态:</div>
      <Switch
        onChange={this.onInputSwitch.bind(this)}
        value={isStart}
        onText=""
        offText="">
      </Switch>
    </LinearLayout>;
  }

  renderSwitchEnd() {

    const { selectedState, isEnd } = this.state;
    this.logger.log("renderSwitchEnd#selectedState", selectedState.name);

    return <LinearLayout orientation={'horizontal'} align={"left"} style={{ marginTop: "10px" }}>
      <div>设为结束状态:</div>
      <Switch
        onChange={this.onInputSwitchEnd.bind(this)}
        value={isEnd}
        onText=""
        offText="">
      </Switch>
    </LinearLayout>;
  }

  renderStateInfo() {
    const { selectedState } = this.state;

    const widgetType = flowHelper.getStateType(selectedState.id);
    this.logger.log("renderSetting#widgetType:", widgetType);

    return <LinearLayout orientation={'vertical'} align={'left'} style={{ background: "#cbc6d2", padding: "10px" }}>
      {this.renderSwitchStart()}
      {this.renderSwitchEnd()}
      <LinearLayout orientation={'horizontal'} align={"left"} style={{ marginTop: "10px" }}>
        {selectedState && <Button type={"danger"} onClick={this.removeState.bind(this)}>移除</Button>}
      </LinearLayout>
      <LinearLayout orientation={'horizontal'} align={"left"} style={{ marginTop: "10px", fontSize: "8pt" }}>
        {selectedState ? selectedState.id : ''}
      </LinearLayout>
    </LinearLayout>;
  }

  renderStateProps() {
    const { selectedState, message, widget } = this.state;
    this.logger.log("renderSetting#selectedState", selectedState.name);

    return <LinearLayout orientation={'vertical'}>
      <LinearLayout orientation={'horizontal'} align={"left"} style={{ marginTop: "10px" }}>
        <div style={{ width: "50px" }}>{widget.type === 'message' ? '提示' : '名称'}:</div><Input value={message} onChange={this.onMessageChanged.bind(this)} />
      </LinearLayout>
    </LinearLayout>;
  }

  handleHttpMethodSelection(httpMethod) {

    const { selectedState } = this.state;

    const apiInfo = flowHelper.getAPIInfo(selectedState);
    apiInfo["httpMethod"] = httpMethod;
    flowHelper.setAPIInfo(selectedState, apiInfo);

    this.setState({
      httpMethod
    });
  }

  renderHttpMethodOptions() {

    const { httpMethod } = this.state;
    return (
      <Dropdown style={{ zIndex: "1" }} onCommand={this.handleHttpMethodSelection.bind(this)} type="primary" trigger="click" menu={(
        <Dropdown.Menu>
          <Dropdown.Item command="GET">GET</Dropdown.Item>
          <Dropdown.Item command="POST">POST</Dropdown.Item>
        </Dropdown.Menu>
      )}
      >
        <Button style={{ width: "100px", marginLeft: "5px" }} type="primary">
          {httpMethod ? httpMethod : 'Method'}
          <i className="el-icon-caret-bottom el-icon--right"></i>
        </Button>
      </Dropdown>
    )
  }

  renderStatePropsForApiRunner() {
    const { url, widget, httpBody } = this.state;

    if (widget.type !== 'api') {
      return '';
    }

    return <LinearLayout orientation={'vertical'}>
      <LinearLayout orientation={'horizontal'} align={"left"} style={{ marginTop: "10px" }}>
        <div>Web API:</div><Input value={url} onChange={this.onAPIUrlChanged.bind(this)} />
        {this.renderHttpMethodOptions()}
        <div>BODY:</div><Input value={httpBody} onChange={this.onBodyChanged.bind(this)} />
      </LinearLayout>
    </LinearLayout>;
  }

  renderSetting() {

    const { selectedState, serializedDiagram, skillNodes, widget } = this.state;

    let showTransitionSetting = true;
    if (widget.type === 'message') {
      showTransitionSetting = true;
    }

    return <div style={{ maxHeight: '500px', width: '100%', overflowY: 'auto' }}><LinearLayout orientation={'vertical'} align={'left'} style={{ background: "#cbc6d2", padding: "10px", marginTop: "10px" }}>
      <LinearLayout orientation={'vertical'} align={"left"} style={{ marginTop: "0px" }}>
        {this.renderStateProps()}
        {this.renderStatePropsForApiRunner()}
        {showTransitionSetting && <TransitionSetting
          selectedSkill={this.props.selectedSkill}
          skills={skillNodes}
          intents={this.props.intents}
          entities={this.props.entities}
          apiResult={this.props.apiResult}
          forceUpdateView={this.props.forceUpdateView}
          selectedState={selectedState}
          serializedDiagram={serializedDiagram} />}
      </LinearLayout>
    </LinearLayout></div>;
  }

  render() {
    const { selectedState } = this.state;
    console.log("StateSetting# render", selectedState);
    if (!selectedState) {
      return '';
    }

    return <LinearLayout orientation={'vertical'} style={{ position: "relative", width: "100%", left: `0px`, overflow: "scroll" }} align={'top'}>
      {this.renderStateInfo()}
      {this.renderSetting()}
    </LinearLayout>
  }
}

export default StateSetting;
