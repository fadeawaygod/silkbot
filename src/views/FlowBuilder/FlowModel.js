import React from 'react';
import {
  DiagramWidget,
  DefaultNodeModel,
  DefaultPortModel,
} from 'storm-react-diagrams';

import Lodash from 'lodash';

import './srd.css';
import './css/flow.less';
import { flowHelper } from "./FlowHelper";
import StateComponent from "./StateComponent"
import { Logger } from "../../utils/Logger";
import robot_naughty from '@/assets/img/robot-naughty.svg';
import MissionIcon from './MissionIcon';
import AdjustmentIcon from './AdjustmentIcon';

class FlowModel extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("FlowModel");
    this.state = {
      engine: undefined,
      showStateLink: undefined,
      showStateLinkDict: {}
    };
    this.logger.debug("[TDEBUG] FlowModel# constructor");
  }

  componentDidMount() {
    this.logger.debug("[TDEBUG] FlowModel# componentDidMount");
    const { diagramData } = this.props;
    this.setup(diagramData);
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.logger.debug("[TDEBUG] FlowModel# componentWillReceiveProps");
    const { diagramData, diagramDataLink, skill } = nextProps;
    if (diagramData !== this.props.diagramData) {
      this.logger.debug("[TDEBUG] componentWillReceiveProps#", "setup");
      this.setup(diagramData)
    }
    if (diagramDataLink && this.props.diagramDataLink) {
      if (diagramDataLink.length !== this.props.diagramDataLink.length) {
        this.setup(diagramData)
      }
    }
    if (skill && !(skill.id in this.state.showStateLinkDict)) {
      this.setState(prevState => {
        let showStateLinkDict = Object.assign({}, prevState.showStateLinkDict);  // creating copy of state variable jasper
        showStateLinkDict[skill.id] = skill.info.showLink;                     // update the name property, assign a new value                 
        return { showStateLinkDict };                                // return new object jasper object
      })
    }

    // if (!diagramData) {
    //   this.setup(diagramData);
    // }
  }

  // componentDidUpdate() {
  //   if (this.props.diagramTag === "Skill") {
  //     this.sortNode(100)
  //   }
  //   // else {
  //   //   this.sortNode(220)
  //   // }
  // }

  setup(diagramData) {
    const engine = flowHelper.createDiagram(this.props.diagramTag, diagramData, {
      onNodeSelected: this.onNodeSelected.bind(this),
      onLinkSelected: this.onLinkSelected.bind(this),
      onNodeAdded: this.onNodeAdded.bind(this),
      forceUpdate: this.forceUpdate.bind(this),
      sourcePortChanged: this.sourcePortChanged.bind(this),
      targetPortChanged: this.targetPortChanged.bind(this),
      linksUpdated: this.linksUpdated.bind(this),
      nodesUpdated: this.nodesUpdated.bind(this),
    });

    if (this.props.init) {
      this.props.init(engine);
    }

    this.setState({ engine })
  }

  sourcePortChanged(link) {
    if (this.props.sourcePortChanged) {
      this.props.sourcePortChanged(link);
    }
  }

  targetPortChanged(link) {
    if (this.props.targetPortChanged) {
      this.props.targetPortChanged(link);
    }
  }

  linksUpdated(link, isAdded) {
    if (this.props.linksUpdated) {
      this.props.linksUpdated(link, isAdded);
    }
  }

  nodesUpdated(node, isAdded) {
    if (this.props.nodesUpdated) {
      this.props.nodesUpdated(node, isAdded);
    }
  }

  changeScale() {
    const { engine } = this.state;
    const { diagramData } = this.props;

    if (!engine) {
      return '';
    }
    if (diagramData) {
      engine.getDiagramModel().zoom = 100;
      engine.getDiagramModel().offsetX = 200 - diagramData.nodes[0].x;
      engine.getDiagramModel().offsetY = 150 - diagramData.nodes[0].y;
    }

    this.forceUpdate();
  }

  onNodeSelected(node, isSelected) {

    console.debug("FlowModel#onNodeSelected", node);
    if (isSelected) {
      this.currentSelectedNode = node;
      this.currentSelectedNodeX = node.x;
      this.currentSelectedNodeY = node.y;
    }

    this.onDiagramModelUpdated();

    if (this.props.onNodeSelected && this.state.engine) {
      this.props.onNodeSelected(node, isSelected, this.state.engine.getDiagramModel());
    } else {
      console.log(isSelected ? 'Selected' : 'Unselected', node);
    }
  }

  hasNode(x, y) {
    const { engine } = this.state;

    if (!engine) {
      return;
    }
    let diagramModel = engine.getDiagramModel()
    let nodes = Object.keys(diagramModel.nodes);
    for (let i = 0; i < nodes.length; i++) {
      if (diagramModel.nodes[nodes[i]].x === x && diagramModel.nodes[nodes[i]].y === y) {
        return diagramModel.nodes[nodes[i]].id
      }
    }
  }

  findNewNodePosition() {
    if (this.props.diagramTag !== "State") {
      return ''
    }
    const { engine } = this.state;

    if (!engine || Object.keys(engine.getDiagramModel().nodes).length == 0) {
      return [0, 0];
    }
    let nodes = Object.keys(engine.getDiagramModel().nodes)
    let len = nodes.length;
    let i = 0;
    let stateRow = 8;
    let gapX = 180
    let j = -1;
    for (i = 0; i < len + 1; i++) {
      if (i % stateRow == 0) {
        j += 1
      }
      if (i == len) {
        return [(i % stateRow) * gapX, 10 + j * 150];
      }
      if (this.hasNode((i % stateRow) * gapX, 10 + j * 150) === undefined) {
        return [(i % stateRow) * gapX, 10 + j * 150];
      }
    }
  }

  calcPosition(points) {
    // if (true) {
    //   return points;
    // }
    if (this.currentSelectedNode) {
      let tmpID = this.currentSelectedNode.id;
      let newX = Math.floor(Math.floor(points.x) / 200) * 200;
      let newY = 10 + (Math.floor(Math.floor(points.y) / 150) * 150);
      let oldNodeID = this.hasNode(newX, newY);
      if (oldNodeID !== undefined) {
        this.state.engine.getDiagramModel().nodes[oldNodeID].x = this.currentSelectedNodeX;
        this.state.engine.getDiagramModel().nodes[oldNodeID].y = this.currentSelectedNodeY;
      }
      this.state.engine.getDiagramModel().nodes[tmpID].x = newX;
      this.state.engine.getDiagramModel().nodes[tmpID].y = newY;
    }
  }

  onMouseDown(event) {
    let points = this.state.engine.getRelativeMousePoint(event);
    this.logger.log("FlowModel#onMouseDown# x, y:", points.x + ", " + points.y);
    this.mouseDownX = points.x;
    this.mouseDownY = points.y;
  }

  onMouseUp(event) {
    if (!this.state.engine) {
      return;
    }

    let points = this.state.engine.getRelativeMousePoint(event);

    this.logger.log("FlowModel#onMouseUp# x, y:", points.x + ", " + points.y);


    if (this.props.diagramTag === "State") {
      if (this.currentSelectedNode) {
        this.logger.log("FlowModel#onMouseUp# node:", this.currentSelectedNode);
        this.currentSelectedNode = undefined;
        if (this.props.onNodeMouseUp) {
          this.props.onNodeMouseUp(this.mouseDownX, this.mouseDownY, points.x, points.y);
        }
      }
    }
  }

  onDiagramModelUpdated() {
    if (!this.state.engine || !this.state.engine.getDiagramModel()) {
      return;
    }

    if (this.props.onDiagramModelUpdated) {
      this.props.onDiagramModelUpdated(this.state.engine.getDiagramModel().serializeDiagram());
    }
  }

  onLinkSelected(link, isSelected) {
    if (this.props.onLinkSelected) {
      this.props.onLinkSelected(link, isSelected);
    } else {
      console.log(isSelected ? 'Selected' : 'Unselected', link);
    }
  }

  onNodeClick(data) {
    const { engine } = this.state;

    if (!engine) {
      console.log("onNodeDrop", "engine is undefined!");
      return;
    }
    let node = undefined;

    if (this.props.onNodeDrop) {
      node = this.props.onNodeDrop(data.type, 0);
    } else {
      node = this.createNode(data.type);
    }

    let posArray = this.findNewNodePosition()
    node.x = posArray[0];
    node.y = posArray[1];

    engine.getDiagramModel().addNode(node);

    this.forceUpdate();
  }

  onNodeAdded(node) {
    if (this.props.onNodeAdded) {
      this.props.onNodeAdded(node);
    }
  }

  onNodeDrop(event) {
    event.preventDefault();
    console.log("onNodeDrop");

    const { engine } = this.state;

    if (!engine) {
      console.log("onNodeDrop", "engine is undefined!");
      return;
    }

    let data = JSON.parse(event.dataTransfer.getData('storm-diagram-node'))

    let node = undefined;

    if (this.props.onNodeDrop) {
      // let nodesCount = Lodash.keys(engine.getDiagramModel().getNodes()).length;
      node = this.props.onNodeDrop(data.type, 0);
    } else {
      node = this.createNode(data.type);
    }

    let points = engine.getRelativeMousePoint(event);
    node.x = points.x;
    node.y = points.y;
    engine.getDiagramModel().addNode(node);
    this.forceUpdate();
  }

  createNode(type) {
    let node = undefined;
    switch (type) {
      case 'skill':
        node = new DefaultNodeModel('Skill', 'peru');
        node.addPort(new DefaultPortModel(false, 'port-message', ' '));
        break;
      case 'message':
        node = new DefaultNodeModel('Message', 'peru');
        node.addPort(new DefaultPortModel(false, 'port-message', ' '));
        break;
      case 'input':
        node = new DefaultNodeModel('Input', 'hotpink');
        node.addPort(new DefaultPortModel(true, 'port-input', ' '));
        break;
      case 'nlu':
        node = new DefaultNodeModel('NLU', 'green');
        node.addPort(new DefaultPortModel(false, 'port-nlu', ' '));
        break;
      case 'decision':
        node = new DefaultNodeModel('Decision', 'blue');
        node.addPort(new DefaultPortModel(true, 'port-decision', ' '));
        break;
      case 'api':
        node = new DefaultNodeModel('API', 'orange');
        node.addPort(new DefaultPortModel(false, 'port-api', ' '));
        break;
    }
    return node;
  }

  onNodeDragOver(event) {
    event.preventDefault();
  }

  renderStateComponent() {
    if (this.props.diagramTag == 'State') {
      return <div>
        <StateComponent
          diagramDataNodes={this.state.engine.getDiagramModel()}>
        </StateComponent>
      </div>
    }
  }

  renderBar() {
    if (this.props.diagramTag == 'State') {
      return this.renderStateSettingBar()
    }
    else {
      return this.renderSkillSettingBar()
    }
  }

  updateShowSkillLink() {
    if (this.props.updateShowSkillLink) {
      this.props.updateShowSkillLink(this.props.skill.id, !this.state.showStateLinkDict[this.props.skill.id]);
      this.setState(prevState => {
        let showStateLinkDict = Object.assign({}, prevState.showStateLinkDict);  // creating copy of state variable jasper
        showStateLinkDict[this.props.skill.id] = !showStateLinkDict[this.props.skill.id];                     // update the name property, assign a new value                 
        return { showStateLinkDict };                                // return new object jasper object
      })
    }
  }

  renderSettings() {
    if (!this.props.skill)
      return ''
    return <AdjustmentIcon
      diagramData={this.props.diagramData}
      // skill={this.props.skill}
      showStateLink={this.state.showStateLinkDict[this.props.skill.id]}
      updateShowSkillLink={this.updateShowSkillLink.bind(this)}
      changeScale={this.changeScale.bind(this)}
    >
    </AdjustmentIcon>
  }

  renderSkillSettingBar() {
    let widgets = [{ type: 'skill' }];
    let widgetsUi = '';
    widgetsUi = widgets.map((widget, idx) => {
      return <MissionIcon
        key={idx}
        type={{ type: widget.type }}
      // onNodeClick={this.onNodeClick.bind(this)}
      />
    })
    return widgetsUi
  }

  renderStateSettingBar() {
    let widgets = [{ type: 'message' }, { type: 'input' }, { type: 'nlu' }, { type: 'api' }];
    let widgetsUi = '';
    widgetsUi = widgets.map((widget, idx) => {
      return <MissionIcon
        key={idx}
        type={{ type: widget.type }}
      // onNodeClick={this.onNodeClick.bind(this)}
      />
    })
    return widgetsUi
  }

  sortNode(gapY) {
    const { engine } = this.state;

    if (!engine || Object.keys(engine.getDiagramModel().nodes).length == 0) {
      return '';
    }
    let nodes = Object.keys(engine.getDiagramModel().nodes)
    let len = nodes.length;
    let i = 0;
    let tmp = nodes[i]
    let stateRow = 8;
    let gapX = 180
    let j = -1;
    for (i = 0; i < len; i++) {
      if (i % stateRow == 0) {
        j += 1
      }
      tmp = nodes[i];
      engine.getDiagramModel().nodes[tmp].x = (i % stateRow) * gapX;
      engine.getDiagramModel().nodes[tmp].y = 10 + j * gapY;
    }
  }

  onKeyDown(event) {
    if (!this.state.engine) {
      return;
    }
    let ctrl = event.ctrlKey ? event.ctrlKey : ((event.keyCode === 17) ? true : false);
    let cmd = event.metaKey ? event.metaKey : ((event.keyCode === 91) ? true : false);
    if (event.keyCode === 67 && (cmd || ctrl)) {
      this.props.copyState();
    }
    else if ((cmd || ctrl) && event.keyCode === 86) {
      if (this.props.onNodeAdded) {
        let node = this.props.onNodeCopy(this.props.statetoCopy);
        node.x = this.props.statetoCopy.x + 100;
        node.y = this.props.statetoCopy.y + 70;
        this.state.engine.getDiagramModel().addNode(node);
      }
      this.forceUpdate();
    }
  }


  render() {
    const { engine } = this.state;

    if (!engine) {
      return '';
    }

    this.logger.debug("[TDEBUG] FlowModel#", "render");

    return <div className="fx fdc" style={{ width: "100%", flexGrow: "1" }}>
      <div className="header-hint vtb">
        <img src={robot_naughty} style={{ height: '45px' }} />
        <span className="hint-text">
          <div className="main-text">{'状态设定'}</div>
          <div className="sub-text">{'请用滑鼠点击右方图示，以便开始编辑任务！'}</div>
        </span>
        <span className="hint-text">
          {this.renderBar()}
          {this.renderSettings()}
        </span>
      </div>

      <div className="diagram-layer _fill h100 fx fdc"
        style={{ flexGrow: "1" }}
        onMouseUp={this.onMouseUp.bind(this)}
        onMouseDown={this.onMouseDown.bind(this)}
        onDrop={this.onNodeDrop.bind(this)}
        onDragOver={this.onNodeDragOver.bind(this)}
        onKeyDown={(e) => this.onKeyDown(e)}
        tabIndex="-1">
        <DiagramWidget deleteKeys={[]} smartRouting={true} diagramEngine={engine} />
      </div>

      {this.renderStateComponent()}
    </div>
  }
}

export default FlowModel;
