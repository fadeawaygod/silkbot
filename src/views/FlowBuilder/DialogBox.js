import React from 'react';

import {Button, Input, Dropdown, Dialog, Switch} from 'element-react';
import 'element-theme-default';
import {Logger} from "../../utils/Logger";
import {flowHelper} from "./FlowHelper";

class DialogBox extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("DialogBox Created!");
    this.state = {
      content:''
    }
  }

  componentDidMount() {
    const config = flowHelper.buildBot(this.props.botId, this.props.flow);
    this.setState({
      content: JSON.stringify(config)
    })
  }

  onCancel() {
    this.props.closeDialog();
  }

  render() {
    return <Dialog
        title={this.props.title}
        size="tiny"
        visible={ this.props.visible }
        onCancel={this.onCancel.bind(this)}
        lockScroll={ false }
    >
      <Dialog.Body>
        <div style={{minHeight: "400px", maxHeight: "600px", overflow: "scroll"}}>
        {this.state.content}
        </div>
      </Dialog.Body>
    </Dialog>
  }
}

export default DialogBox;
