import React from 'react';

import { Button, Input, Dropdown, Dialog, Switch } from 'element-react';
import 'element-theme-default';
import { Logger } from "../../utils/Logger";
import { flowHelper } from "./FlowHelper";
import LinearLayout from "./LinearLayout";

class DialogProgress extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("DialogProgress Created!");
    this.state = {
      content: '储存中... 请稍候'
    }
  }

  onCancel() {
    this.props.closeDialog();
  }

  render() {
    return <Dialog
      size="tiny"
      visible={this.props.visible}
      onCancel={this.onCancel.bind(this)}
      lockScroll={false}
      showClose={false}
      title={this.props.title}
    >
      <Dialog.Body>
        <LinearLayout orientation={'horizontal'} align={"center"}>
          <div>
            {this.state.content}
          </div>
          <i className="el-icon-loading" style={{ marginLeft: "10px" }} />
        </LinearLayout>
      </Dialog.Body>
    </Dialog>
  }
}

export default DialogProgress;
