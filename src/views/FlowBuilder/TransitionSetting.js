import React from 'react';

import { Button, Input, Dropdown, Popover, Switch } from 'element-react';
import 'element-theme-default';
import LinearLayout from "./LinearLayout";
import { Logger } from "../../utils/Logger";
import { Widgets } from "./data/StateWidgets";
import { flowHelper } from "./FlowHelper";
import DialogSelector from "./DialogSelector";
import IntentDialogSelector from "./IntentDialogSelector";
import SlotDialogSelector from "./SlotDialogSelector"
import IntentAPIDialogSelector from "./IntentAPIDialogSelector"

class TransitionSetting extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("TransitionSetting");
    const { selectedState, serializedDiagram, skills, selectedSkill } = props;

    this.prepareTransitions(serializedDiagram, selectedState, skills[0]);

    this.state = {
      skills,
      selectedState,
      serializedDiagram,
      sessionManager: skills[0],
      conditionType: undefined,
      conditionValue: '',
      entityKey: '',
      next_state: undefined,
      next_skill: undefined,
      withInput: flowHelper.getNodeInfoValue(selectedState, 'withInput') ? flowHelper.getNodeInfoValue(selectedState, 'withInput') : false,
      transitions: flowHelper.getTransitions(selectedState),
      showDialog: false,
      showIntentDialog: false,
      showSlotDialog: false,
      showIntentAPIDialog: false,
      selectedSkill,
    };
  }

  createConditions(transitions) {
    this.logger.log("createConditions#", transitions);
    if (!transitions) {
      return [];
    }

    const conditions = [];
    transitions.forEach(transition => {
      conditions.push({ next_state: transition.next_state, next_skill: transition.next_skill, operation: '', value: '' })
    });

    return conditions;
  }

  setup(props) {
    const { selectedState, serializedDiagram, skills, selectedSkill } = props;

    this.state = {
      skills,
      sessionManager: skills[0],
      selectedState,
      serializedDiagram,
      conditionType: undefined,
      conditionValue: '',
      entityKey: '',
      withInput: flowHelper.getNodeInfoValue(selectedState, 'withInput') ? flowHelper.getNodeInfoValue(selectedState, 'withInput') : false,
      transitions: flowHelper.getTransitions(selectedState),
      showDialog: false,
      showIntentDialog: false,
      showSlotDialog: false,
      showIntentAPIDialog: false,
      selectedSkill
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.logger.log("componentWillReceiveProps#");
    this.setup(nextProps);
  }

  prepareTransitions(serializedDiagram, state, sessionManager) {

    this.logger.log("prepareTransitions#serializedDiagram:", serializedDiagram);
    this.logger.log("prepareTransitions#sessionManager:", sessionManager);
    this.logger.log("prepareTransitions#state:", state);

    if (!serializedDiagram || !state || !sessionManager) {
      return;
    }

    let transitions = flowHelper.getNodeInfoValue(state, "transitions");

    if (!transitions) {
      transitions = [];
      flowHelper.setNodeInfo(state, "transitions", transitions);
    }

    if (transitions.length > 0) {
      return transitions;
    }

    const links = flowHelper.getLinks(serializedDiagram, state);

    this.logger.log("prepareTransitions#links:", links);

    if (links && links.length > 0) {
      links.forEach(link => {
        transitions.push({ conditions: [{ operation: "intent", value: "unknown" }], next_state: link.target })
      })
    } else {
      transitions.push({ conditions: [{ operation: "intent", value: "unknown" }], next_skill: "session-manager" })
    }
    this.logger.log("prepareTransitions#transitions:", transitions);

    flowHelper.setNodeInfo(state, "transitions", transitions);
  }

  onInputConditionValue(value) {
    this.logger.log("onInputConditionValue# value:", value);
    this.setState({
      conditionValue: value,
    });
  }

  onInputEntityKey(value) {
    this.logger.log("onInputEntityKey# value:", value);
    this.setState({
      entityKey: value,
    });
  }

  onAddCondition() {

    const { operation, conditionValue, next_state, sessionManager, selectedState, entityKey } = this.state;
    let next_skill = this.state.next_skill === sessionManager.id ? 'session-manager' : this.state.next_skill;

    if (!next_skill && !next_state) {
      alert("請先選擇轉移狀態或任務");
      return;
    }

    if (!operation) {
      alert("請先選擇條件類型");
      return;
    }

    if (conditionValue === undefined) {
      alert("請先輸入條件值");
      return;
    }

    const condition = operation === 'reset' ? {
      operation: 'intent',
      value: "ok"
    } : {
      operation,
      value: conditionValue
    };

    this.addCondition(condition);
  }

  saveUpdatedTransitions(transitions) {

    this.logger.log("save#transitions#", transitions);

    flowHelper.setTransitions(this.state.selectedState, transitions);
    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }
    this.setState({
      transitions: JSON.parse(JSON.stringify(transitions)),
      next_skill: undefined,
      next_state: undefined,
      entityKey: '',
    })
  }

  addConditions(conditions) {

    const { next_state, sessionManager, } = this.state;
    let next_skill = this.state.next_skill === sessionManager.id ? 'session-manager' : this.state.next_skill;

    if (!next_skill && !next_state) {
      alert("請先選擇轉移狀態或任務");
      return;
    }

    if (!conditions) {
      return;
    }
    conditions.forEach(condition => {
      this.addCondition(condition);
    })
  }

  addCondition(condition) {
    const { next_state, sessionManager, selectedState, entityKey } = this.state;
    let next_skill = this.state.next_skill === sessionManager.id ? 'session-manager' : this.state.next_skill;

    if (next_state) {
      next_skill = undefined;
    }

    if (!next_skill && !next_state) {
      alert("請先選擇轉移狀態或任務");
      return;
    }

    if (!condition.operation) {
      alert("請先選擇條件類型");
      return;
    }

    if (!condition.value) {
      alert("請先輸入條件值");
      return;
    }

    const transitions = flowHelper.getTransitions(selectedState) ? flowHelper.getTransitions(selectedState) : [];

    let foundTransition = transitions.find(t => {
      this.logger.debug("addCondition# transition:", t);
      return t.next_state === next_state && t.next_skill === next_skill
    });

    this.logger.debug("addCondition#foundTransition", foundTransition);

    if (foundTransition) {
      if (!foundTransition.conditions) {
        foundTransition["conditions"] = [];
      }
      if (entityKey) {
        foundTransition["entity"] = entityKey;
      }
      foundTransition.conditions.push(condition);
    } else {
      const transition = {
        entity: entityKey ? entityKey : undefined,
        next_state: next_state,
        next_skill: next_skill,
        conditions: [condition]
      };
      transitions.push(transition);
    }

    this.saveUpdatedTransitions(transitions);
  }

  handleOperation(operation) {

    const { next_state, sessionManager } = this.state;
    let next_skill = this.state.next_skill === sessionManager.id ? 'session-manager' : this.state.next_skill;

    if (!next_skill && !next_state) {
      alert("請先選擇轉移狀態或任務");
      return;
    }

    this.setState({
      operation
    });

    if (operation === 'intent') {
      this.showDialog('intent', true)
    }

    if (operation === 'slot') {
      this.showDialog('slot', true)
    }

    if (operation === 'intent_API') {
      this.showDialog('intent_API', true)
    }
  }

  getOperationText(operation) {
    let matchOperation = '';
    switch (operation) {
      case 'reset':
        matchOperation = '重新開始';
        break;
      case 'intent':
        matchOperation = '意圖等於';
        break;
      case 'intent_API':
        matchOperation = 'API等於';
        break;
      case 'slot':
        matchOperation = '插槽等於';
        break;
      case 'equal':
        matchOperation = '等於';
        break;
      case 'includes':
        matchOperation = '包含';
        break;
      case 'regex':
        matchOperation = '正規表示';
        break;
    }
    return matchOperation;
  }

  renderOperationOptions() {

    const { operation } = this.state;
    const matchOperation = this.getOperationText(operation);

    return (
        <Dropdown onCommand={this.handleOperation.bind(this)} type="primary" trigger="click" menu={(
            <Dropdown.Menu>
              <Dropdown.Item command="intent">{this.getOperationText('intent')}</Dropdown.Item>
              <Dropdown.Item command="intent_API">{this.getOperationText('intent_API')}</Dropdown.Item>
              <Dropdown.Item command="slot">{this.getOperationText('slot')}</Dropdown.Item>
              <Dropdown.Item command="equal">{this.getOperationText('equal')}</Dropdown.Item>
              <Dropdown.Item command="includes">{this.getOperationText('includes')}</Dropdown.Item>
              <Dropdown.Item command="regex">{this.getOperationText('regex')}</Dropdown.Item>
            </Dropdown.Menu>
        )}
        >
          <Button style={{ width: "100px" }} type="primary">
            {matchOperation ? matchOperation : '匹配方式'}
            <i className="el-icon-caret-bottom el-icon--right"></i>
          </Button>
        </Dropdown>
    )
  }

  findSkill(id) {

    const { skills, sessionManager } = this.state;
    if (id === 'session-manager') {
      return sessionManager;
    }

    if (!skills) {
      return undefined;
    }

    return skills.find(skills => skills.id === id);
  }

  renderSkillOptions(skills) {

    const { next_skill } = this.state;
    const skill = this.findSkill(next_skill);

    const items = [{
      isState: false,
      selectedNode: undefined,
      node: undefined,
      content: `移除`
    }];

    skills.map((s, idx) => {
      items.push({
        isState: false,
        selectedNode: skill,
        node: s, content: `${idx + 1}. ${s.name}`
      });
    });

    return <Button style={{ width: "300px", marginLeft: "5px" }} type="info" onClick={this.showSelectDialog.bind(this, items)}>
      {skill ? skill.name : '選擇任務'}
      <i className="el-icon-caret-bottom el-icon--right"></i>
    </Button>
  }

  showSelectDialog(items) {
    this.setState({
      selectItems: items,
    });

    this.showDialog('', true);
  }

  showDialog(id, shown) {
    switch (id) {
      case '':
        this.setState({
          showDialog: shown,
        });
        break;
      case 'intent':
        this.setState({
          showIntentDialog: shown,
        });
        break;
      case 'slot':
        this.setState({
          showSlotDialog: shown,
        });
        break;
      case 'intent_API':
        this.setState({
          showIntentAPIDialog: shown,
        });
        break;
    }
  }

  onSelected(item) {
    this.logger.log("onSelected# item", item);

    if (!item || !item.node) {
      if (item.isState) {
        this.setState({
          next_state: undefined,
          showDialog: false,
        })
      } else {
        this.setState({
          next_skill: undefined,
          showDialog: false,
        })
      }
      return;
    }

    if (item.isState) {
      this.setState({
        next_state: item.node.id,
        showDialog: false,
      })
    } else {
      this.setState({
        next_skill: item.node.id,
        showDialog: false,
      })
    }
  }

  renderStateOptions(states) {

    const { next_state, next_skill, selectedSkill, serializedDiagram } = this.state;

    if (!next_skill || next_skill !== selectedSkill.id) {
      return;
    }

    const state = flowHelper.findNode(serializedDiagram, next_state);

    const items = [{
      isState: true,
      selectedNode: undefined,
      node: undefined,
      content: `移除`
    }];

    states.map((s, idx) => {
      items.push({
        isState: true,
        selectedNode: state,
        node: s,
        content: `${idx + 1}. ${s.name}: ${s.ports && s.ports.length > 0 ? s.ports[0].label : ''}`
      });
    });

    return <Button style={{ width: "300px", marginTop: "5px", marginLeft: "5px" }} type="primary" onClick={this.showSelectDialog.bind(this, items)}>
      {state ? `${state.name} ${state.ports && state.ports.length > 0 ? state.ports[0].label : ''}` : '選擇狀態'}
      <i className="el-icon-caret-bottom el-icon--right"></i>
    </Button>
  }

  renderAddCondition() {

    const { skills, serializedDiagram, conditionValue, entityKey, next_skill, next_state } = this.state;

    const states = serializedDiagram && serializedDiagram.nodes ? serializedDiagram.nodes : [];

    return <LinearLayout orientation={'vertical'} style={{ position: "relative", width: "100%", left: `0px` }} align={'left'}>
      <div style={{ marginTop: "10px" }}>
        状态转移到:
      </div>
      <div style={{ marginTop: "10px" }}>
        <div>
          {this.renderSkillOptions(skills)}
        </div>
        <div>
          {this.renderStateOptions(states)}
        </div>
      </div>

      {(next_skill || next_state) && <div style={{ display: "flex", marginTop: "10px" }}>
        {this.renderOperationOptions()}
        <Input style={{ marginLeft: "5px", width: "150px" }} value={conditionValue}
               onChange={this.onInputConditionValue.bind(this)} />
        <Input style={{ marginLeft: "5px", width: "150px" }} value={entityKey}
               onChange={this.onInputEntityKey.bind(this)} />
      </div>
      }
      {(next_skill || next_state) && <div>
        <Button style={{ marginLeft: "5px", marginTop: "10px" }} onClick={this.onAddCondition.bind(this)}>新增</Button>
      </div>
      }
    </LinearLayout>
  }

  renderTransitions() {
    const { selectedState, serializedDiagram } = this.state;

    const transitions = flowHelper.getTransitions(selectedState);

    if (!transitions) {
      return '';
    }

    return (
        transitions && transitions.map((transition, idx) => {
          const state = flowHelper.findNode(serializedDiagram, transition.next_state);
          const skill = this.findSkill(transition.next_skill);
          this.logger.log("renderTransitions#state", state);
          this.logger.log("renderTransitions#skill", skill);
          return <LinearLayout key={`link-${idx}`} orientation={'vertical'} style={{ position: "relative", width: "100%", left: `0px` }} align={'left'}>
            {<LinearLayout orientation={'horizontal'} align={'left'} style={{ marginTop: "20px" }}>
              {idx + 1}. 轉移到
              {skill && <Button style={{ marginLeft: "5px", fontSize: "10pt", padding: "3px 6px", background: "#53b8ed" }}>任務:{skill.name}</Button>}
              {state && <Button style={{ marginLeft: "5px", fontSize: "10pt", padding: "3px 6px", background: "#ffd114" }}>狀態:{state.name}({state.info.label})</Button>}
              {transition.entity ? (`Entity: ${transition.entity}`) : ('')}
              <i className="el-icon-delete" style={{ marginLeft: "5px" }} onClick={this.removeTransition.bind(this, transition)}></i>
            </LinearLayout>}
            <LinearLayout orientation={'horizontal'} align={'left'} style={{ marginTop: "5px" }}>
              {/*{transition.entity}*/}
            </LinearLayout>
            {this.renderTransition(transition)}
            <LinearLayout orientation={'horizontal'} align={'left'} style={{ marginTop: "10px" }}>
            </LinearLayout>
          </LinearLayout>
        })
    )
  }

  removeCondition(transition, condition) {

    if (!confirm("確定要刪除此條件?")) {
      return;
    }

    const { selectedState } = this.state;

    const transitions = flowHelper.removeCondition(selectedState, transition, condition);

    this.logger.log("removeCondition# condition removed# transitions", transitions);
    this.saveUpdatedTransitions(transitions);
  }

  removeTransition(transition) {
    if (!confirm("確定要刪除此狀態轉移?")) {
      return;
    }
    const { selectedState } = this.state;
    const transitions = flowHelper.removeTransition(selectedState, transition);
    this.logger.log("removeCondition# transitions", transitions);
    this.saveUpdatedTransitions(transitions);
  }

  showCondition(condition) {
    const { operation, value, entities } = condition;

    this.logger.log('showCondition', condition)

    if (operation !== 'intent' || !entities) {
      return;
    }

    let es = '';
    entities.forEach((e) => {
      es += "  " + e.entity + "\n";
    })

    alert(`${value}:\n${es}`);
  }

  renderTransition(transition) {
    this.logger.log("renderTransition#", transition);
    if (!transition) {
      return '';
    }

    const widget = Widgets.find(w => w.name === this.state.selectedState.name);
    const inputType = widget.type === 'input' ? '輸入' : '意圖';

    return <LinearLayout orientation={'vertical'} style={{ paddingLeft: "20px", marginTop: "5px" }}>
      <LinearLayout orientation={'vertical'} align={'left'}>
        {transition.conditions.map((condition, idx) => {
          this.logger.log("TransitionSetting# condition", condition);
          let content = '';
          switch (condition.operation) {
            case 'intent':
              content = 'Intent 等於 ' + condition.value;
              break;
            case 'intent_API':
              content = 'API state 等於 ' + condition.value;
              break;
            case 'slot':
              content = 'Slot 等於 ' + condition.value;
              break;
            case 'includes':
              content = `${inputType}包含 ${condition.value}`;
              break;
            case 'regex':
              content = `${inputType}正規表示式 ${condition.value}`;
              break;
            case 'equal':
              content = `${inputType}等於 ${condition.value}`;
              break;
          }

          return <LinearLayout key={`t-${idx}`} orientation={'vertical'} style={{ marginTop: "5px" }}>
            <LinearLayout orientation={'horizontal'} align={'left'}>
              {idx + 1}.
              <span style={{ marginLeft: "5px", fontSize: "10pt", padding: "3px 6px", background: "#ffd114" }}
                    onClick={this.showCondition.bind(this, condition)}>
                {content} {condition.entities && condition.entities.length > 0 ? " ES" : ''}
              </span>
              <i className="el-icon-delete" style={{ marginLeft: "5px" }} onClick={this.removeCondition.bind(this, transition)}></i>
            </LinearLayout>
          </LinearLayout>
        })}
      </LinearLayout>
    </LinearLayout>
  }

  onInputSwitch() {
    const value = !this.state.withInput;
    flowHelper.setNodeInfo(this.state.selectedState, 'withInput', value)
    this.setState({
      withInput: value
    });

    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }
  }

  renderSwitchInput() {

    const { selectedState, withInput } = this.state;
    const widget = flowHelper.getNodeInfoValue(selectedState, 'widget');
    this.logger.log("renderSetting#selectedState", selectedState.name);
    if (!widget || widget.type !== 'message') {
      return '';
    }

    return <LinearLayout orientation={'horizontal'} align={"left"} style={{ marginTop: "10px" }}>
      <div>顯示提示後等待輸入:</div>
      <Switch
          onChange={this.onInputSwitch.bind(this)}
          value={withInput}
          onText=""
          offText="">
      </Switch>
    </LinearLayout>;
  }


  onIntentConfirm(condition) {
    if (!condition.intent) {
      this.showDialog('intent', false);
      return;
    }
  }

  onSlotConfirm(condition) {
    if (!condition.slot) {
      this.showDialog('slot', false);
      return;
    }
  }

  render() {
    const { transitions, showProgressBox, showIntentDialog, showSlotDialog, showIntentAPIDialog, selectItems, selectedSkill, next_skill } = this.state;
    this.logger.log("render# selectedSkill:", selectedSkill);
    this.logger.log("render# selectedSkill: next_skill", next_skill);
    this.logger.log("TransitionSetting# transitions", transitions);

    return <LinearLayout orientation={'vertical'} style={{ position: "relative", width: "100%", left: `0px` }} align={'top'}>
      {this.renderSwitchInput()}
      {this.renderAddCondition()}
      {this.renderTransitions()}
      <DialogSelector
          onSelected={this.onSelected.bind(this)}
          closeDialog={this.showDialog.bind(this, '', false)}
          visible={showDialog}
          items={selectItems} title={"状态"} />
      <IntentDialogSelector
          intents={this.props.intents}
          onConfirm={this.addConditions.bind(this)}
          closeDialog={this.showDialog.bind(this, 'intent', false)}
          visible={showIntentDialog} />
      <SlotDialogSelector
          entities={this.props.entities}
          onConfirm={this.addConditions.bind(this)}
          closeDialog={this.showDialog.bind(this, 'slot', false)}
          visible={showSlotDialog} />
      <IntentAPIDialogSelector
          apiResult={this.props.apiResult}
          onConfirm={this.addConditions.bind(this)}
          closeDialog={this.showDialog.bind(this, 'intent_API', false)}
          visible={showIntentAPIDialog} />

    </LinearLayout>
  }
}

export default TransitionSetting;
