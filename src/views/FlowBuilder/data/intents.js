export const NLU_INTENTS = `h_催促或等候太久
h_充值:充值渠道,金額,時間,日期,遊戲帳號
h_兌換:金幣,金額,時間,日期,遊戲帳號
h_咒罵或抱怨
h_問候
h_報告作弊:作弊玩家名稱,遊戲名稱或內容,時間,日期
h_已提供QQ憑證資訊
h_感謝
h_提供未到帳資訊:充值渠道,金額,時間,日期,遊戲帳號
h_未到帳:充值渠道,金額,時間,日期,遊戲帳號
h_充值未到帳:充值渠道,金額,時間,日期,遊戲帳號
h_催促充值未到帳:充值渠道,金額,時間,日期,遊戲帳號
h_兌換未到帳:充值渠道,金額,時間,日期,遊戲帳號
h_催促兌換未到帳:充值渠道,金額,時間,日期,遊戲帳號
h_催促未到帳:充值渠道,金額,時間,日期,遊戲帳號
h_等待時間已過:時間間隔
h_肯定_了解
h_要求回覆
h_詢問處理時間
h_转账
h_金币异常
none-of-the-above
h_問_代_大類
h_問_代_聲請
h_問_充_不成功
h_問_充_大類
h_問_充_方法
h_問_兌_大類
h_問_兌_方法
h_問_兌_最低金額
h_問_回_QQ未回
h_問_回_代理未回
h_問_回_大類
h_問_登_大類
h_問_登_進不去
h_問_綁_大類
h_問_綁_方法
h_問_綁_更換或解除
h_問_註_大類
h_問_輸_大類
h_問_輸_打魚
h_問_輸_輸錢`;

export const NLU_ENTITY_PROMPTS = {
    "充值渠道": "老板，请您确认一下，您充值不到帐的那笔订单，是点击我们游戏中哪一个充值渠道进行充值的呢？（尊享闪付、支付宝、微信）",
    "日期": "请提供交易时间(格式范例：11/20 15:00)",
    "時間間隔": "请输入時間間隔",
    "金額": "请提供金額，格式：xxx元",
    "時間": "请提供交易时间(格式范例：11/20 15:00)",
    "遊戲名稱或內容": "请提供遊戲名稱或內容",
    "充值人員名稱": "请输入充值人員名稱",
    "銀行": "请输入銀行",
    "訂單號": "请提供訂單號",
    "作弊玩家名稱": "请提供作弊玩家名稱",
    "金幣": "请提供金币数",
    "遊戲帳號": "请提供您的游戏ID！"
};
