export const API_RESULT = `200:金額,時間,日期,充值渠道,訂單號,game-account-number
205:金額,時間,日期,充值渠道,訂單號,game-account-number`;

export const API_RESULT_SLOT = {
    "金額": "{response[money]}",
    "日期": "{response[date]}",
    "時間": "{response[time]}",
    "充值渠道": "{response[cha]}",
    "訂單號": "{response[order]}",
};
