export const Widgets = [{
    type: "message",
    name: "提示狀態",
    color: 'peru',
    dropColor: 'white'
}, {
    type: "input",
    name: "輸入狀態",
    color: 'red',
    dropColor: 'white'
}, {
    type: "nlu",
    name: "AI狀態",
    color: 'blue',
    dropColor: 'white'
}, {
    type: "api",
    name: "查詢狀態",
    color: 'orange',
    dropColor: 'white'
}];
