import React from 'react';
import {
    DiagramWidget,
} from 'storm-react-diagrams';
import skill_graph from '@/assets/img/skill_graph.svg';


class SkillFlow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            engine: undefined,
            display: 'none',
            modal: false
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const { flow } = this.props;
        if (nextProps.show === true) {
            this.setState({
                display: 'block',
                modal: true
            })
        }
        else {
            this.setState({
                display: 'none',
                modal: false
            })
        }
    }

    closeSkillGraph() {
        this.props.switchSkillGraph(false);
        this.setState({
            display: 'none',
            modal: false
        })
    }


    render() {
        const { skillEngine, flow } = this.props;
        if (!skillEngine) {
            return '';
        }
        return <div className="fx fdc" style={{ width: "100%", flexGrow: "1" }}>
            <div className="header-hint-skill vtb">
                <img src={skill_graph} style={{ height: '45px', width: '52px' }} />
                <span className="hint-text">
                    <div className="main-text">{'任务流程'}</div>
                    <div className="sub-text">{'任务间的关系图表'}</div>
                </span>
            </div>
            <div id='skillDivID'></div>
            <div className="diagram-layer _fill h100 fx fdc"
                style={{ flexGrow: "1" }}
                tabIndex="-1">
                <DiagramWidget deleteKeys={[]} smartRouting={true} diagramEngine={skillEngine} />
            </div>
        </div>


    }
}

export default SkillFlow;