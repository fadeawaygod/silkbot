import React from 'react';
import { flowHelper } from "./FlowHelper";
import './css/flow.less'
import { v4 as uuidv4 } from "uuid";

class NodeBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    }
  }

  // componentDidMount() {
  //     flowHelper.renderRemeveStateNode()
  // }

  componentDidUpdate() {
    flowHelper.renderRemeveStateNode()
  }


  remove() {
    if (this.props.selectedState && this.props.selectedState.info) {
      this.props.removeState(this.props.selectedState.info.id)
      //this.removeState(this.props.selectedState.info.id, this.props.stateEngine)
    }

  }

  copyAndPaste() {
    let selectedState = this.props.selectedState;
    selectedState.skillId = this.props.selectedSkill.id;
    let node = this.props.onNodeCopy(selectedState);
    node.x = selectedState.x + 100;
    node.y = selectedState.y + 70;
    this.props.stateEngine.getDiagramModel().addNode(node);
    this.forceUpdate();
    // this.props.saveFlowDiagram();
  }

  defaultNodeModeltoObject(defaultNode, skillName) {
    let nodeObj = {}
    nodeObj['id'] = defaultNode.id;
    nodeObj['color'] = defaultNode.color;
    nodeObj['extras'] = defaultNode.extras;
    nodeObj['info'] = defaultNode.info;
    nodeObj['name'] = defaultNode.name;
    nodeObj['ports'] = [{
      id: uuidv4(),
      in: false,
      label: defaultNode.info.label,
      links: [],
      name: "port01",
      parentNode: defaultNode.id,
      selected: false,
      _class: "DefaultPortModel"
    }];
    nodeObj['selected'] = defaultNode.selected;
    nodeObj['skillId'] = defaultNode.skillId;
    nodeObj['skillName'] = skillName;
    nodeObj['type'] = "default";
    nodeObj['x'] = 120;
    nodeObj['y'] = 120;
    nodeObj['_class'] = "DefaultNodeModel";
    return nodeObj
  }

  move(skillId) {
    const { selectedSkill, selectedState, flow } = this.props;
    // let selectedState = this.props.selectedState;
    let localFlow = JSON.parse(JSON.stringify(flow));
    let node = this.props.onNodeCopy(selectedState, true);
    node.skillId = skillId;
    let targetSkill = localFlow.content.skills.nodes.find(node => node.id === skillId)
    let nodeObj = this.defaultNodeModeltoObject(node, targetSkill.name);
    const findNodeIndex = (element) => element.id === selectedState.id;
    let cutIndex = localFlow.content.states[selectedSkill.id].nodes.findIndex(findNodeIndex);
    localFlow.content.states[selectedSkill.id].nodes.splice(cutIndex, 1);
    localFlow.content.states[skillId].nodes.push(nodeObj);
    localFlow = flowHelper.showLinks(localFlow, undefined);
    this.props.saveFlow(localFlow);
    this.forceUpdate();
  }



  renderSkills() {
    const { flow, selectedSkill } = this.props;
    if (!flow || !selectedSkill)
      return ''
    let skillNames = [];
    for (const skill of flow.content.skills.nodes) {
      if (skill.id !== selectedSkill.id) {
        let node = flow.content.skills.nodes.find(node => node.id === skill.id)
        skillNames.push(node)
      }
    }
    return (
      <div className="move-to">
        {skillNames.map(function (skill, index) {
          return <div className="Rectangle-pop-box"
            onClick={e => this.move(skill.id)}
            key={index}>
            {skill.name}
          </div>
        }, this)}
      </div>
    )
  }

  renderBar() {
    return <div className="button-more" tabIndex='-1'>
      <div className="O"></div>
      <div className="O"></div>
      <div className="O"></div>
      <div className="menu-box">
        <div className="Rectangle-pop-box" onClick={this.copyAndPaste.bind(this)}>
          <div className="text-pop-box">复制及贴上</div>
        </div>
        <div className="Rectangle-pop-box" onClick={this.remove.bind(this)}>
          <div className="text-pop-box">删除</div>
        </div>
        <div className="Rectangle-pop-box">
          <div className="text-pop-box">移至
          <span className="right-triangle"></span>
            {/* <div className="move-to"> */}
            {this.renderSkills()}
          </div>

        </div>
      </div>
    </div>
  }


  renderAll() {
    if (this.props.stateEngine && this.props.stateEngine.diagramModel && this.props.stateEngine.diagramModel.nodes) {
      const stateNodes = Object.keys(this.props.stateEngine.diagramModel.nodes);
      return stateNodes.map((stateNode) => {
        return <div id={'bar-div-id_' + stateNode} key={stateNode}>
          {this.renderBar()}
        </div>
      })

    }
    else {
      return ''
    }
  }

  render() {
    return <div style={{ height: 0, overflow: 'hidden' }}>{this.renderAll()}</div>

  }

}

export default NodeBar
