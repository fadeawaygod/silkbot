import React from 'react';
import './css/flow.less'
import show_path from '@/assets/img/show_path.svg';
import show_path_hover from '@/assets/img/show_path_hover.svg';
import close_path from '@/assets/img/close_path.svg';
import close_path_hover from '@/assets/img/close_path_hover.svg';
import to_middle from '@/assets/img/to_middle.svg';
import to_middle_hover from '@/assets/img/to_middle_hover.svg';

import store from '@/store';
export default class AdjustmentIcon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hoverPath: false,
      hoverMiddle: false
    };
  }

  updateShowSkillLink() {
    if (this.props.updateShowSkillLink) {
      this.props.updateShowSkillLink()
    }
  }

  changeScale() {
    this.props.changeScale()
  }

  toggleHPathover() {
    this.setState(prevState => ({ hoverPath: !prevState.hoverPath }));
  }

  toggleHMiddleover() {
    this.setState(prevState => ({ hoverMiddle: !prevState.hoverMiddle }));
  }


  render() {
    const { hoverMiddle, hoverPath } = this.state;
    let pathImage = show_path;
    let middleIcon = to_middle;
    if (hoverMiddle === true) {
      middleIcon = to_middle_hover
    }
    if (this.props.showStateLink === true) {
      if (hoverPath === false) {
        pathImage = close_path;
      }
      else {
        pathImage = close_path_hover
      }
    }
    else {
      if (hoverPath === false) {
        pathImage = show_path;
      }
      else {
        pathImage = show_path_hover
      }
    }

    return <span>
      <div style={{
        display: "flex",
        alignItems: 'flex-end'
      }}>
        {/* <span style={{
          top: "127px",
          left: "999px",
          width: "0px",
          height: "59px",
          border: "1px solid #DBDBDB",
          opacity: 1,
          marginRight: "20px"
        }}>
        </span> */}
        <img className="new-task-button"
          src={pathImage}
          onMouseEnter={this.toggleHPathover.bind(this)}
          onMouseLeave={this.toggleHPathover.bind(this)}
          onClick={this.updateShowSkillLink.bind(this)} />
        <img className="new-task-button"
          src={middleIcon}
          onMouseEnter={this.toggleHMiddleover.bind(this)}
          onMouseLeave={this.toggleHMiddleover.bind(this)}
          onClick={this.changeScale.bind(this)} />
        <span style={{ cursor: "pointer" }} className={store.getters.currentBot.config.chinese_convert} onClick={() => { store.state.chineseConvertShield_show = true }}>
          <div style={{
            fontSize: "24px",
            textAlign: "center"
          }}>{{
            none: "默认",
            auto: "自动",
            zh_cn: "简体",
            zh_tw: "繁体"
          }[store.getters.currentBot.config.chinese_convert]}</div>
          <div>繁简转换</div>
        </span>
      </div>
    </span>
  }
}
