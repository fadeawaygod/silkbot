import React from 'react';

import SkillEditor from "./SkillEditor";
import StateEditor from "./StateEditor";
import LinearLayout from "./LinearLayout";
import { Logger } from "../../utils/Logger";
import { Button, Checkbox, Input } from "element-react";
import StateSetting from "./StateSetting";
import SkillSetting from "./SkillSetting";
import Sample from "./data/sample.json";
import defaultSessionManager from "./data/defaultSessionManager.json";
import { notificationService } from "../../api/NotificationService";
import NodeBar from './NodeBar';
import SkillBar from './SkillBar';
import SkillFlow from './SkillFlow';
import { DefaultNodeModel, DefaultPortModel } from 'storm-react-diagrams';
import { v4 as uuidv4 } from "uuid";

import { flowHelper } from "./FlowHelper";

import {
  createFlow,
  getBot,
  getFlow,
  getFlowHistory, removeFlowHistory, setupConfig,
  setUsingDefaultNLU,
  testFlow,
  updateBot,
  updateFlow
} from "../../api/Bot";
import DialogSelector from "./DialogSelector";
import { getTimeText, toYYYYMMDDhhmmss } from "../../utils/time";
import DialogProgress from "./DialogProgress";
import SavingProgress from "./SavingProgress";
import { FormControl } from "react-bootstrap";

class FlowEditor extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("FlowEditor");
    this.logger.log("FlowEditor#created#botId:", props.botId);
    this.createFunctionss();
    this.state = {
      selectedSkill: undefined,
      statetoCopy: undefined,
      botId: this.props.botId,
      botData: Sample.agent,
      flow: {},
      flows: [],
      nextSkill: undefined,
      nextState: undefined,
      intents: [],
      entities: [],
      apiResult: [],
      flowHistories: [],
      showSkillGraph: false
    }
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    return {
      botId: nextProps.botId
    }
  };

  createFunctionss() {
    this.functions = {
      updateSkillName: (id, name) => {
        this.logger.log("functions#updateSkillName#", `${id}:${name}`);
        const node = flowHelper.getNode(this.skillEngine, id);

        if (!node) {
          alert("Skill Node not found!");
          return;
        }

        flowHelper.setNodeName(node, name);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      updateShowSkillLink: (id, showLink) => {
        const node = flowHelper.getNode(this.skillEngine, id);

        if (!node) {
          alert("Skill Node not found!");
          return;
        }
        if (!showLink) {
          flowHelper.setNodeShowLink(node, false);
        }
        else {
          flowHelper.setNodeShowLink(node, showLink);
        }

        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      removeSkill: (id) => {
        const node = flowHelper.getNode(this.skillEngine, id);

        if (!node) {
          alert("Skill Node not found!");
          return;
        }

        if (!confirm(`是否刪除 ${node.info.name}？`)) {
          return;
        }
        flowHelper.removeNode(this.skillEngine, node);
        this.setState({
          selectedSkill: undefined
        });
        flowHelper.removeRelatedSkillTransitions(this.state.flow.content.states, id)
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      updateStateName: (id, name) => {
        this.logger.log("functions#updateStateName#", `${id}:${name}`);
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          alert("State Node not found!");
          return;
        }
        flowHelper.setNodeName(node, name);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      onNodeSelect: (skillId, stateId) => {
        flowHelper.setNodeSelected(this.skillEngine, skillId);
        let skillNode = flowHelper.getNode(this.skillEngine, skillId);
        this.setState({
          selectedSkill: skillNode,
        });
        setTimeout(() => {
          let stateNode = flowHelper.getNode(this.stateEngine, stateId);
          this.setState({
            selectedState: stateNode,
          });
          this.props.getCurrentNode()
        }, 0);
      },

      onStateShow: (isSelected, skillId) => {
        if (isSelected) {
          let showSkill = flowHelper.getNode(this.skillEngine, skillId)
          if (showSkill) {
            this.setState({
              selectedSkill: showSkill,
              showSkillGraph: false
            })
          }
          else {
            this.state.selectedSkill = undefined;
          }
        }
        else {
          this.state.selectedSkill = undefined;
        }
        return

      },

      removeState: (id) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          alert("State Node not found!");
          return;
        }
        if (!confirm(`是否刪除 ${node.info.name}？`)) {
          return;
        }

        flowHelper.removeNode(this.stateEngine, node);
        this.setState({
          selectedState: undefined
        });
        flowHelper.removeRelatedStateTransitions(this.state.flow.content.states[this.state.selectedSkill.id], id)
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      removeSkillLinks: (id) => {
        const node = flowHelper.getNode(this.skillEngine, id);
        flowHelper.removeLinks(this.skillEngine, node);

        this.setState({
          selectedSkill: undefined
        });

        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      removeStateLinks: (id) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }

        flowHelper.removeLinks(this.stateEngine, node);
        this.setState({
          selectedState: undefined
        });
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setStateNodeAsStart: (id, isStart) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        let stateNode = node
        if (this.stateEngine.diagramModel.nodes) {
          Object.keys(this.stateEngine.diagramModel.nodes).forEach((nodeID) => {
            stateNode = flowHelper.getNode(this.stateEngine, nodeID);
            flowHelper.setNodeInfo(stateNode, 'isStart', false);
          })
        }
        flowHelper.setNodeInfo(node, 'isStart', isStart);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setStateNodeAsEnd: (id, isEnd) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        flowHelper.setNodeInfo(node, 'isEnd', isEnd);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setNluUrl: (id, nluUrl) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        flowHelper.setNodeInfo(node, 'nluUrl', nluUrl);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setNluToken: (id, nluToken) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        flowHelper.setNodeInfo(node, 'nluToken', nluToken);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setStateNodeWithInput: (id, withInput) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        flowHelper.setNodeInfo(node, 'withInput', withInput);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      onSkillAdd: () => {
        let node = new DefaultNodeModel('任务', 'peru');
        node.addPort(new DefaultPortModel(false, 'port-message', ' '));
        node["info"] = { isSkill: true, id: node.id, name: '任务' };
        this.skillEngine.getDiagramModel().addNode(node);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setStateLabel: (id, label) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        flowHelper.setNodeLabel(node, label);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setStateAlias: (id, alias) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        flowHelper.setNodeInfo(node, "alias", alias);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setStateText: (id, text) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        flowHelper.setStateText(node, text);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      getStateText: (id) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }
        return flowHelper.getStateText(node);
      },

      addCondition: (currentStateId, next_state_id, next_skill_id, condition, entityKey) => {

        const stateNode = flowHelper.getNode(this.stateEngine, currentStateId);
        if (!stateNode) {
          return;
        }

        const skillNodes = flowHelper.getSkillNodes(this.skillEngine);
        if (!skillNodes || skillNodes.length === 0) {
          return;
        }

        if (!next_skill_id && !next_state_id) {
          // @TODO to remove
          alert("請先選擇轉移狀態或任務");
          return;
        }

        if (!condition.operation) {
          // @TODO to remove
          alert("請先選擇條件類型");
          return;
        }

        if (!condition.value) {
          // @TODO to remove
          alert("請先輸入條件值");
          return;
        }

        const sessionManager = skillNodes[0];
        let next_skill = next_skill_id === sessionManager.id ? 'session-manager' : next_skill_id;
        if (next_state_id) {
          next_skill = undefined;
        }

        const transitions = flowHelper.getTransitions(stateNode) ? flowHelper.getTransitions(stateNode) : [];

        let foundTransition = transitions.find(t => {
          this.logger.debug("addCondition# transition:", t);
          return t.next_state === next_state && t.next_skill === next_skill_id
        });

        this.logger.debug("addCondition#foundTransition", foundTransition);

        if (foundTransition) {
          if (!foundTransition.conditions) {
            foundTransition["conditions"] = [];
          }
          if (entityKey) {
            foundTransition["entity"] = entityKey;
          }
          foundTransition.conditions.push(condition);
        } else {
          const transition = {
            entity: entityKey ? entityKey : undefined,
            next_state: next_state_id,
            next_skill: next_skill,
            conditions: [condition]
          };
          transitions.push(transition);
        }

        flowHelper.setTransitions(stateNode, transitions);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      removeCondition: (id, transition, condition) => {

        const stateNode = flowHelper.getNode(this.stateEngine, id);
        if (!stateNode) {
          return;
        }

        if (!confirm("確定要刪除此條件?")) {
          return;
        }

        const transitions = flowHelper.removeCondition(stateNode, transition, condition);

        flowHelper.setTransitions(stateNode, transitions);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      removeTransition(id, transition) {

        const stateNode = flowHelper.getNode(this.stateEngine, id);
        if (!stateNode) {
          return;
        }

        if (!confirm("確定要刪除此狀態轉移?")) {
          return;
        }

        const transitions = flowHelper.removeTransition(stateNode, transition);
        flowHelper.setTransitions(stateNode, transitions);
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      setTransitions: (id, transitions) => {

        const stateNode = flowHelper.getNode(this.stateEngine, id);
        if (!stateNode) {
          return;
        }

        if (stateNode.info) {
          stateNode.info["transitions"] = transitions;
        } else {
          stateNode["info"] = { transitions };
        }
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      getTransitions: (id) => {
        const stateNode = flowHelper.getNode(this.stateEngine, id);
        if (!stateNode) {
          return;
        }
        return flowHelper.getTransitions(stateNode);
      },

      getCurrentSkill: () => {
        if (!this.state.selectedSkill) {
          return undefined;
        }

        return flowHelper.getNode(this.skillEngine, this.state.selectedSkill.id);
      },

      getCurrentState: () => {
        if (!this.state.selectedState) {
          return undefined;
        }

        return flowHelper.getNode(this.stateEngine, this.state.selectedState.id);
      },

      getCurrentSkills: () => {
        return flowHelper.getSkillNodes(this.skillEngine);
      },

      getCurrentStates: () => {
        return flowHelper.getStateNodes(this.stateEngine);
      },

      getStateAPIInfo: (id) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return undefined;
        }

        return flowHelper.getNodeInfoValue(node, "apiInfo");
      },

      setStateAPIInfo: (id, url, httpMethod, httpBody) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }

        flowHelper.setAPIInfo(node, { url, httpMethod, httpBody });
        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      switchSkillGraph: (show) => {
        const { flow } = this.state;
        if (flow.content && flow.content.skills.nodes.length === 0) {
          this.setState({
            showSkillGraph: show
          });
          return
        }
        let firstSkill = flowHelper.getNode(this.skillEngine, flow.content.skills.nodes[0].id);
        if (show) {
          this.setState({
            showSkillGraph: show,
            selectedSkill: undefined,
            selectedState: undefined
          });
        }
        else {
          this.setState({
            showSkillGraph: show,
            selectedSkill: firstSkill,
            selectedState: undefined
          });
        }
      },

      setState: (id) => {
        const node = flowHelper.getNode(this.stateEngine, id);
        if (!node) {
          return;
        }

        this.forceUpdateView();
        this.saveFlowDiagram();
      },

      saveFlowDiagram: () => {
        this.saveFlowDiagram();
      }
    }
  }

  componentDidMount() {
    this.logger.log("componentDidMount#botId:", this.props.botId);
    this.reload(this.props.botId);
    // 這功能會影響回傳訊息速度, 先註解
    // this.listenStateChanged();
  }


  // componentDidUpdate() {
  //   this.layout();
  // }

  copyState() {
    const { selectedState } = this.state;
    if (!selectedState) {
      return
    }
    let tmpState = Object.assign({}, selectedState);
    let id = uuidv4();
    tmpState['id'] = id;
    tmpState['skillId'] = this.state.selectedSkill.id;
    this.setState({
      statetoCopy: tmpState,
    });
    // console.log(243555, selectedState, tmpState)
  }

  listenStateChanged() {
    const { botId } = this.props;
    notificationService.init(() => {
      this.logger.log("state#init successfully");
      notificationService.subscribe({
        onOpen: (data) => {
          this.logger.log("onOpen", data)
        },
        onCreate: (data) => {
          this.logger.log("onCreate", data)
        },
        onDelete: (data) => {
          this.logger.log("onDelete", data)
        },
        onUpdate: (data) => {
          this.logger.log("state#onUpdate", data)
          if (data && data.content) {
            this.setState({
              nextSkill: data.content.skill ? data.content.skill : undefined,
              nextState: data.content.state ? data.content.state : undefined,
            });
            const skillNode = flowHelper.getNextSkillNode(this.skillEngine, data.content.skill);
            if (skillNode) {
              flowHelper.setNodeSelected(this.skillEngine, skillNode.id);
            }
            const stateNode = flowHelper.getStateNode(this.stateEngine, data.content.state);
            if (stateNode) {
              flowHelper.setNodeSelected(this.stateEngine, stateNode.id);
            }
          }
        }
      },
        { event: 'user_state_changed', agent_id: botId })
    })
  }

  loadBot(botId) {
    return getBot({ id: botId }).then(result => {
      if (!result || !result.bot) {
        return Promise.reject("loadBot# get bot failed!");
      }

      const botData = this.prepareBotData(result.bot);
      botData["bot"] = result.bot;

      return this.loadFlow(botId).then(flow => {
        flow.content.skills.links = [];
        let linkPairs = flowHelper.findSkillLink(flow.content.states);
        let allSkillNodesId = flowHelper.findAllNodeId(flow.content.states);
        linkPairs.forEach(pair => {
          if (allSkillNodesId.includes(pair[0]) && allSkillNodesId.includes(pair[1])) {
            flow.content.skills.links.push(flowHelper.addlink(flow.content.skills, pair[0], pair[1]));
          }
        })
        botData["flow"] = flow;
        return Promise.resolve(botData);
      })
    })
  }


  getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }

  prepareBotData(bot) {
    this.logger.log("prepareBotData# bot:", bot);
    let intents = bot.intents;
    let entities = bot.entities;
    let apiResult = [];
    if (true || bot.use_fixed_nlu) {
      intents = flowHelper.getSilkIntents();
      entities = flowHelper.getSilkEntities();
      apiResult = flowHelper.getSilkAPIResult();
    }

    const data = { intents, entities, apiResult };

    this.logger.log("prepareBotData# data:", data);
    return data;
  }

  updateStateFlow(flow) {
    this.setState({
      flow: this.flowPrepare(flow)
    })
  }

  reload(botId) {
    this.loadBot(botId).then(botData => {
      this.updateBotData(botData);
      if (this.props.onInit) {
        botData["functions"] = this.functions;
        this.props.onInit(botData);
      }
    });

    this.loadFlowHistory(botId);
  }

  loadFlow(botId) {
    return flowHelper.loadFlow(botId).then(flow => {
      if (!flow.id) {
        this.logger.log("reload# createFlow due to first created bot");
        return createFlow(botId).then(result => {
          this.logger.log("reload# createFlow#result:", result);
          return flowHelper.loadFlow(botId).then(flow => {
            flow.content = defaultSessionManager;
            updateFlow(flow.id, defaultSessionManager).then(result2 => {
              console.log("saveFlowDiagram#", result2);
              this.setState({
                flow: flow
              });
            });
            return Promise.resolve(flow);
          })
        })
      } else {
        return Promise.resolve(flow);
      }
    });
  }

  loadFlowHistory(botId) {
    this.setState({
      isLoadFlowHistory: true,
    });

    getFlowHistory(botId).then(flowHistories => {
      this.logger.log("isLoadFlowHistory#", flowHistories);
      this.setState({
        flowHistories,
        isLoadFlowHistory: false,
      })
    })
  }

  checkState() {
    flowHelper.getBotState().then(result => {
      this.logger.log("checkState#", result);
      this.updateStates(result);
    });
  }

  updateStates(result) {
    const skillNodes = this.skillEngine.diagramModel.nodes;
    if (!skillNodes) {
      return;
    }

    const skillNodeKeys = Object.keys(skillNodes);

    const skillId = result["80yo0abFI7171kPx13AU6a0XkBvbaSUqDi3VcbBpiHUMTU2NzU2ODA5NTEzMg==_766451083_skill"];
    const stateId = result["80yo0abFI7171kPx13AU6a0XkBvbaSUqDi3VcbBpiHUMTU2NzU2ODA5NTEzMg==_766451083_state"];
    this.logger.log("updateSelectedState# current Skill:", skillId);
    this.logger.log("updateSelectedState# current State:", stateId);
    if (skillId === 'session-manager') {
      flowHelper.setSelectedNode(this.skillEngine.diagramModel, skillNodeKeys[0]);
    } else {
      flowHelper.setSelectedNode(this.skillEngine.diagramModel, skillId);
    }

    const stateNodes = this.stateEngine.diagramModel.nodes;
    if (!stateNodes) {
      return;
    }

    flowHelper.setSelectedNode(this.stateEngine.diagramModel, stateId);
  }

  saveFlowDiagram() {
    const { flow, selectedSkill } = this.state;

    this.setState({
      showDialog: true
    });

    const updatedFlow = flowHelper.getUpdatedDiagramFlow(flow, selectedSkill, this.skillEngine, this.stateEngine);
    if (updatedFlow) {
      this.logger.log("saveFlowDiagram#content:", updatedFlow.content);
      updateFlow(updatedFlow.id, updatedFlow.content).then(result => {
        this.setState({
          toUpload: false,
          showDialog: false,
          flow: updatedFlow
        });
        // location.reload();
      });
      //更新vue的flow
      this.props.syncFlow(updatedFlow);
      // this.props.popSuccess("储存中, 请稍候");
    }
  }

  saveFlow(flow) {
    if (flow && flow.id) {
      updateFlow(flow.id, flow.content).then(result => {
        this.setState({
          toUpload: false,
          flow: flow
        });
      }).then(() => {
        this.saveFlowDiagram();
      });
    }
  }

  updateFlowDiagramByFlowHistory(flowHistory) {
    if (flowHistory && flowHistory.flow) {
      const { flow } = flowHistory;
      this.logger.log("updateFlowDiagramByFlowHistory#flow:", flow);
      updateFlow(flow.id, flow.content).then(result => {
        console.log("updateFlowDiagramByFlowHistory#", result);
        this.setState({
          toUpload: false,
        });
        location.reload();
      });
    }
  }

  updateBotConfig() {
    const { botId, flow } = this.state;
    getBot({ id: botId }).then(result => {
      if (result.status === "ok") {
        const bot = result.bot;
        this.logger.log("updateBotConfig#current Bot:", bot);
        const config = flowHelper.buildBot(botId, flow);
        bot["config"] = config;
        bot["status"] = 'ready';
        this.logger.log(`updateBotConfig# config(${botId}):`, config);
        this.logger.log(`updateBotConfig# config(${botId}):`, JSON.stringify(config));
        updateBot(bot).then(result => {
          if (result.status === 'ok') {
            // alert("發佈成功");
            this.forceUpdateBotConfig();

            this.copyText(JSON.stringify(config), "Copy config successfully");
          } else {
            this.logger.debug("發佈失敗");
            alert(JSON.stringify(result))
          }
        });
      }
    })
  }

  copyText(text, message) {
    console.log(`copy text ${text}`)
    // document and select() only allows user interactive elements. Does not work on <span>
    // create ghost element, copy, and remove ghost element
    var ghostTextArea = document.createElement("textarea")
    ghostTextArea.value = text
    document.body.appendChild(ghostTextArea)
    ghostTextArea.select()
    document.execCommand("Copy")

    if (message) {
      alert(message)
    }
    // remove ghost area
    ghostTextArea.remove();
  }

  forceUpdateBotConfig() {
    const { botId, flow } = this.state;
    const config = flowHelper.buildBot(botId, flow);
    this.logger.log("forceUpdateBotConfig#", config);
    flowHelper.publishConfig(botId, config).then(result => {
      this.logger.log("forceUpdateBotConfig#", result);
      if (result.status === 'ok') {
        // alert("發佈成功");
      } else {
        this.logger.debug("發佈失敗");
        alert(JSON.stringify(result))
      }
    })
  }

  publishDefault() {
    const { botId } = this.state;
    this.logger.log("publishDefault#", Sample);
    flowHelper.publishConfig(botId, Sample).then(result => {
      this.logger.log("publishDefault#", result);
      if (result.status === 'ok') {
        // alert("發佈成功");
      } else {
        this.logger.debug("發佈失敗");
        alert(JSON.stringify(result))
      }
    })
  }

  onSkillNodeAdded(node) {

    this.logger.debug("onSkillNodeAdded#node:", node);

    if (this.props.onSkillSelected) {
      this.props.onSkillSelected(node);
    }
    this.setState({
      selectedSkill: node,
    });

    this.saveFlowDiagram();
  }

  onStateNodeAdded(node) {
    this.logger.debug("onStateNodeAdded#node:", node);
    if (this.props.onStateSelected) {
      this.props.onStateSelected(node)
    }

    this.setState({
      selectedState: node,
    });
    this.saveFlowDiagram();
  }


  onSkillSelected(selectedSkill, isSelected, diagramModel) {

    this.logger.debug("onSkillSelected#skill:", selectedSkill);

    this.setState({
      selectedSkill: undefined,
      selectedState: undefined
    });
    return;

  }


  onStateSelected(selectedState, isSelected, diagramModel) {


    this.logger.debug("onStateSelected#state:", selectedState);

    const currentState = this.state.selectedState;

    if (isSelected && selectedState !== currentState) {
      if (this.props.onStateSelected) {
        this.props.onStateSelected(selectedState)
      }

      this.setState({
        selectedState
      })
    }

    if (!isSelected) {

      if (this.props.onStateSelected) {
        this.props.onStateSelected(undefined)
      }

      this.setState({
        selectedState: undefined,
      })
    }
  }

  onSkillDiagramChanged(diagramModel) {
  }

  onStateDiagramChanged(diagramModel) {
  }

  initSkillDiagramEngine(engine) {
    this.logger.debug("initSkillDiagramEngine#")
    this.skillEngine = engine;
  }

  initStateDiagramEngine(engine) {
    this.logger.debug("initStateDiagramEngine#")
    this.stateEngine = engine;
  }

  forceUpdateView() {
    this.logger.debug("forceUpdateView#")

    const { botId } = this.state;
    this.forceUpdate();
    this.setState({
      toUpload: true,
    })
    // this.updateFlowDiagramDelayed(500);
  }

  removeSkillNode() {
    const { selectedSkill, flow } = this.state;
    if (!selectedSkill || !this.skillEngine) {
      return;
    }

    if (!confirm("確定要刪除 Skill " + selectedSkill.name + "?")) {
      return;
    }

    flowHelper.removeNode(this.skillEngine, selectedSkill);

    this.setState({
      selectedSkill: undefined
    });

    this.forceUpdateView();
  }

  removeSkillLinks() {
    const { selectedSkill } = this.state;
    if (!selectedSkill || !this.skillEngine) {
      return;
    }

    if (!confirm("確定要刪除 Skill " + selectedSkill.name + " 的所有連結?")) {
      return;
    }

    flowHelper.removeLinks(this.skillEngine, selectedSkill);

    this.setState({
      selectedSkill: undefined
    });

    this.forceUpdateView();
  }

  removeStateNode() {
    const { selectedState } = this.state;
    if (!selectedState || !this.stateEngine) {
      return;
    }

    if (!confirm(`確定要刪除 "${selectedState.name}" 狀態?`)) {
      return;
    }

    flowHelper.removeNode(this.stateEngine, selectedState);
    this.setState({
      selectedState: undefined
    });
    this.forceUpdateView();
  }

  setBotUseDefaultNLU() {
    const { bot } = this.state;
    if (!bot) {
      return;
    }
    bot["use_fixed_nlu"] = !bot.use_fixed_nlu;
    setUsingDefaultNLU(bot.id, bot["use_fixed_nlu"]).then(result => {
      this.loadBot(bot.id).then(botData => {
        this.updateBotData(botData);
      });
    })
  }

  updateBotData(botData) {
    this.setState({
      bot: botData.bot,
      intents: botData.intents,
      entities: botData.entities,
      apiResult: botData.apiResult,
      flow: botData.flow
    })
  }

  saveConfigVersion() {
    const { botId, flow } = this.state;

    const title = prompt("请输入脚本资讯", flow && flow.title ? flow.title : '');

    if (!title) {
      alert("請輸入版本資訊!");
      return;
    }

    flow["title"] = title;

    getBot({ id: botId }).then(result => {
      if (result.status === "ok") {
        const bot = result.bot;
        this.logger.log("updateBotConfig#current Bot:", bot);
        const config = flowHelper.buildBot(botId, flow);
        bot["config"] = config;
        bot["status"] = 'ready';
        this.logger.log(`updateBotConfig# config(${botId}):`, config);
        this.logger.log(`updateBotConfig# config(${botId}):`, JSON.stringify(config));
        testFlow(botId, flow, config).then(result2 => {
          this.logger.log("saveConfigVersion#", result2);
          this.logger.log("saveFlowDiagram#content:", flow.content);
          alert("储存成功");
          this.setState({
            flow
          })
        })
      }
    })
  }

  renderStateBar() {
    const { botId, flow } = this.state;
    return <NodeBar
      flow={flow}
      stateEngine={this.stateEngine}
      selectedState={this.state.selectedState}
      selectedSkill={this.state.selectedSkill}
      removeState={this.functions.removeState.bind(this)}
      copyState={this.copyState.bind(this)}
      onNodeCopy={this.onNodeCopy.bind(this)}
      onNodeAdded={this.onStateNodeAdded.bind(this)}
      saveFlow={this.saveFlow.bind(this)}
    />
  }

  renderBotInfo() {
    const { toUpload, bot, botId, selectedState } = this.state;
    return <LinearLayout orientation={'horizontal'} align={'right'} style={{ width: '98%', marginTop: '20px' }}>
      <NodeBar
        stateEngine={this.stateEngine}
        // forceUpdateView={this.forceUpdateView.bind(this)}
        // saveFlowDiagram={this.saveFlowDiagram.bind(this)}
        removeState={this.functions.removeState.bind(this)}
      />
      <Button style={{ marginLeft: "10px" }} onClick={this.showProgressBox.bind(this, true)}>查看脚本</Button>
      <Button style={{ marginLeft: "10px" }} onClick={this.saveFlowDiagram.bind(this)}>保存</Button>
      <Button style={{ marginLeft: "10px" }} onClick={this.copyBotConfig.bind(this)}>複製脚本</Button>
      <Button style={{ marginLeft: "10px" }} onClick={this.saveConfigVersion.bind(this)}>儲存為新版本</Button>
      <Button style={{ marginLeft: "10px" }} onClick={this.showConfigHistory.bind(this, true)}>版本紀錄</Button>
      <SkillBar
        skillEngine={this.skillEngine}
        // forceUpdateView={this.forceUpdateView.bind(this)}
        // saveFlowDiagram={this.saveFlowDiagram.bind(this)}
        selectedSkill={this.state.selectedSkill}
        botId={botId}
        removeSkill={this.functions.removeSkill.bind(this)}
        flow={this.state.flow}
      />
      <Checkbox style={{ marginLeft: "10px" }} onChange={this.setBotUseDefaultNLU.bind(this, bot)}
        checked={bot && bot.use_fixed_nlu ? true : false}>使用預設 AI 引擎</Checkbox>
      {/*<Checkbox key={`slot-${idx}`} onChange={this.onEntityChanged.bind(this, intent, slot)} checked={this.hasEntity(intent, slot)}>{slot.name}</Checkbox>*/}
      {/*<Button style={{marginLeft:"10px"}} onClick={this.publishDefault.bind(this)}>发布预设脚本</Button>*/}
    </LinearLayout>
  }

  renderSettings() {
    const { selectedSkill, selectedState, intents, entities, apiResult } = this.state;
    return <LinearLayout orientation={"vertical"} style={{ width: '600px', minHeight: '800px' }} align={"top"}>
      <LinearLayout orientation={'horizontal'} align={'left'} style={{ marginTop: "20px" }}>
        {selectedSkill && <Button type={'warning'}>任務設定</Button>}
      </LinearLayout>
      {selectedSkill &&
        <SkillSetting
          forceUpdateView={this.forceUpdateView.bind(this)}
          removeSkill={this.removeSkillNode.bind(this)}
          removeLinks={this.removeSkillLinks.bind(this)}
          skill={flowHelper.getSelectedSkillNode(selectedSkill, this.skillEngine)} />}
      <div style={{ width: '100px', height: '20px' }} />
      <LinearLayout orientation={'horizontal'} align={'left'} style={{ marginTop: "20px" }}>
        {selectedState && <Button type={'warning'}>{selectedState.name}狀態設定</Button>}
      </LinearLayout>
      {selectedState &&
        <StateSetting
          intents={intents}
          entities={entities}
          apiResult={apiResult}
          skillNodes={flowHelper.getSkillNodes(this.skillEngine)}
          stateNodes={flowHelper.getStateNodes(this.stateEngine)}
          forceUpdateView={this.forceUpdateView.bind(this)}
          serializedDiagram={flowHelper.serialize(this.stateEngine)}
          removeState={this.removeStateNode.bind(this)}
          selectedSkill={selectedSkill}
          selectedState={selectedState} />}
    </LinearLayout>
  }

  closeConfigHistory() {
    this.showConfigHistory(false);
  }

  onConfigSelected(item) {
    this.logger.log("onConfigSelected", item);
  }

  removeHistory(flowHistory) {
    const { botId } = this.state;
    if (!flowHistory || !flowHistory.id) {
      return;
    }

    if (!confirm("确定要移除? " + (flowHistory.flow.title ? flowHistory.flow.title : flowHistory.id))) {
      return;
    }

    removeFlowHistory(botId, flowHistory.id).then(result => {
      alert(result ? "移除成功" : "移除失败");
      if (result) {
        this.showConfigHistory(true);
      }
    })
  }


  addStateLink(flow) {
    const { selectedState } = this.state;
    if (!flow || !flow.content) {
      return
    }
    let localFlow = flow;
    if (selectedState) {
      localFlow = flowHelper.showLinks(localFlow, selectedState.id);
    }
    else {
      localFlow = flowHelper.showLinks(localFlow, undefined);
    }
  }

  onLinkSelected(link, isSelected) {
    if (this.props.onLinkSelected) {
      this.props.onLinkSelected(link, isSelected);
    } else {
      console.log(isSelected ? 'Selected' : 'Unselected', link);
    }
  }

  onNodeCopy(node, isMove = false) {
    const newNode = new DefaultNodeModel(node.info.widget.name, node.info.widget.dropColor);
    let newName = isMove ? node.info.name : node.info.name + '-copy';
    newNode.addPort(new DefaultPortModel(false, 'port01', newName));
    let newtransitions = [];
    if (node.skillId === this.state.selectedSkill.id) {
      newtransitions = JSON.parse(JSON.stringify(node.info.transitions));
    }
    if (node.info.widget.type === 'message') {
      newNode["info"] = {
        widget: node.info.widget,
        id: newNode.id,
        text: node.info.text,
        tprompt: node.info.tprompt,
        name: newName,
        label: newName,
        transitions: newtransitions,
        isEnd: node.info.isEnd
      };
    }
    else if (node.info.widget.type === 'api') {
      newNode["info"] = {
        widget: node.info.widget,
        id: newNode.id,
        name: newName,
        label: newName,
        transitions: newtransitions,
        apiInfo: node.info.apiInfo,
        httpBody: node.info.httpBody,
        httpMethod: node.info.httpMethod,
        url: node.info.url,
        isEnd: node.info.isEnd
      };
    }
    else if (node.info.widget.type === 'nlu') {
      if (node.info.nlu_id) {
        newNode["info"] = {
          widget: node.info.widget,
          id: newNode.id,
          name: newName,
          label: newName,
          variables: node.info.variables,
          transitions: newtransitions,
          isEnd: node.info.isEnd,
          nlu_id: node.info.nlu_id
        };
      }
      else {
        newNode["info"] = {
          widget: node.info.widget,
          id: newNode.id,
          name: newName,
          label: newName,
          variables: node.info.variables,
          transitions: newtransitions,
          isEnd: node.info.isEnd
        };
      }

    }
    else {
      newNode["info"] = {
        widget: node.info.widget,
        id: newNode.id,
        name: newName,
        label: newName,
        transitions: newtransitions,
        isEnd: node.info.isEnd
      };
    }
    flowHelper.addStateType(newNode.id, node.info.widget);
    return newNode;
  }

  renderBar() {
    const { botId } = this.state;
    return <SkillBar
      skillEngine={this.skillEngine}
      selectedSkill={this.state.selectedSkill}
      botId={botId}
      removeSkill={this.functions.removeSkill.bind(this)}
      flow={this.state.flow}
    />
  }

  getView4ConfigHistory(idx, item) {
    return <LinearLayout style={{ marginLeft: "20px", marginTop: "20px" }} orientation={'horizontal'}>
      <div style={{ width: "50px" }}>{idx + 1}</div>
      <div style={{ width: "150px" }}>{item.flow && item.flow.title ? item.flow.title : '标题'}</div>
      <div style={{ width: "150px" }}>{item.updated ? toYYYYMMDDhhmmss(item.updated) : '时间'}</div>
      <div style={{ width: "100px" }}>{item.test_result ? "pass" : "failed"}</div>
      <div style={{ width: "200px" }}>{item.flow &&
        <Button onClick={this.updateFlowDiagramByFlowHistory.bind(this, item)}>设为此版本</Button>}</div>
      <LinearLayout style={{ width: "50px" }} orientation={"horizontal"} align={"left"}>{item.flow &&
        <i className="el-icon-delete" style={{ marginLeft: "5px" }}
          onClick={this.removeHistory.bind(this, item)}></i>}</LinearLayout>
    </LinearLayout>
  }

  renderConfigHistory() {

    return (this.state.isShowConfigHistory && <DialogSelector
      title={'腳本歷史紀錄'}
      items={this.state.flowHistories}
      loading={this.state.isLoadFlowHistory ? true : false}
      getView={this.getView4ConfigHistory.bind(this)}
      visible={this.state.showConfigHistory ? true : false}
      onCancel={this.closeConfigHistory.bind(this)}
      closeDialog={this.closeConfigHistory.bind(this)}
      onSelected={this.onConfigSelected.bind(this)}
    />);
  }

  showConfigHistory(shown) {

    const { botId } = this.state;

    this.loadFlowHistory(botId);

    this.setState({
      isShowConfigHistory: shown,
    });
  }

  showProgressBox(shown) {
    this.setState({
      showDialog: shown,
    });
  }

  layout() {
    if (this.skillEngine && this.skillEngine.canvas) {
      this.skillEngine.canvas.lastChild.attributes.style.nodeValue = 'transform: scale(1) translate(5, 5); width: 100%; height: 100%;'
    }
    if (this.stateEngine && this.stateEngine.canvas) {
      this.stateEngine.canvas.lastChild.attributes.style.nodeValue = 'transform: scale(1) translate(5, 5); width: 100%; height: 100%;'
    }
  }


  loadConfig() {
    if (!confirm('確定刪除現有flow？')) {
      return;
    }
    const { flow, selectedSkill } = this.state;

    let skillIDs = flowHelper.createSkillID();
    let config = flowHelper.buildUIConfig(skillIDs);

    const updatedFlow = flowHelper.getUpdatedDiagramFlow(flow, selectedSkill, this.skillEngine, this.stateEngine);

    if (updatedFlow.id) {
      updateFlow(updatedFlow.id, config).then(result => {
        console.log("clearFlowDiagram#", result);
        location.reload();
      });
    }


  }

  renderSkillFlow() {
    const { flow, showSkillGraph } = this.state;
    return <SkillFlow
      flow={flow}
      skillEngine={this.skillEngine}
      show={showSkillGraph}
      switchSkillGraph={this.functions.switchSkillGraph.bind(this)}
    />
  }

  renderstate() {
    const { botId, selectedSkill, flow, statetoCopy, selectedState } = this.state;
    const editorWidth = '100%';
    return <StateEditor
      width={editorWidth}
      botId={botId}
      flow={flow}
      saveFlowDiagram={this.saveFlowDiagram.bind(this)}
      forceUpdateView={this.forceUpdateView.bind(this)}
      initDiagramEngine={this.initStateDiagramEngine.bind(this)}
      onStateSelected={this.onStateSelected.bind(this)}
      onNodeAdded={this.onStateNodeAdded.bind(this)}
      skill={selectedSkill}
      saveFlowDiagram={this.saveFlowDiagram.bind(this)}
      copyState={this.copyState.bind(this)}
      onNodeCopy={this.onNodeCopy.bind(this)}
      statetoCopy={statetoCopy}
      selectedState={selectedState}
      addStateLink={this.addStateLink.bind(this)}
      onLinkSelected={this.onLinkSelected.bind(this)}
      updateShowSkillLink={this.functions.updateShowSkillLink.bind(this)}
      onDiagramModelUpdated={this.onStateDiagramChanged.bind(this)} />
  }

  render() {
    const { botId, selectedSkill, flow, showDialog, statetoCopy, selectedState, showSkillGraph } = this.state;
    const editorWidth = '100%';

    return <div className="edit_frame">
      {/*{this.renderBotInfo()}*/}
      {this.renderStateBar()}
      {this.renderBar()}
      {this.renderConfigHistory()}
      {/* <Button type={"danger"} onClick={this.loadConfig.bind(this)}>匯入</Button> */}
      <SavingProgress
        show={showDialog}
      />

      <div className="fx fdc h100">
        <div id='skillDivID'></div>
        {flow && <SkillEditor
          width={editorWidth}
          botId={botId}
          flow={flow}
          forceUpdateView={this.forceUpdateView.bind(this)}
          initDiagramEngine={this.initSkillDiagramEngine.bind(this)}
          onDiagramModelUpdated={this.onSkillDiagramChanged.bind(this)}
          onNodeAdded={this.onSkillNodeAdded.bind(this)}
          saveFlowDiagram={this.saveFlowDiagram.bind(this)}
          skillEngine={this.skillEngine}
          stateEngine={this.stateEngine}
          selectedSkill={this.state.selectedSkill}
          onSkillSelected={this.onSkillSelected.bind(this)} />
        }
        <div id='stateDivID'></div>
        {!showSkillGraph && this.renderstate()}
        <div id='skillDivID'></div>
        {showSkillGraph && this.renderSkillFlow()}
        {/* <StateEditor
          width={editorWidth}
          botId={botId}
          flow={flow}
          saveFlowDiagram={this.saveFlowDiagram.bind(this)}
          forceUpdateView={this.forceUpdateView.bind(this)}
          initDiagramEngine={this.initStateDiagramEngine.bind(this)}
          onStateSelected={this.onStateSelected.bind(this)}
          onNodeAdded={this.onStateNodeAdded.bind(this)}
          skill={selectedSkill}
          saveFlowDiagram={this.saveFlowDiagram.bind(this)}
          copyState={this.copyState.bind(this)}
          onNodeCopy={this.onNodeCopy.bind(this)}
          statetoCopy={statetoCopy}
          selectedState={selectedState}
          addStateLink={this.addStateLink.bind(this)}
          onLinkSelected={this.onLinkSelected.bind(this)}
          updateShowSkillLink={this.functions.updateShowSkillLink.bind(this)}
          onDiagramModelUpdated={this.onStateDiagramChanged.bind(this)} /> */}
        {/* {this.renderSettings()} */}
      </div>
    </div>
  }
}

export default FlowEditor;
