import React from 'react';
import { flowHelper } from "./FlowHelper";
import './css/flow.less'

class SkillBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }

    componentDidMount() {
    }

    componentDidUpdate() {
        flowHelper.renderRemeveSkillNode()
    }



    remove(id) {
        this.props.removeSkill(id);
    }

    renderAll() {
        const { flow } = this.props;

        if (flow && flow.content && flow.content.skills) {
            let skills = flow.content.skills.nodes.map(node => node.id)
            return skills.map((skillNode) => {
                return <div id={'bar-skill-div-id_' + skillNode} key={skillNode} >
                    <div style={{ display: "none" }}>
                        <div className="button-more" tabIndex='-1'>
                            <div className="O"></div>
                            <div className="O"></div>
                            <div className="O"></div>
                            <div className="delete">
                                <div className="Rectangle-pop-box" onClick={this.remove.bind(this, skillNode)}>
                                    <div className="text-pop-box">刪除</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            })
        }
        else {
            return ''
        }
    }

    render() {
        return <div style={{ height: 0, overflow: 'hidden' }}>{this.renderAll()}</div>

    }

}

export default SkillBar
