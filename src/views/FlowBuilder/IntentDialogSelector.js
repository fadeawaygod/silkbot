import React from 'react';

import { Button, Checkbox, Dropdown, Dialog, Switch } from 'element-react';
import 'element-theme-default';
import { Logger } from "../../utils/Logger";
import LinearLayout from "./LinearLayout";
import { flowHelper } from "./FlowHelper";

class IntentDialogSelector extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("IntentDialogSelector");
    this.logger.log("created#");
    this.state = {
      title: props.title,
      items: props.items,
      visible: props.visible,
      onCancel: props.closeDialog,
      conditions: [],
      condition: { id: '', operation: 'intent', value: '', entities: [] },
      intents: this.props.intents,
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.logger.log("componentWillReceiveProps#", nextProps);
    this.setup(nextProps);
  }

  setup(props) {
    this.setState({
      title: props.title ? props.title : this.state.title,
      items: props.items ? props.items : this.state.items,
      visible: props.visible,
      conditions: [],
      condition: { id: '', operation: 'intent', value: '', entities: [] },
      intents: this.props.intents
    })
  }

  findIntentSlot(intent, slotName) {
    const slots = intent.slots;
    return slots.find(slot => slot.name === slotName);
  }

  getFirstPrompt(slot) {
    if (!slot || !slot.prompts || slot.prompts.length === 0) {
      return '';
    }
    return slot.prompts[0];
  }

  onEntityChanged(intent, slot) {

    const conditions = JSON.parse(JSON.stringify(this.state.conditions));
    this.logger.log("onEntityChanged# conditions:", conditions);
    this.logger.log("onEntityChanged# intent:", intent.name);
    this.logger.log("onEntityChanged# slot:", slot);

    let condition = conditions.find(c => c.id === intent.id);
    this.logger.log("onEntityChanged# condition:", condition);

    if (!condition) {
      condition = {
        id: intent.id,
        operation: "intent",
        value: intent.name,
        entities: []
      }
      conditions.push(condition);
    }

    const entityIndex = condition.entities.findIndex(e => e.entity === slot.name);
    if (entityIndex === -1) {
      condition.entities.push({ entity: slot.name, prompt: slot.prompts, id: slot.id })
    } else {
      condition.entities.splice(entityIndex, 1);
    }

    this.setState({
      conditions
    })
  }

  hasEntity(intent, slot) {
    const condition = this.state.conditions.find(c => c.id === intent.id);
    if (!condition) {
      return false;
    }

    return condition.entities && condition.entities.find(e => e.entity === slot.name) !== undefined
  }

  renderEntities(intent) {
    const slots = intent.slots;
    return slots.map((slot, idx) => {
      return <Checkbox key={`slot-${idx}`} onChange={this.onEntityChanged.bind(this, intent, slot)} checked={this.hasEntity(intent, slot)}>{slot.name}</Checkbox>
    })
  }

  onIntentClick(intent) {
    const conditions = JSON.parse(JSON.stringify(this.state.conditions));

    let condition = conditions.find(c => c.id === intent.id);
    if (!condition) {
      condition = {
        id: intent.id,
        operation: "intent",
        value: intent.name,
        entities: []
      };
      conditions.push(condition);
    } else {
      conditions.splice(conditions.findIndex(c => c.id === intent.id), 1);
    }

    this.setState({
      conditions
    })
  }

  renderItems() {
    const { intents, conditions } = this.state;
    if (!intents) {
      return '';
    }
    return intents.map((intent, idx) => {
      console.log("renderItems# intent", intent);
      const condition = conditions.find(c => c.id === intent.id);
      return <LinearLayout key={`intent-${idx}`} orientation={'vertical'} align={"left"} style={{ marginTop: "20px" }}>
        <Button type={condition ? 'success' : 'default'} onClick={this.onIntentClick.bind(this, intent)} style={{ width: "100%", marginTop: "10px", textAlign: "left" }}>{idx}. {intent.name}</Button>
        <div key={`intent-slots-${idx}`} style={{ width: "100%", marginTop: "10px" }}>
          {this.renderEntities(intent)}
        </div>
      </LinearLayout>
    })
  }

  onCancel() {
    this.props.closeDialog();
  }

  onConfirm() {
    const conditions = flowHelper.objClone(this.state.conditions);
    if (!conditions) {
      alert("conditions clone error")
      this.props.closeDialog();
      return;
    }
    console.log("!!!!conditions", conditions)

    // conditions.forEach(c => {
    //   if ('entities' in c && c.entities.length === 0) {
    //     delete c["entities"];
    //   }
    // });

    for (let i = 0; i < conditions.length; i++) {
      let c = conditions[i];
      if ('entities' in c && c.entities.length === 0) {
        delete conditions[i]["entities"];
      }
    }

    this.props.onConfirm(conditions);
    this.props.closeDialog();
  }

  render() {
    const { conditions } = this.state;
    return <Dialog
      title={this.state.title}
      size="small"
      visible={this.state.visible}
      onCancel={this.onCancel.bind(this)}
      lockScroll={false}
    >
      <Dialog.Body>
        <div style={{ minHeight: "400px", maxHeight: "500px", overflow: "scroll", padding: "30px" }}>
          {this.renderItems()}
        </div>
      </Dialog.Body>
      <Dialog.Footer className="dialog-footer">
        <Button onClick={this.onCancel.bind(this)}>取 消</Button>
        <Button type="primary" onClick={this.onConfirm.bind(this)}>确 定</Button>
      </Dialog.Footer>
    </Dialog>
  }
}

export default IntentDialogSelector;
