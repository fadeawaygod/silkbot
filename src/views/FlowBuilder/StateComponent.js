import React from 'react';
import './css/flow.less'
// import pink_question_mark from '@/assets/img/pink_question_mark.svg';
// import blue_question_mark from '@/assets/img/blue_question_mark.svg';
// import brown_question_mark from '@/assets/img/brown_question_mark.svg';
// import purple_question_mark from '@/assets/img/purple_question_mark.svg';
import red_question_mark from '@/assets/img/red_question_mark.svg';
// import end from '@/assets/img/gray_end.svg';
// import blue_check from '@/assets/img/blue_check.svg';
// import pink_check from '@/assets/img/pink_check.svg';
// import brown_check from '@/assets/img/brown_check.svg';
// import purple_check from '@/assets/img/purple_check.svg';
import state_start from '@/assets/img/state_start.svg';
import state_end from '@/assets/img/state_end.svg';
import { flowHelper } from "./FlowHelper";


class StateComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            diagramDataNodes: props.diagramDataNodes,
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        this.setup(nextProps);
    }

    setup(props) {
        this.setState({
            diagramDataNodes: props.diagramDataNodes ? props.diagramDataNodes : this.state.diagramDataNodes,
        })
    }



    componentDidMount() {
        flowHelper.renderStateSetting();
    }

    componentDidUpdate() {
        flowHelper.renderStateSetting();
    }
    renderNotSet(hasTransition, color, questionImage) {
        let cssName = 'state_box ' + color;
        if (!hasTransition) {
            return <span className={cssName} >
                <img src={questionImage} className="image" />
                <span className="end-hover">尚未設定轉移</span>
                {/* <span className="text-1">尚未設定轉移</span> */}
            </span>
        }
    }


    renderStart(isStart, color, checkImage) {
        let cssName = 'state_box ' + color;
        if (isStart) {
            return <span className={cssName} >
                <img src={checkImage} className="image" />
                <span className="end-hover">開始狀態</span>
                {/* <span className="text-1">設為開始狀態</span> */}
            </span>
        }
        else {
            return ''
        }

    }

    renderEnd(isEnd) {
        if (isEnd) {
            return <span className="state_box Rectangle-gray">
                <img src={state_end} className="image" />
                <span className="end-hover">結束狀態</span>
                {/* <span className="text-1">設為結束狀態</span> */}
            </span>
        }
        else {
            return ''
        }

    }

    renderTransition(color, checkImage) {
        return <div className={color}>
            <img src={checkImage} className="image" />
            {/* <span className="text-1">轉移到</span> */}
        </div>
    }

    renderMessage(type, text) {
        if (type === 'message') {
            return <div className='text-message'>
                {text}
            </div>
        }
        else {
            return ''
        }
    }


    renderAll() {
        let nodes = [];
        Object.values(this.state.diagramDataNodes.nodes).forEach((node) => {
            nodes.push(node)
        })
        return nodes.map((node) => {
            // if (node.info.widget.type === 'message') {

            // }
            let transition = false;
            if (node.info.transitions && node.info.transitions.length > 0) {
                transition = true;
            }
            let isStart = node.info.isStart ? node.info.isStart : false
            let isEnd = node.info.isEnd ? node.info.isEnd : false
            let type = node.info.widget.type
            let divID = '_' + node.info.id
            let color = 'Rectangle-blue'
            let checkImage = state_start
            let questionImage = red_question_mark
            if (type === 'message') {
                color = 'Rectangle-blue'
                // checkImage = blue_check
                // questionImage = blue_question_mark
            }
            else if (type === 'input') {
                color = 'Rectangle-purple'
                // checkImage = purple_check
                // questionImage = purple_question_mark
            }
            else if (type === 'nlu') {
                color = 'Rectangle-pink'
                // checkImage = pink_check
                // questionImage = pink_question_mark
            }
            else {
                color = 'Rectangle-brown'
                // checkImage = brown_check
                // questionImage = brown_question_mark
            }


            return <div id={divID} key={node.id}>
                {/* {this.renderMessage(type, node.info.text ? node.info.text : node.info.label)} */}
                {this.renderStart(isStart, color, checkImage)}
                {this.renderNotSet(transition, color, questionImage)}
                {this.renderEnd(isEnd)}
            </div>
        })
    }

    render() {
        return <div style={{ height: 0, overflow: 'hidden' }}>
            {this.renderAll()}
        </div>
    }

}

export default StateComponent;