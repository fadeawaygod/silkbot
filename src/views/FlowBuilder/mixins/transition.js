function transitionObj() {
  return {
    conditions: [],
    entity: "",
    isEnabled: true,
    next_skill: undefined,
    next_state: undefined,
  }
}
function transitionObj_operations() {
  return {
    intent: [],
    api: [],
    // slot: [], ////隱藏slot選項
    equal: [],
    includes: [],
    regex: []
  }
}
function transitionObj_render() {
  let result = {
    i: -1,//對應flow data index
    key: Number(new Date()),//重新渲染
    isEditable: false,
    isDuplicate: false,
    next_type: "",
    next_name: "",
    next_label: {
      type: "",
      name: ""
    },
    skillClass: "",
    stateClass: "",
  }
  Object.keys(new transitionObj_operations()).forEach(operation => {
    result[`${operation}_show`] = false;
    result[`${operation}_value`] = "";
  })
  return result;
}
export default {
  data() {
    return {
      transitions_self: [],
      transition_index: -1,
      transition_new: undefined,
      //intentSelection
      intentSelection_show: false,
      intentSelection_conditions: [],
      intentSelection_operation: ""
    }
  },
  computed: {
    operations() {
      return Object.keys(new transitionObj_operations());
    },
    transition_new_changed() {
      let result = false;
      if (this.stateNode) {
        const { next_type } = this.transition_new;
        if (this.transition_new[`next_${next_type}`]) {
          result = true;
        }
        if (Object.keys(new transitionObj_operations()).map(operation => this.transition_new[operation].length).reduce((a, b) => a + b) > 0) {
          result = true;
        }
      }
      return result;
    },
    transitions_changed() {
      let result = false;
      if (this.stateNode && this.transitions_self.length) {
        this.transitions_self.filter(transition_self => transition_self.isEditable).forEach(transition_self => {
          if (JSON.stringify(this.parse_transition("output", transition_self)) !== JSON.stringify(this.stateNode.info.transitions[transition_self.i])) {
            result = true;
          }
        })
      }
      return result;
    }
  },
  methods: {
    selectNextNode(type, transition, node) {
      if (type === "skill") {
        transition.next_skill = node.info.id;
        transition.next_state = undefined;
        transition.skillClass = "focus";
        transition.stateClass = "unfocus";
      }
      else if (type === "state") {
        transition.next_skill = undefined;
        transition.next_state = node.info.id;
        transition.skillClass = "unfocus";
        transition.stateClass = "focus";
      }
      transition.next_type = type;
      transition.next_name = node.info.name;
    },
    deleteCondition(transition, operation, conditionIndex) {
      transition[operation].splice(conditionIndex, 1)
      transition[`${operation}_show`] = !!transition[operation].length;
    },
    event_transition_new(type, operation) {
      if (type === "init") {
        this.transition_new = Object.assign(
          new transitionObj(),
          new transitionObj_render(),
          new transitionObj_operations()
        )
      }
      else if (type === "selectOperation") {
        if (["api", "intent"].includes(operation)) {
          this.event_intentConditions("edit", operation, {});
        }
        else if (["slot", "equal", "includes", "regex",].includes(operation)) {
          this.transition_new[`${operation}_show`] = true;
        }
      }
      else if (type === "editOperation") {
        if (["api", "intent"].includes(operation)) {
          this.event_intentConditions("edit", operation, {});
        }
      }
      else if (type === "deleteOperation") {
        this.$root.confirm(this.$tp(`conditionTypes.${operation}`)).then(() => {
          this.transition_new[operation].splice(0);
          this.transition_new[`${operation}_show`] = false;
        })
      }
      else if (type === "updateToFlow") {//新增
        const { next_type } = this.transition_new;
        if (!this.transition_new[`next_${next_type}`]) {
          this.$root.m_error(this.$tp("warn.shouldChooseTarnsitionTarget"));
          return false;
        }
        if (Object.keys(new transitionObj_operations()).map(operation => this.transition_new[operation].length).reduce((a, b) => a + b) === 0) {
          this.$root.m_error(this.$tp("warn.shouldaddConditions"));
          return false;
        }
        let targetIndex = this.transitions_self.map(t => t[`next_${next_type}`]).indexOf(this.transition_new[`next_${next_type}`]);
        if (targetIndex > -1) {
          for (let operation in new transitionObj_operations()) {
            this.transition_new[operation].forEach(condition_new => {
              if (["api", "intent"].includes(operation)) {
                if (this.transitions_self[targetIndex][operation].filter(condition_self => condition_self.id === condition_new.id).length) return false;
              }
              else {
                if (this.transitions_self[targetIndex][operation].filter(condition_self => condition_self.value === condition_new.value).length) return false;
              }
              this.transitions_self[targetIndex][operation].push(condition_new);
            })
            this.transitions_self[targetIndex][`${operation}_show`] = !!this.transitions_self[targetIndex][operation].length;
          }
          let transition_self = this.transitions_self[targetIndex];
          let originIndex = transition_self.i
          this.transitions_self[targetIndex].conditions = this.parse_transition("output", transition_self).conditions;
          this.stateNode.info.transitions.splice(originIndex, 1, this.parse_transition("output", transition_self));
        }
        else {
          let transition_self = this.transition_new;
          this.transitions_self.push(this.parse_transition("input", this.parse_transition("output", transition_self), this.transitions_self.length));
          if (!this.stateNode.info.transitions) this.stateNode.info.transitions = [];
          this.stateNode.info.transitions.push(this.parse_transition("output", transition_self));
        }

        this.flowData.functions.setState(this.stateNode.id);

        this.event_transition_new("init");
      }
    },
    addCondition(transition, operation) {
      let value = transition[`${operation}_value`];
      if (!value) return false;
      transition[operation].unshift({ value });
      transition[`${operation}_value`] = "";
    },
    event_transitions(type, index = -1) {
      let transition_self = this.transitions_self[index];
      let originIndex = transition_self === undefined ? -1 : transition_self.i;
      if (type === "getFromFlow") {
        this.transitions_self = (this.stateNode.info.transitions || []).map(
          (transition, index) => {
            return this.parse_transition("input", transition, index);
          }
        );
      }
      else if (type === "updateToFlow") {
        this.transitions_self[index].conditions = this.parse_transition("output", transition_self).conditions;

        this.stateNode.info.transitions[transition_self.i] = this.parse_transition("output", transition_self);
        this.flowData.functions.setState(this.stateNode.id);

        this.transitions_self[index].isEditable = false;
      }
      else if (type === "cancelEditMode") {
        this.event_transitions("reset", index);
        this.transitions_self[index].isEditable = false;
      }
      else if (type === "delete") {
        this.$root.confirm(`${transition_self.next_label.type}:${transition_self.next_label.name}`).then(() => {
          this.transitions_self.splice(index, 1);
          this.stateNode.info.transitions.splice(originIndex, 1);
          this.flowData.functions.setState(this.stateNode.id);
        })
      }
      else if (type === "reset") {
        this.transitions_self.splice(index, 1,
          this.parse_transition("input", transition_self, index)
        );
      }
      else if (type === "duplicate") {
        this.transitions_self.splice(index + 1, 0, Object.assign(
          JSON.parse(JSON.stringify(transition_self)),
          new transitionObj_render(),
        ));
        this.transitions_self[index + 1].isDuplicate = true;
        this.transitions_self[index + 1].isEditable = true;

        for (let operation in new transitionObj_operations()) {
          this.transitions_self[index + 1][`${operation}_show`] = !!this.transitions_self[index + 1][operation].length;
        }
        this.transitions_self[index + 1].next_skill = this.transitions_self[index + 1].next_state = undefined;
      }
    },
    parse_transition(type, transition, index) {
      if (type === "input") {
        let result = Object.assign(
          new transitionObj(),
          JSON.parse(JSON.stringify(transition)),
          new transitionObj_render(),
          new transitionObj_operations()
        )
        result.i = index;
        let targetNode;
        if (result.next_skill) {
          result.next_type = "skill";
          targetNode = this.flowData.flow.content.skills.nodes.find(x => x.id === result.next_skill);
        }
        else if (result.next_state) {
          result.next_type = "state";
          targetNode = this.stateNodes.find(x => x.id === result.next_state);
        }
        if (targetNode) {
          result.key += targetNode.id;
          this.selectNextNode(result.next_type, result, targetNode);
          result.next_label = {
            type: targetNode.name,
            name: targetNode.info.name
          };
        }

        for (let condition of transition.conditions) {
          if (result.hasOwnProperty(condition.operation)) {
            result[condition.operation].push({
              id: condition.id,
              value: condition.value,
              slot: condition.slot,
              entities: condition.entities
            });
          }
        }
        for (let operation in new transitionObj_operations()) {
          result[`${operation}_show`] = !!result[operation].length;
        }
        return result;
      }
      else if (type === "output") {
        let result = JSON.parse(JSON.stringify(transition));
        result.conditions = [];
        for (let operation in new transitionObj_operations()) {
          transition[operation].forEach(_condition => {
            let condition = {
              operation: operation,
              value: _condition.value
            };
            if (_condition.entities) condition.entities = _condition.entities;
            if (_condition.id) condition.id = _condition.id;
            if (_condition.slot) condition.slot = _condition.slot;
            result.conditions.push(condition);
          })
        }
        for (let key in new transitionObj_render()) {
          delete result[key];
        }
        for (let key in new transitionObj_operations()) {
          delete result[key];
        }

        if (!result.next_skill) delete result.next_skill;
        if (!result.next_statenext_state) delete result.next_statenext_state;

        return result;
      }
    },
    event_intentConditions(type, operation, { transition_index, conditions_set }) {
      this.intentSelection_operation = operation;
      this.transition_index = transition_index;

      let transition_self;
      if (this.transition_index !== undefined) {
        transition_self = this.transitions_self[this.transition_index];
      }
      else {
        transition_self = this.transition_new;
      }

      if (type === "edit") {
        this.intentSelection_conditions = transition_self[operation];
        this.intentSelection_show = true;
      }
      else if (type === "delete") {
        this.$root.confirm(this.$tp(`conditionTypes.${operation}`)).then(() => {
          transition_self[operation].splice(0);
          transition_self[`${operation}_show`] = false;
        })
      }
      else if (type === "set") {
        if (this.transition_index === undefined) {
          this.transition_new[`${operation}_show`] = conditions_set.length;
        }
        else {
          this.transitions_self[this.transition_index][`${operation}_show`] = conditions_set.length;
        }
        this.intentSelection_conditions.splice(0, this.intentSelection_conditions.length, ...conditions_set);
        this.intentSelection_show = false;
      }
      else if (type === "selectOperation") {
        if (["api", "intent"].includes(operation)) {
          this.event_intentConditions("edit", operation, { transition_index });
        }
        else if (["slot", "equal", "includes", "regex",].includes(operation)) {
          transition_self[`${operation}_show`] = true;
        }
      }
    },
    event_duplicate(type, index) {
      if (type === "updateToFlow") {//新增
        const { next_type } = this.transitions_self[index];
        if (!this.transitions_self[index][`next_${next_type}`]) {
          this.$root.m_error(this.$tp("warn.shouldChooseTarnsitionTarget"));
          return false;
        }
        if (Object.keys(new transitionObj_operations()).map(operation => this.transitions_self[index][operation].length).reduce((a, b) => a + b) === 0) {
          this.$root.m_error(this.$tp("warn.shouldaddConditions"));
          return false;
        }
        let selfIndex = this.transitions_self.filter(x => !x.isDuplicate).map(t => t[`next_${next_type}`]).indexOf(this.transitions_self[index][`next_${next_type}`]);
        let transition_self;
        if (selfIndex > -1) {
          for (let operation in new transitionObj_operations()) {
            this.transitions_self[index][operation].forEach(condition_new => {
              if (["api", "intent"].includes(operation)) {
                if (this.transitions_self[selfIndex][operation].filter(condition_self => condition_self.id === condition_new.id).length) return false;
              }
              else {
                if (this.transitions_self[selfIndex][operation].filter(condition_self => condition_self.value === condition_new.value).length) return false;
              }
              this.transitions_self[selfIndex][operation].push(condition_new);
            })
            this.transitions_self[selfIndex][`${operation}_show`] = !!this.transitions_self[selfIndex][operation].length;
          }
          transition_self = this.transitions_self[selfIndex];
          let originIndex = transition_self.i
          this.transitions_self[selfIndex].conditions = this.parse_transition("output", transition_self).conditions;
          this.transitions_self.splice(index, 1);
          this.stateNode.info.transitions.splice(originIndex, 1, this.parse_transition("output", transition_self));
        }
        else {
          this.transitions_self[index].isDuplicate = false;
          this.transitions_self.filter(x => !x.isDuplicate).forEach((el, i) => {
            this.transitions_self[i].i = i;
          })
          transition_self = this.transitions_self[index];
          this.transitions_self.splice(transition_self.i, 1, this.parse_transition("input", this.parse_transition("output", transition_self), transition_self.i));
          if (!this.stateNode.info.transitions) this.stateNode.info.transitions = [];
          this.stateNode.info.transitions.splice(transition_self.i, 0, this.parse_transition("output", transition_self));
        }

        this.flowData.functions.setState(this.stateNode.id);
      }
      else if (type === "cancel") {
        this.transitions_self.splice(index, 1);
      }
    }
  }
}