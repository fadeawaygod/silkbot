import { mapState } from "vuex";
export default {
  computed: {
    ...mapState(["nluEndpoints"])
  },
  methods: {
    event_nluEndpoints(type, nluEndpoint) {
      if (type === "set") {
        this.stateNode.info.nlu_id = nluEndpoint.id;
        this.stateNode.info.nlu_name = nluEndpoint.nlu_name;

        this.flowData.functions.setState(this.stateNode.id);
      }
      else if (type === "setDefault") {
        this.stateNode.info.nlu_id = "";
        this.stateNode.info.nlu_name = "";

        this.flowData.functions.setState(this.stateNode.id);
      }
    }
  }
}