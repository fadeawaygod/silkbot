export default {
  data() {
    return {
      variablesEditable: false,
      variables_self: []
    }
  },
  methods: {
    event_variables(type) {
      if (type === "init") {
        if (!this.stateNode.info.hasOwnProperty("variables")) {
          this.stateNode.info.variables = [{
            alias: "",
            entity: "",
            prompt: []
          }];
        }
        else {
          if (!Array.isArray(this.stateNode.info.variables)) {
            let _variables = [];
            Object.keys(this.stateNode.info.variables).forEach(key => {
              let value = this.stateNode.info.variables[key];
              _variables.push({
                alias: key,
                entity: value,
                prompt: []
              })
            })
            this.stateNode.info.variables = _variables;
          }
          if (!this.stateNode.info.variables.length) {
            this.stateNode.info.variables.push({
              alias: "",
              entity: "",
              prompt: []
            })
          }
        }
        this.variables_self.splice(0, this.variables_self.length, ...this.event_variables("input"));
      }
      else if (type === "edit") {
        if (this.variables_self.slice(0, -1).map(x => !!x.alias && !!x.entity).includes(false)) {
          this.$root.m_error(this.$tp("warn.inputsShouldNotBeEmpty"));
          return false;
        }
        this.stateNode.info.variables = this.event_variables("output");
        this.flowData.functions.saveFlowDiagram();
        this.variablesEditable = false;
      }
      else if (type === "cancel") {
        this.variables_self.splice(0, this.variables_self.length, ...this.event_variables("input"));
        this.variablesEditable = false;
      }
      else if (type === "input") {
        return JSON.parse(JSON.stringify(this.stateNode.info.variables));
      }
      else if (type === "output") {
        return JSON.parse(JSON.stringify(this.variables_self.slice(0, -1)));
      }
    },
  }
}