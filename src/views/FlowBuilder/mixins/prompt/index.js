let carouselObj = {
  create(type) {
    switch (type) {
      case "column":
        return {
          title: "",
          sub_title: "",
          image_url: "",
          buttons: []
        }
      case "button":
        return {
          type: "postback", title: "", payload: ""
        }
      case "show":
        return {
          loading_uploadImage: false,
          create: false,
          list: []
        }
    }
  },
  createShow(buttonCount) {
    let _show = this.create("show");
    _show.list = new Array(buttonCount).fill(false);
    return _show;
  },
  createColumn() {
    let _column = this.create("column");
    return _column;
  }
}
import { getConfig } from "@/settings";
//api
import { pocFile } from "@/api/Chat";
const { host_pocFile } = getConfig();
export default {
  data() {
    return {
      saveFlow: false,
      textPrompt: {
        message: "",
        tmpMessage: "",
        select: -1,
        repeat_conditions: {
          h: "",
          m: "",
          s: "",
          count: 0
        },
      },
      confirmPrompt: undefined,
      menuPrompt: undefined,
      menuPrompt_button: {
        type: "postback", title: "", payload: ""
      },
      menuPrompt_show: {
        loading_uploadImage: false,
        create: false,
        list: []
      },
      carouselPrompt: undefined,
      currentCarouselPrompt: 0,//index
      carouselPrompt_button: {
        type: "postback", title: "", payload: ""
      },
      carouselPrompt_show: [
        // {
        //   create: false,
        //   list: []
        // }
      ],
      imagemapPrompt: undefined,
      imagemapPrompt_button: {
        type: "text", title: "", payload: "",
        area: {
          x: 0,
          y: 0,
          width: 0,
          height: 0
        }
      },
      imagemapPrompt_show: {
        loading_uploadImage: false,
        create: false,
        list: []
      }
    }
  },
  computed: {
    promptType() {
      return (this.stateNode.info.tprompt || {}).type;
    },
    confirmPrompt_check() {
      let result = false;
      if (this.stateNode && this.stateNode.info.tprompt && this.promptType === "confirm") {
        if (JSON.stringify(this.confirmPrompt) !== JSON.stringify(this.stateNode.info.tprompt)) {
          result = true;
        }
      }
      return result;
    },
    menuPrompt_content_check() {
      let result = false;
      if (this.stateNode && this.stateNode.info.tprompt && this.promptType === "menu") {
        if (this.menuPrompt.image_url !== this.stateNode.info.tprompt.image_url ||
          this.menuPrompt.title !== this.stateNode.info.tprompt.title ||
          this.menuPrompt.sub_title !== this.stateNode.info.tprompt.sub_title) {
          result = true;
        }
      }
      return result;
    }
  },
  methods: {
    promptObj(type) {
      switch (type) {
        case "text":
          return {
            type: "text",
            items: [
              // { message: "", repeated: false }
            ],
            repeat_conditions: { duration: 0, count: 0 }
          }
        case "confirm":
          return {
            type: "confirm",
            title: this.$tp("label.confirmTemplateMessage"),
            buttons: [
              { type: "text", title: this.$tp("label.yes"), payload: this.$tp("label.yes") },
              { type: "text", title: this.$tp("label.no"), payload: this.$tp("label.no") }
            ]
          }
        case "menu":
          return {
            type: "menu",
            title: this.$tp("label.title"),
            sub_title: this.$tp("label.content"),
            image_url: "",
            buttons: [
              // { type: "postback", title: "", payload: "" }
            ]
          }
        case "carousel":
          return {
            type: "carousel",
            title: "",
            column: [
              {
                title: this.$tp("label.title"),
                sub_title: this.$tp("label.content"),
                image_url: "",
                buttons: [
                  //  { type: "postback", title: "", payload: "" }
                ]
              }
            ]
          }
        case "imagemap":
          return {
            type: "imagemap",
            title: "",
            image_url: "",
            baseSize: {
              width: 0,
              height: 0
            },
            buttons: [
              // {
              //     type: "",
              //     title: "",
              //     payload: "",
              //     area: {
              //         x: 0,
              //         y: 0,
              //         width: 0,
              //         height: 0
              //     }
              // }
            ]
          }
      }
    },
    getImageInfo(e) {
      const _target = "target";
      if (!e[_target].files.length) return false;
      //FormData
      let formData = new FormData();
      formData.append("file_to_upload", e[_target].files[0]);
      formData.append("is_compress", 0);
      //get file
      let extension = e[_target].files[0].name.replace(/^.*\./gm, "");
      const reader = new FileReader();
      reader.onload = e2 => {

      };
      reader.readAsDataURL(e[_target].files[0]);
      e[_target].value = "";
      return {
        formData: formData,
        extension: extension
      };
    },
    initPrompt(tprompt) {
      let _tprompt = (JSON.stringify(tprompt) || "").isJSON() ?
        JSON.parse(JSON.stringify(tprompt)) :
        undefined;
      let promptType = (tprompt || {}).type;
      if (promptType === "text") {
        this.textPrompt.message = "";
        //重複句
        this.textPrompt.repeat_conditions = { h: "", m: "", s: "", count: 0 };
        if (tprompt && tprompt.repeat_conditions) {
          this.textPrompt.repeat_conditions.h =
            Math.floor(tprompt.repeat_conditions.duration / 3600) || "";
          this.textPrompt.repeat_conditions.m =
            Math.floor((tprompt.repeat_conditions.duration % 3600) / 60) ||
            "";
          this.textPrompt.repeat_conditions.s =
            Math.floor((tprompt.repeat_conditions.duration % 3600) % 60) ||
            "";
          this.textPrompt.repeat_conditions.count = tprompt.repeat_conditions.count;
          this.$forceUpdate();
        }
      }
      else if (promptType === "confirm") {
        this.confirmPrompt = _tprompt;
      }
      else if (promptType === "menu") {
        this.menuPrompt = _tprompt;
        this.menuPrompt_show.list = new Array(_tprompt.buttons.length).fill(false);
      }
      else if (promptType === "carousel") {
        this.currentCarouselPrompt = 0;
        this.carouselPrompt = _tprompt;
        _tprompt.column.forEach(column => {
          this.carouselPrompt_show.push(carouselObj.createShow(column.buttons.length));
        })
      }
      else if (promptType === "imagemap") {
        this.imagemapPrompt = _tprompt;
        this.imagemapPrompt_show.list = new Array(_tprompt.buttons.length).fill(false);
      }
    },
    initPrompt_flowData(flowData) {
      Object.values(flowData.flow.content.states).forEach(state => {
        state.nodes.forEach(stateNode => {
          //提示狀態
          if (stateNode.info.widget.type === "message") {
            //prompt
            if (stateNode.info.hasOwnProperty("tprompt")) {
              //tprompt
            }
            else {
              this.saveFlow = true;
              //prompts
              if (!stateNode.info.hasOwnProperty("prompts")) {
                let prompt = (stateNode.info.text || stateNode.info.label || "").trim();
                if (prompt) stateNode.info.prompts = [prompt];
              }
              if (stateNode.info.hasOwnProperty("prompts")) {
                stateNode.info.tprompt = this.promptObj("text");
                stateNode.info.prompts.forEach(message => {
                  if (typeof message === "string") {
                    stateNode.info.tprompt.items.push({
                      message: message,
                      repeated: false
                    });
                  }
                  else if (typeof message === "object") {
                    stateNode.info.tprompt.items.push(message);
                  }
                });
              }
            }
            if (stateNode.info.tprompt) {
              stateNode.info.tprompts = Object.assign(
                stateNode.info.tprompts || {},
                {
                  [stateNode.info.tprompt.type]: stateNode.info.tprompt
                }
              )
            }
          }
        })
      })
      return flowData;
    },
    setPromptType(promptType) {
      if (promptType === this.promptType) return false;
      this.setPrompt(promptType);
    },
    setPrompt(promptType) {
      if (!this.stateNode.info.hasOwnProperty("tprompts")) {
        this.$set(this.stateNode.info, "tprompts", {});
      }
      //切換前先將當前tprompt存入tprompts
      if (this.stateNode.info.tprompt) {
        this.$set(this.stateNode.info.tprompts, this.promptType, this.stateNode.info.tprompt);
      }
      //
      let _promptObj = undefined;
      if (this.stateNode.info.tprompts.hasOwnProperty(promptType)) {
        _promptObj = this.stateNode.info.tprompts[promptType];
      }
      else {
        _promptObj = this.promptObj(promptType);
        if (promptType === "text") { }
        else if (promptType === "confirm") { }
        else if (promptType === "menu") { }
        else if (promptType === "carousel") { }
        else if (promptType === "imagemap") { }
      }
      this.initPrompt(_promptObj);
      this.$set(this.stateNode.info, "tprompt", JSON.parse(JSON.stringify(_promptObj)));
      this.flowData.functions.setState(this.stateNode.id);
    },
    //textPrompt
    add_textPrompt(message) {
      this.stateNode.info.tprompt.items.push({
        message: message,
        repeated: false
      });
      this.flowData.functions.setState(this.stateNode.id);
    },
    event_textPrompt(type, index, prompt) {
      if (type === "input") {
        if (this.textPrompt.message === "") return false;

        this.add_textPrompt(this.textPrompt.message);

        this.textPrompt.message = "";
      }
      else if (type === "editMode") {
        this.textPrompt.select = index;
        this.textPrompt.tmpMessage = prompt.message;
      }
      else if (type === "edit") {
        this.stateNode.info.tprompt.items[
          this.textPrompt.select
        ].message = this.textPrompt.tmpMessage;
        this.event_textPrompt("closeEditMode");

        this.flowData.functions.setState(this.stateNode.id);
      }
      else if (type === "closeEditMode") {
        this.textPrompt.select = -1;
        this.textPrompt.tmpMessage = "";
      }
      else if (type === "delete") {
        this.$root.confirm(prompt.message).then(() => {
          this.stateNode.info.tprompt.items.splice(index, 1);
          this.flowData.functions.setState(this.stateNode.id);
          this.$forceUpdate();
        });
      }
    },
    checkRepeat_conditions() {
      for (let key in this.textPrompt.repeat_conditions) {
        this.textPrompt.repeat_conditions[key] = this.textPrompt.repeat_conditions[key]
          .toString()
          .replace(/[^0-9]/gm, "");
      }
    },
    updateRepeat_conditions() {
      const { h, m, s } = this.textPrompt.repeat_conditions;

      this.$set(this.stateNode.info.tprompt, "repeat_conditions", {
        duration: Number(h || 0) * 3600 + Number(m || 0) * 60 + Number(s || 0),
        count: this.textPrompt.repeat_conditions.count || 0
      });

      this.flowData.functions.setState(this.stateNode.id);
    },
    //confirmPrompt
    event_confirmPrompt(type) {
      if (type === "edit") {
        if (!this.confirmPrompt.title ||
          !this.confirmPrompt.buttons.map(button => !!button.title && !!button.payload).every(x => x === true)) {
          this.$root.m_error(this.$tp("warn.inputsShouldNotBeEmpty"));
          return false;
        }
        this.stateNode.info.tprompt = JSON.parse(JSON.stringify(this.confirmPrompt));
        this.flowData.functions.setState(this.stateNode.id);
      }
      else if (type === "cancel") {
        this.confirmPrompt = JSON.parse(JSON.stringify(this.stateNode.info.tprompt));
      }
    },
    //menuPrompt
    event_menuPrompt(type, index) {
      if (type === "editInput") {
        if (!this.menuPrompt.title) {
          this.$root.m_error(this.$tp("please.enter", { name: this.$tp("label.title") }));
          return false;
        }
        if (!this.menuPrompt.sub_title) {
          this.$root.m_error(this.$tp("please.enter", { name: this.$tp("label.content") }));
          return false;
        }
        this.stateNode.info.tprompt.image_url = this.menuPrompt.image_url;
        this.stateNode.info.tprompt.title = this.menuPrompt.title;
        this.stateNode.info.tprompt.sub_title = this.menuPrompt.sub_title;
        this.flowData.functions.setState(this.stateNode.id);
      }
      else if (type === "cancelInput") {
        this.menuPrompt.image_url = this.stateNode.info.tprompt.image_url;
        this.menuPrompt.title = this.stateNode.info.tprompt.title;
        this.menuPrompt.sub_title = this.stateNode.info.tprompt.sub_title;
      }
      else if (type === "deleteButton") {
        this.$root.confirm(this.menuPrompt.buttons[index].title).then(() => {
          this.menuPrompt.buttons.splice(index, 1);
          this.menuPrompt_show.list.splice(index, 1);
          this.stateNode.info.tprompt.buttons.splice(index, 1);
          this.flowData.functions.setState(this.stateNode.id);
        })
      }
      else if (type === "cancelButton") {
        this.menuPrompt.buttons.splice(index, 1, JSON.parse(JSON.stringify(this.stateNode.info.tprompt.buttons[index])));
        this.menuPrompt_show.list.splice(index, 1, false);
      }
      else if (type === "editButton") {
        this.stateNode.info.tprompt.buttons[index] = JSON.parse(JSON.stringify(this.menuPrompt.buttons[index]));
        this.flowData.functions.setState(this.stateNode.id);

        this.menuPrompt_show.list.splice(index, 1, false);
      }
      else if (type === "selectButton") {
        if (this.menuPrompt_show.list[index]) {
          this.menuPrompt_show.list = new Array(this.menuPrompt_show.list.length).fill(false)
        }
        else {
          this.menuPrompt_show.list = new Array(this.menuPrompt_show.list.length).fill(false)
          this.menuPrompt_show.list.splice(index, 1, true);
        }
      }
    },
    event_menuPrompt_button(type) {
      if (type === "init") {
        this.menuPrompt_button = {
          type: "postback", title: "", payload: ""
        }
        this.menuPrompt_show.create = false;
      }
      else if (type === "create") {
        const { type, title, payload } = this.menuPrompt_button;
        if (!title) {
          this.$root.m_error(this.$tp("please.enter", { name: "title" }))
          return false;
        }
        if (!payload) {
          this.$root.m_error(this.$tp("please.enter", { name: "payload" }))
          return false;
        }
        const button = {
          type: type,
          title: title,
          payload: payload
        }
        this.menuPrompt.buttons.push(button);
        this.menuPrompt_show.list.push(false);

        this.stateNode.info.tprompt.buttons.push(JSON.parse(JSON.stringify(button)));
        this.flowData.functions.setState(this.stateNode.id);

        this.event_menuPrompt_button("init");
      }
    },
    //carouselPrompt
    event_carouselPrompt(type, index, buttonIndex) {
      if (type === "createColumn") {
        this.carouselPrompt_show.push(carouselObj.createShow(0));
        this.carouselPrompt.column.push(carouselObj.createColumn());
        this.stateNode.info.tprompt.column.push(carouselObj.createColumn());
        this.flowData.functions.setState(this.stateNode.id);

        this.currentCarouselPrompt = this.carouselPrompt.column.length - 1;
      }
      else if (type === "deleteColumn") {
        this.carouselPrompt.column.splice(index, 1);
        if (this.currentCarouselPrompt > this.carouselPrompt.column.length - 1) {
          this.currentCarouselPrompt -= 1;
        }
        this.stateNode.info.tprompt.column.splice(index, 1);
        this.flowData.functions.setState(this.stateNode.id);
      }
      else if (/^editInput_/.test(type)) {
        let inputName = type.replace("editInput_", "");
        // if (!this.carouselPrompt.column[index][inputName]) {
        //   return false;
        // }
        this.stateNode.info.tprompt.column[index][inputName] = this.carouselPrompt.column[index][inputName];
        this.flowData.functions.setState(this.stateNode.id);
      }
      else if (type === "deleteButton") {
        this.$root.confirm(this.carouselPrompt.column[index].buttons[buttonIndex].title).then(() => {
          this.carouselPrompt.column[index].buttons.splice(buttonIndex, 1);
          this.carouselPrompt_show[index].list.splice(buttonIndex, 1);
          this.stateNode.info.tprompt.column[index].buttons.splice(buttonIndex, 1);
          this.flowData.functions.setState(this.stateNode.id);
        })
      }
      else if (type === "cancelButton") {
        this.carouselPrompt.column[index].buttons.splice(buttonIndex, 1, JSON.parse(JSON.stringify(this.stateNode.info.tprompt.column[index].buttons[buttonIndex])));
        this.carouselPrompt_show[index].list.splice(buttonIndex, 1, false);
      }
      else if (type === "editButton") {
        this.stateNode.info.tprompt.column[index].buttons[buttonIndex] = JSON.parse(JSON.stringify(this.carouselPrompt.column[index].buttons[buttonIndex]));
        this.flowData.functions.setState(this.stateNode.id);

        this.carouselPrompt_show[index].list.splice(buttonIndex, 1, false);
      }
      else if (type === "selectButton") {
        if (this.carouselPrompt_show[index].list[buttonIndex]) {
          this.carouselPrompt_show[index].list = new Array(this.carouselPrompt_show[index].list.length).fill(false)
        }
        else {
          this.carouselPrompt_show[index].list = new Array(this.carouselPrompt_show[index].list.length).fill(false)
          this.carouselPrompt_show[index].list.splice(buttonIndex, 1, true);
        }
      }
    },
    event_carouselPrompt_button(type, index) {
      if (type === "init") {
        this.carouselPrompt_button = {
          type: "postback", title: "", payload: ""
        }
        this.carouselPrompt_show[index].create = false;
      }
      else if (type === "create") {
        const { type, title, payload } = this.carouselPrompt_button;
        if (!title) {
          this.$root.m_error(this.$tp("please.enter", { name: "title" }))
          return false;
        }
        if (!payload) {
          this.$root.m_error(this.$tp("please.enter", { name: "payload" }))
          return false;
        }
        const button = {
          type: type,
          title: title,
          payload: payload
        }
        this.carouselPrompt.column[index].buttons.push(button);
        this.carouselPrompt_show[index].list.push(false);

        this.stateNode.info.tprompt.column[index].buttons.push(JSON.parse(JSON.stringify(button)));
        this.flowData.functions.setState(this.stateNode.id);

        this.event_carouselPrompt_button("init", index);
      }
    },
    uploadImge_carouselPrompt(e, index) {
      this.carouselPrompt_show[index].loading_uploadImage = true;
      const { formData, extension } = this.getImageInfo(e);
      pocFile(formData).then(res => {
        this.carouselPrompt.column[index].image_url = `${host_pocFile}${res.file_name}`;
        this.event_carouselPrompt('editInput_image_url', index);
      }).finally(() => {
        this.carouselPrompt_show[index].loading_uploadImage = false;
      })
    },
    //imagemapPrompt
    event_imagemapPrompt(type, index) {
      if (/^editInput_/.test(type)) {
        let inputName = type.replace("editInput_", "");
        // if (!this.imagemapPrompt[inputName]) {
        //   return false;
        // }
        if (inputName === "image_url" && !this.imagemapPrompt.image_url) {
          this.event_imagemapPrompt("imageOnerror");
        }
        this.stateNode.info.tprompt[inputName] = this.imagemapPrompt[inputName];
        this.flowData.functions.setState(this.stateNode.id);
      }
      else if (type === "imageOnload") {
        this.imagemapPrompt.baseSize.height = document.getElementById("imagemapImage").naturalHeight;
        this.imagemapPrompt.baseSize.width = document.getElementById("imagemapImage").naturalWidth;
        this.event_imagemapPrompt('editInput_baseSize');
      }
      else if (type === "imageOnerror") {
        this.imagemapPrompt.baseSize.height = 0;
        this.imagemapPrompt.baseSize.width = 0;
        this.event_imagemapPrompt('editInput_baseSize');
      }
      else if (type === "deleteButton") {
        this.$root.confirm(this.imagemapPrompt.buttons[index].title).then(() => {
          this.imagemapPrompt.buttons.splice(index, 1);
          this.imagemapPrompt_show.list.splice(index, 1);
          this.stateNode.info.tprompt.buttons.splice(index, 1);
          this.flowData.functions.setState(this.stateNode.id);
        })
      }
      else if (type === "cancelButton") {
        this.imagemapPrompt.buttons.splice(index, 1, JSON.parse(JSON.stringify(this.stateNode.info.tprompt.buttons[index])));
        this.imagemapPrompt_show.list.splice(index, 1, false);
      }
      else if (type === "editButton") {
        this.stateNode.info.tprompt.buttons[index] = JSON.parse(JSON.stringify(this.imagemapPrompt.buttons[index]));
        this.flowData.functions.setState(this.stateNode.id);

        this.imagemapPrompt_show.list.splice(index, 1, false);
      }
      else if (type === "selectButton") {
        if (this.imagemapPrompt_show.list[index]) {
          this.imagemapPrompt_show.list = new Array(this.imagemapPrompt_show.list.length).fill(false)
        }
        else {
          this.imagemapPrompt_show.list = new Array(this.imagemapPrompt_show.list.length).fill(false)
          this.imagemapPrompt_show.list.splice(index, 1, true);
        }
      }
    },
    event_imagemapPrompt_button(type) {
      if (type === "init") {
        this.imagemapPrompt_button = {
          type: "postback", title: "", payload: "",
          area: {
            x: 0,
            y: 0,
            width: 0,
            height: 0
          }
        }
        this.imagemapPrompt_show.create = false;
      }
      else if (type === "create") {
        const { type, title, payload, area } = this.imagemapPrompt_button;
        if (!title) {
          this.$root.m_error(this.$tp("please.enter", { name: "title" }))
          return false;
        }
        if (!payload) {
          this.$root.m_error(this.$tp("please.enter", { name: "payload" }))
          return false;
        }
        const button = {
          type: type,
          title: title,
          payload: payload,
          area: area
        }
        this.imagemapPrompt.buttons.push(button);
        this.imagemapPrompt_show.list.push(false);

        this.stateNode.info.tprompt.buttons.push(JSON.parse(JSON.stringify(button)));
        this.flowData.functions.setState(this.stateNode.id);

        this.event_imagemapPrompt_button("init");
      }
    },
    uploadImge_imagemapPrompt(e) {
      this.imagemapPrompt_show.loading_uploadImage = true;
      const { formData, extension } = this.getImageInfo(e);
      if (!/jpeg|png/.test(extension)) {
        this.$root.m_error("仅支持jpeg或png类型图片");
        return false;
      }
      pocFile(formData).then(res => {
        this.imagemapPrompt.image_url = `${host_pocFile}${res.file_name}`;
        this.event_imagemapPrompt('editInput_image_url');
      }).finally(() => {
        this.imagemapPrompt_show.loading_uploadImage = false;
      })
    },
  },
  mounted() {
    this.ccc();
  }
}