import React from 'react';
import './css/flow.less'
import { flowHelper } from "./FlowHelper";

class SkillComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            flow: props.flow,
        }
    }

    componentDidMount() {
        flowHelper.renderSkillSetting();
    }

    componentDidUpdate() {
        flowHelper.renderSkillSetting();
    }

    renderNoSetting(sum) {
        if (sum === 0) {
            return <span className='status_box status-count-question'>
                <div className='count-text'>
                    {'?'}
                </div>
                <span className="question-text">尚无状态设定</span>
            </span>
        }
        return ''
    }

    renderSetNotDone(hasNoSet) {
        if (!hasNoSet) {
            return <span className='status_box status-not-set'>
                <div className='count-text'>
                    {'i'}
                </div>
                <span className="no-set-text">尚无开始状态</span>
            </span>
        }
        return ''
    }

    sumDatareduce(arr) {
        return arr.reduce((a, b) => a + b);
    }

    sumObj(obj) {
        var sum = 0;
        for (var el in obj) {
            if (obj.hasOwnProperty(el)) {
                sum += parseInt(obj[el]);
            }
        }
        return sum;
    }

    renderAll() {
        const { flow, stateEngine } = this.props;
        let skills = [];
        let counts = [];
        let hasSets = [];
        let stateCounts = {
            message: 0,
            input: 0,
            nlu: 0,
            api: 0
        };
        let divIDs = []
        if (flow && flow.content && flow.content.skills) {
            skills = flow.content.skills.nodes.map(node => node.id)
            skills.forEach((skill) => {
                let count = [];
                let flag = false
                stateCounts = {
                    message: 0,
                    input: 0,
                    nlu: 0,
                    api: 0
                };
                let states = flow.content.states[skill] ? flow.content.states[skill].nodes : []
                states.forEach((state) => {
                    if (state.info.widget) {
                        stateCounts[state.info.widget.type] += 1
                    }
                    if (state.info.isStart && state.info.isStart === true) {
                        flag = true
                    }
                })
                count.push(stateCounts['message'])
                count.push(stateCounts['input'])
                count.push(stateCounts['nlu'])
                count.push(stateCounts['api'])
                counts.push(count)
                divIDs.push(skill)
                hasSets.push(flag)
            })
        }

        let i = -1
        return counts.map((count) => {
            i += 1

            return <div id={'_' + divIDs[i]} key={divIDs[i]}>
                {this.renderSetNotDone(hasSets[i])}
                {this.renderNoSetting(this.sumDatareduce(count))}
                {this.renderCount(count[0], 'status_box status-count-message', 'message-text', '提示')}
                {this.renderCount(count[1], 'status_box status-count-input', 'input-text', '輸入')}
                {this.renderCount(count[2], 'status_box status-count-nlu', 'nlu-text', 'AI')}
                {this.renderCount(count[3], 'status_box status-count-api', 'api-text', '查詢')}
            </div>
        })
    }

    renderSelectedSkill() {
        const { stateEngine } = this.props;
        let flag = false;
        if (stateEngine && stateEngine.diagramModel) {
            let stateCounts = {
                message: 0,
                input: 0,
                nlu: 0,
                api: 0
            };
            Object.values(stateEngine.diagramModel.nodes).forEach((node) => {
                if (node.info.widget) {
                    stateCounts[node.info.widget.type] += 1
                }
                if (node.info.isStart && node.info.isStart === true) {
                    flag = true
                }
            })
            return <div id={'_' + this.props.selectedSkill.id}>
                {this.renderSetNotDone(flag)}
                {this.renderCount(stateCounts['message'], 'status-count-message', 'message-text', '提示')}
                {this.renderCount(stateCounts['input'], 'status-count-input', 'input-text', '輸入')}
                {this.renderCount(stateCounts['nlu'], 'status-count-nlu', 'nlu-text', 'AI')}
                {this.renderCount(stateCounts['api'], 'status-count-api', 'api-text', '查詢')}
            </div>

        }
    }

    renderCount(count, color, popTextStyle, type) {
        if (count === 0) {
            return ''
        }
        return <span className={color}>
            <span className='count-text'>
                {count}
            </span>
            <span className={popTextStyle}>{`${count}個${type}狀態`}</span>
        </span>
    }

    render() {
        return <div style={{ height: 0, overflow: 'hidden' }}>
            {this.renderAll()}
        </div>

    }




}

export default SkillComponent
