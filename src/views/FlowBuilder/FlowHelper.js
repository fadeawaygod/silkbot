import {
    DefaultLinkFactory,
    DefaultNodeFactory,
    DefaultNodeInstanceFactory,
    DefaultPortInstanceFactory,
    DiagramEngine,
    DiagramModel,
    LinkInstanceFactory
} from "storm-react-diagrams";
import { Logger } from "../../utils/Logger";
import { get, post } from "../../api/HttpClient";
import sample from "./data/sample.json";
import uuidv1 from "uuid/v1";
import { getFlow } from "../../api/Bot";
import { findNluEndpoint } from "../../api/NluServices";
import { v4 as uuidv4 } from "uuid";
import sessionManager from "./data/sessionManager.json";
import sessionManagerAI from "./data/sessionManagerAI.json";
import { NLU_ENTITY_PROMPTS, NLU_INTENTS } from "./data/intents";
import { NLU_SLOTS } from "./data/slots";
import { API_RESULT, API_RESULT_SLOT } from "./data/apiResult";
import store from "@/store";

class FlowHelper {
    constructor() {
        this.logger = new Logger("FlowHelper");
        this.logger.log("FlowHelper is created!");
    }

    toConfig(diagram) {
        this.logger.log("toFlowConfig# diagram:", diagram);
    }

    toDiagram(config) {
        this.logger.log("toDiagram# config:", config);
    }

    setNodeName(node, name) {
        node.name = name;
        if (node.info) {
            node.info.name = name;
        } else {
            node["info"] = { name };
        }
        return node;
    }

    setNodeShowLink(node, showLink) {
        node.showLink = showLink;
        if (node.info) {
            node.info.showLink = showLink;
        } else {
            node["info"] = { showLink };
        }
        return node;
    }

    getNodeLabel(node) {
        return node && node.ports ? node.ports["port01"].label : "";
    }

    setNodeLabel(node, label) {
        if (!node || !node.ports | (node.ports.length === 0)) {
            return;
        }

        node.ports["port01"].label = label;
        this.setNodeInfo(node, "label", label);
    }

    removeRelatedStateTransitions(state, id) {
        state.nodes.forEach((node, nideIndex) => {
            if (node.id === id) {
                state.nodes.splice(nideIndex, 1);
            }
            if (node.info.transitions) {
                node.info.transitions.forEach(function (transition, index) {
                    if (transition.next_state === id) {
                        node.info.transitions.splice(index, 1);
                    }
                });
            }
        });
    }

    removeRelatedSkillTransitions(nodes, id) {
        for (let stateId in nodes) {
            nodes[stateId].nodes.forEach((node) => {
                if (node.info.transitions) {
                    node.info.transitions.forEach(function (transition, index) {
                        if (transition.next_skill === id) {
                            node.info.transitions.splice(index, 1);
                        }
                    });
                }
            });
        }
    }

    setNodeText(node, text) {
        if (!node || !node.ports | (node.ports.length === 0)) {
            return;
        }

        node.ports["port01"].text = text;
        this.setNodeInfo(node, "text", text);
    }

    setNodeInfo(node, key, value) {
        if (node.info) {
            node.info[key] = value;
        } else {
            node["info"] = { key: value };
        }
    }

    getNodeInfoValue(node, key) {
        if (node.info) {
            return node.info[key];
        }
        return undefined;
    }

    getAPIInfo(node) {
        return node.info["apiInfo"];
    }

    setAPIInfo(node, apiInfo) {
        if (node.info) {
            node.info["apiInfo"] = apiInfo;
        }
    }

    getTransitions(node) {
        this.logger.log("getTransitions#", node);

        if (!node) {
            return undefined;
        }

        if (node.info) {
            return node.info["transitions"];
        }
        return undefined;
    }

    removeCondition(selectedState, transition, condition) {
        const transitions = flowHelper.getTransitions(selectedState);

        let foundTransition = transitions.find((t) => t === transition);

        if (foundTransition) {
            foundTransition.conditions.splice(
                foundTransition.conditions.findIndex((c) => c === condition),
                1
            );
        }

        return transitions;
    }

    removeTransition(selectedState, transition) {
        const transitions = flowHelper.getTransitions(selectedState);
        let index = transitions.findIndex((t) => t === transition);
        if (index !== -1) {
            transitions.splice(index, 1);
        }
        return transitions;
    }

    setTransitions(node, transitions) {
        if (node.info) {
            node.info["transitions"] = transitions;
        } else {
            node["info"] = { transitions };
        }
    }

    getNodeInfo(node) {
        return node.info;
    }

    removeNode(engine, node) {
        if (!engine || !node) {
            return false;
        }

        const logger = new Logger("removeNode");

        const diagramMode = engine.diagramModel.serializeDiagram();
        logger.log("model:", diagramMode);

        engine.diagramModel.removeNode(node);
        logger.log("node removed:", node);

        const links = [];
        diagramMode.links.forEach((link) => {
            if (link.target === node.id || link.source === node.id) {
                links.push(link);
            }
        });

        links.forEach((link) => {
            logger.log("link removed:", engine.diagramModel.links[link.id]);
            engine.diagramModel.removeLink(engine.diagramModel.links[link.id]);
        });
        return true;
    }

    removeLinks(engine, node) {
        if (!engine) {
            return false;
        }

        const logger = new Logger("removeLinks");

        const diagramMode = engine.diagramModel.serializeDiagram();
        logger.log("model:", diagramMode);

        const links = [];
        diagramMode.links.forEach((link) => {
            if (link.target === node.id || link.source === node.id) {
                links.push(link);
            }
        });

        links.forEach((link) => {
            logger.log("link removed:", engine.diagramModel.links[link.id]);
            engine.diagramModel.removeLink(engine.diagramModel.links[link.id]);
        });
        return true;
    }

    getNode(engine, id) {
        if (!engine) {
            return false;
        }

        const keys = Object.keys(engine.diagramModel.nodes);
        if (!keys || keys.length === 0) {
            return undefined;
        }

        return engine.diagramModel.nodes[id];
    }

    setNodeSelected(engine, id) {
        if (!engine) {
            return false;
        }

        const keys = Object.keys(engine.diagramModel.nodes);
        if (!keys || keys.length === 0) {
            return undefined;
        }

        keys.forEach((key) => {
            engine.diagramModel.nodes[key].setSelected(false);
        });

        engine.diagramModel.nodes[id].setSelected(true);
    }

    getStateNode(engine, id) {
        if (!engine || !id) {
            return undefined;
        }
        return this.getNode(engine, id);
    }

    getNextSkillNode(engine, id) {
        if (!engine) {
            return false;
        }

        const keys = Object.keys(engine.diagramModel.nodes);
        if (!keys || keys.length === 0) {
            return undefined;
        }

        if (id === "session-manager") {
            id = keys[0];
        }

        return this.getNode(engine, id);
    }

    createDiagram(tag, serializedModel, listener) {
        const logger = new Logger("createDiagram");
        logger.debug("serializedModel:", serializedModel);

        const engine = new DiagramEngine();
        engine.registerNodeFactory(new DefaultNodeFactory());
        engine.registerLinkFactory(new DefaultLinkFactory());

        engine.registerInstanceFactory(new DefaultNodeInstanceFactory());
        engine.registerInstanceFactory(new DefaultPortInstanceFactory());
        engine.registerInstanceFactory(new LinkInstanceFactory());

        let model = engine.getDiagramModel();
        if (serializedModel) {
            model = new DiagramModel();

            serializedModel.links.forEach((link) => {
                link["_class"] = "LinkModel";
                link.points.forEach((point) => {
                    point["_class"] = "PointModel";
                });
            });

            serializedModel.nodes.forEach((node) => {
                node["_class"] = "DefaultNodeModel";
                node.ports.forEach((port) => {
                    port["_class"] = "DefaultPortModel";
                });
            });

            model.deSerializeDiagram(serializedModel, engine);

            Object.keys(model.getNodes()).forEach((key) => {
                const node = model.getNode(key);
                node.addListener({
                    selectionChanged: (node, isSelected) => {
                        logger.log("node#selectionChanged#", node);
                        listener.onNodeSelected(node, isSelected);
                    }
                });
            });

            Object.keys(model.getLinks()).forEach((key) => {
                const link = model.getLink(key);
                link.addListener({
                    selectionChanged: (link, isSelected) => {
                        logger.log("link#selectionChanged#", link);
                        listener.onLinkSelected(link, isSelected);
                    },
                    sourcePortChanged: (link) => {
                        logger.log("sourcePortChanged#", link);
                    },
                    targetPortChanged: (link) => {
                        logger.log("targetPortChanged#", link);
                    }
                });
            });
            engine.setDiagramModel(model);
        }

        model.addListener({
            linksUpdated: (link, isAdded) => {
                logger.log(
                    isAdded ? "model#linksUpdated# added" : "model#linksUpdated# removed",
                    link
                );

                if (listener && listener.linksUpdated) {
                    listener.linksUpdated(link, isAdded);
                }

                if (isAdded) {
                    link.addListener({
                        selectionChanged: (link, isSelected) => {
                            logger.debug("link#selectionChanged#", link);
                            listener.onLinkSelected(link, isSelected);
                        },
                        sourcePortChanged: (link) => {
                            logger.debug("link#sourcePortChanged#", link);
                            listener.sourcePortChanged(link);
                        },
                        targetPortChanged: (link) => {
                            logger.debug("link#targetPortChanged#", link);
                            listener.targetPortChanged(link);
                        }
                    });
                }
            },
            nodesUpdated: (node, isAdded) => {
                logger.debug(
                    isAdded ? "model#nodesUpdated# added" : "model#nodesUpdated# removed",
                    node
                );
                if (listener && listener.nodesUpdated) {
                    listener.nodesUpdated(node, isAdded);
                }

                if (isAdded) {
                    node.addListener({
                        selectionChanged: (node, isSelected) => {
                            logger.debug(
                                isSelected
                                    ? "model#selectionChanged# selected"
                                    : "model#selectionChanged# unselected",
                                node
                            );
                            listener.onNodeSelected(node, isSelected);
                        }
                    });

                    if (listener && listener.nodesUpdated) {
                        listener.onNodeAdded(node);
                    }
                } else {
                    listener.forceUpdate();
                }
            }
        });

        return this.mergeInfo(engine, serializedModel);
    }

    serialize(engine) {
        if (!engine || !engine.diagramModel) {
            return undefined;
        }

        const logger = new Logger("serialize");
        const model = engine.diagramModel;
        logger.log("serialize# model:", model);
        const sModel = engine.diagramModel.serializeDiagram();
        logger.log("serialize# sModel:", sModel);

        // merge info
        Object.keys(model.nodes).forEach((key) => {
            const node = model.nodes[key];
            const sNode = sModel.nodes.find((n) => n.id === node.id);
            if (node && sNode) {
                sNode["info"] = node["info"];
            }
        });

        return sModel;
    }

    mergeInfo(engine, serializedModelObject) {
        if (!engine || !engine.diagramModel || !serializedModelObject) {
            return engine;
        }

        const logger = new Logger("serialize");
        const model = engine.diagramModel;
        logger.log("mergeInfo# model:", model);
        logger.log("mergeInfo# serializedModel:", serializedModelObject);

        // merge info
        Object.keys(model.nodes).forEach((key) => {
            const node = model.nodes[key];
            const sNode = serializedModelObject.nodes.find((n) => n.id === node.id);
            if (node && sNode) {
                node["info"] = sNode["info"];
            }
        });

        return engine;
    }

    getStateValue(id) {
        return JSON.parse(localStorage.getItem(`state-${id}`));
    }

    setStateValue(id, value) {
        return localStorage.setItem(`state-${id}`, value);
    }

    removeStateValue(id) {
        return localStorage.removeItem(`state-${id}`);
    }

    addStateType(sid, type) {
        let types = JSON.parse(localStorage.getItem(`state-types`));
        if (!types) {
            types = [];
        }

        const idx = types.findIndex((id) => id === sid);
        if (idx === -1) {
            types.push({ id: sid, type });
        }
        localStorage.setItem(`state-types`, JSON.stringify(types));
        //this.renderStateSetting();
    }

    renderRemeveStateNode() {
        setTimeout(() => {
            let basicNode = document.querySelectorAll(
                "#stateDivID+div div.node[data-nodeid]"
            );
            Array.prototype.slice.call(basicNode).forEach((element, index) => {
                let bar = element.querySelector(".bar");
                if (!bar) {
                    bar = document.createElement("div");
                    bar.classList.add("bar");
                    element.querySelector(".basic-node").appendChild(bar);
                }
                if (
                    document.querySelector(
                        "#bar-div-id_" + element.attributes["data-nodeid"].value
                    )
                ) {
                    bar.innerHTML = document.querySelector(
                        "#bar-div-id_" + element.attributes["data-nodeid"].value
                    ).innerHTML;
                    for (let i = 0; i < bar.querySelectorAll(".Rectangle-pop-box").length; i++) {
                        bar.querySelectorAll(".Rectangle-pop-box")[i].onmouseup = () => {
                            document.querySelector("#bar-div-id_" + element.attributes["data-nodeid"].value).querySelectorAll(".Rectangle-pop-box")[i].click();
                        };
                    }
                }
            });
        });
    }

    renderRemeveSkillNode() {
        setTimeout(() => {
            let basicNode = document.querySelectorAll(
                "#skillDivID+div div.node[data-nodeid]"
            );
            Array.prototype.slice.call(basicNode).forEach((element, index) => {
                let bar = element.querySelector(".bar");
                if (!bar) {
                    bar = document.createElement("div");
                    bar.classList.add("bar");
                    element.querySelector(".basic-node").appendChild(bar);
                }
                if (
                    document.querySelector(
                        "#bar-skill-div-id_" + element.attributes["data-nodeid"].value
                    )
                ) {
                    bar.innerHTML = document.querySelector(
                        "#bar-skill-div-id_" + element.attributes["data-nodeid"].value
                    ).innerHTML;
                    bar.querySelector(".Rectangle-pop-box").onclick = () => {
                        document
                            .querySelector(
                                "#bar-skill-div-id_" + element.attributes["data-nodeid"].value
                            )
                            .querySelector(".Rectangle-pop-box")
                            .click();
                    };
                }
            });
        }, 0);
    }

    renderStateSetting() {
        setTimeout(() => {
            let basicNode = document.querySelectorAll(
                "#stateDivID+div div.node[data-nodeid]"
            );
            Array.prototype.slice.call(basicNode).forEach((element, index) => {
                let nodeContent = element.querySelector(".nodeContent");
                let title = element.querySelector(".title");
                if (title.innerText.includes("提示")) {
                    title.querySelector(".name").classList.add("_message");
                } else if (title.innerText.includes("輸入")) {
                    title.querySelector(".name").classList.add("_input");
                } else if (title.innerText.includes("查詢")) {
                    title.querySelector(".name").classList.add("_api");
                } else if (title.innerText.includes("AI")) {
                    title.querySelector(".name").classList.add("_nlu");
                }
                if (!nodeContent) {
                    nodeContent = document.createElement("div");
                    nodeContent.classList.add("nodeContent");
                    element.querySelector(".basic-node").appendChild(nodeContent);
                }
                if (
                    document.querySelector(
                        "#_" + element.attributes["data-nodeid"].value
                    ) !== null
                ) {
                    nodeContent.innerHTML = document.querySelector(
                        "#_" + element.attributes["data-nodeid"].value
                    ).innerHTML;
                }
                element.querySelector(".title").appendChild(nodeContent);
            });
        }, 0);
    }

    renderSkillSetting() {
        setTimeout(() => {
            let basicNode = document.querySelectorAll(
                "#skillDivID+div div.node[data-nodeid]"
            );
            Array.prototype.slice.call(basicNode).forEach((element, index) => {
                if (index === 0) {
                    let FirstbasicNode = element.querySelector(".basic-node");
                    FirstbasicNode.classList.add("first");
                }

                let titleLine = element.querySelector(".out .name");
                if (
                    document.querySelector(
                        "#_" + element.attributes["data-nodeid"].value
                    ) !== null
                ) {
                    titleLine.innerHTML = document.querySelector(
                        "#_" + element.attributes["data-nodeid"].value
                    ).innerHTML;
                    //sync skillList.vue
                    document.querySelector(
                        "#skillNode_" + element.attributes["data-nodeid"].value
                    ).innerHTML = document.querySelector(
                        "#_" + element.attributes["data-nodeid"].value
                    ).innerHTML;
                }
            });
        }, 0);
    }

    removeStateType(sid) {
        let types = JSON.parse(localStorage.getItem(`state-types`));
        if (!types) {
            return;
        }

        const idx = types.findIndex((state) => state.id === sid);
        if (idx === -1) {
            return;
        }
        types.splice(idx, 1);
        localStorage.setItem(`state-types`, JSON.stringify(types));
    }

    getStateType(sid) {
        let types = JSON.parse(localStorage.getItem(`state-types`));
        if (!types) {
            return;
        }

        return types.find((state) => state.id === sid);
    }

    setStateText(node, text) {
        this.setNodeInfo(node, "text", text);
    }

    getStateText(node) {
        return this.getNodeInfoValue(node, "text");
    }

    loadSkillDiagram(botId) {
        return JSON.parse(localStorage.getItem(`${botId}-skills`));
    }

    loadStateDiagram(botId, skillId) {
        return JSON.parse(localStorage.getItem(`${botId}-${skillId}-states`));
    }

    buildRunner(info, nluEndpoints, currentBot, nlu_query_url) {
        if (!info) {
            return undefined;
        }

        switch (info.widget.type) {
            case "message":
                return {
                    type: info.widget.type,
                    prompt: info.tprompt
                };
            case "nlu":
                //用nlu_id判斷是否currentBot
                function formatVariables(variables) {
                    let result = {};
                    for (const { alias, entity } of variables) {
                        result[alias] = entity;
                    }
                    return result;
                }
                if (!info.nlu_id || info.nlu_id === "") {
                    if (currentBot.nlu_token) {
                        return {
                            type: info.widget.type,
                            url: nlu_query_url,
                            token: currentBot.nlu_token,
                            threshold:
                                ((currentBot.nlu_endpoint || {}).header || {})
                                    .intent_threshold || currentBot.intent_threshold,
                            variables: formatVariables(info.variables)
                        };
                    } else {
                        return {
                            type: info.widget.type,
                            url: nlu_query_url,
                            token: "",
                            variables: formatVariables(info.variables)
                        };
                    }
                }
                let endpoint = nluEndpoints.find((x) => x.id === info.nlu_id);

                return {
                    type: info.widget.type,
                    url: endpoint.url ? endpoint.url : "",
                    token: endpoint.header.token ? endpoint.header.token : "",
                    body: endpoint.body ? endpoint.body : "",
                    id: endpoint.id ? endpoint.id : "",
                    nlu_name: endpoint.nlu_name ? endpoint.nlu_name : "",
                    http_type: endpoint.http_type ? endpoint.http_type : "",
                    parameter: endpoint.parameter ? endpoint.parameter : "",
                    variables: formatVariables(info.variables)
                };
            case "api":
                return {
                    type: info.widget.type,
                    url: info.url,
                    method: info.httpMethod,
                    //body: info.httpBody,
                    headers: { "content-type": "application/json" },
                    payload: info.httpBody ? info.httpBody : { intent: "{data[intent]}" }
                };
            case "input":
                let is_show_condition_prompts = info.hasOwnProperty("is_show_condition_prompts") ? info.is_show_condition_prompts : false;
                let condition_prompts = is_show_condition_prompts ? (info.condition_prompts || []) : [];
                if (condition_prompts.length && condition_prompts[0].text === "") {
                    condition_prompts = [];
                }
                let prompt = info.hasOwnProperty("prompt") ? info.prompt : {};
                if (!(((prompt.items || [])[0]) || {}).message) {
                    prompt = {};
                }
                return {
                    type: info.widget.type,
                    is_show_condition_prompts: is_show_condition_prompts,
                    condition_prompts: condition_prompts,
                    prompt: prompt
                }
        }

        return {
            type: info.widget.type
        };
    }

    appendIntentOk(node, transitions) {
        if (!node) {
            return;
        }

        if (!node.ports) {
            return;
        }

        Object.keys(node.ports).forEach((key) => {
            const port = node.ports[key];
            this.logger.log(node.name + "#port.target", port.target);
            if (port.target) {
                transitions.push({
                    conditions: [{ intent: "ok" }],
                    nextState: port.target
                });
            }
        });
        return transitions;
    }

    getFirstPortLabel(node) {
        if (!node) {
            return "";
        }

        if (!node.ports) {
            return "";
        }

        return node.ports["port01"].label;
    }

    checkSkillEmpty(flow) {
        if (!flow || !flow.content) return;
        let emptySkillName = ''
        Object.keys(flow.content.states).forEach((stateId) => {
            if (flow.content.states[stateId].nodes.length === 0) {
                let emptySkill = flow.content.skills.nodes.find(node => node.id === stateId);
                emptySkillName = emptySkill.name;
            }
        })
        return emptySkillName
    }

    checkMessageTprompt(flow) {
        if (!flow || !flow.content) return;
        let emptySkillName = ''
        let emptyStateName = ''
        Object.keys(flow.content.states).forEach((stateId) => {
            flow.content.states[stateId].nodes.forEach((node) => {
                if (node.info.widget.type === "message") {
                    if (!node.info.tprompt) {
                        emptySkillName = node.skillName;
                        emptyStateName = node.info.name;
                    }
                    if (node.info.tprompt && node.info.tprompt.items) {
                        if (node.info.tprompt.items.length === 0) {
                            emptySkillName = node.skillName;
                            emptyStateName = node.info.name;
                        }
                    }
                }
            })
        })
        return [emptySkillName, emptyStateName]
    }

    syncFlowName(flow) {
        if (!flow || !flow.content) return;
        Object.keys(flow.content.states).forEach((stateId) => {
            flow.content.states[stateId].nodes.forEach((node) => {
                if (node.info.transitions) {
                    node.info.transitions.forEach((transition) => {
                        transition.conditions.forEach((condition) => {
                            if (condition.operation === "intent") {
                                let intent = flow.intents.find(
                                    (intent) => intent.id === condition.id
                                );
                                if (intent) condition.value = intent.name;
                                if (condition.entities) {
                                    condition.entities.forEach((entity) => {
                                        if (entity.isAlias) return false;
                                        if (entity.id) {
                                            let theEntity = flow.entities.find(flowEntity => flowEntity.id === entity.id)
                                            if (theEntity)
                                                entity.entity = theEntity.name
                                        }
                                    })
                                }
                            }
                        });
                    });
                }
            });
        });
        return flow;
    }

    async sync_flow_nlu_id_exist(flow) {
        if (!flow || !flow.content) return;
        let otherNluId = [];
        Object.keys(flow.content.states).forEach((stateId) => {
            flow.content.states[stateId].nodes.forEach((node) => {
                if (node.info.nlu_id) {
                    if (
                        !flow.endpoints.find((endpoint) => endpoint.id === node.info.nlu_id)
                    ) {
                        if (!otherNluId.includes(node.info.nlu_id)) {
                            otherNluId.push(node.info.nlu_id);
                        }
                    }
                }
            });
        });
        for (let accessor in flow.currentBot.accessors) {
            for (let type in flow.currentBot.accessors[accessor]) {
                flow.currentBot.accessors[accessor][type] = flow.currentBot.accessors[
                    accessor
                ][type].toString();
            }
        }
        let endpoints = [];
        for (let i = 0; i < otherNluId.length; i++) {
            let result = await findNluEndpoint(
                otherNluId[i],
                flow.currentBot.accessors
            );
            endpoints.push(result.endpoint[0]);
        }
        return endpoints;
    }

    getFlowIntents(flow) {
        let _intents = [];
        if (!flow || !flow.content) return _intents;
        Object.keys(flow.content.states).forEach((stateId) => {
            flow.content.states[stateId].nodes.forEach((node) => {
                if (node.info.transitions) {
                    node.info.transitions.forEach((transition) => {
                        transition.conditions.forEach((condition) => {
                            if (condition.operation === "intent") {
                                if (condition.id) {
                                    if (!_intents.includes(condition.id))
                                        _intents.push(condition.id);
                                }
                            }
                        });
                    });
                }
            });
        });
        return _intents;
    }

    checkEntityHasPrompt(entities, contents) {
        for (let entity of entities) {
            if (!entity.hasOwnProperty("prompts") || entity.prompts.length === 0) {
                alert(`${entity.name} 尚未設定反問句`)
                return false
            }
        }
        for (let stateId in contents.states) {
            for (let node of contents.states[stateId].nodes) {
                if (node.info.widget.type === 'nlu') {
                    for (let variable of node.info.variables) {
                        if (variable.alias !== "") {
                            if (!variable.hasOwnProperty("prompt") || variable.prompt.length === 0) {
                                alert(`${variable.alias} 尚未設定反問句`)
                                return false
                            }
                        }
                    }
                }
            }
        }
        return true
    }

    convertNextStateSkill(node, transitions, entities) {
        this.logger.log(
            "convertNextStateSkill#node#1" + node.info.label,
            transitions
        );
        transitions.forEach((transition) => {
            this.logger.log(
                "convertNextStateSkill#transition:" + node.info.label,
                transition
            );
            if (transition.nextSkill) {
                transition["next_skill"] = transition.nextSkill;
                delete transition.nextSkill;
            }

            if (transition.nextState) {
                transition["next_state"] = transition.nextState;
                delete transition.nextState;
            }

            if (transition.conditions) {
                transition.conditions.forEach((condition) => {
                    switch (condition.operation) {
                        case "intent":
                            condition["intent"] = condition.value;
                            break;
                        case "api":
                            condition["intent"] = condition.value;
                            break;
                        case "slot":
                            condition["entity"] = condition.value;
                            break;
                        case "includes":
                            condition["includes"] = condition.value;
                            break;
                        case "regex":
                            condition["regex"] = condition.value;
                            break;
                        case "equal":
                            condition["equal"] = condition.value;
                            break;
                    }

                    if (condition.slots && condition.slots.length === 0) {
                        delete condition["slots"];
                    }

                    if ("entities" in condition && condition.entities.length === 0) {
                        delete condition["entities"];
                    }

                    if ("entities" in condition) {
                        condition["entities"].forEach((entity) => {
                            if (entity.isAlias) {
                                entity.prompt = (node.info.variables.find(x => x.alias === entity.entity) || {}).prompt || [];
                            }
                            else {
                                let theEntity = entities.find((x) => x.id === entity.id);
                                if (!theEntity) {
                                    theEntity = entities.find((x) => x.name === entity.entity);
                                }
                                theEntity.prompts.forEach((x) => {
                                    if (!entity.hasOwnProperty("prompt")) {
                                        entity.prompt = [];
                                    }
                                    entity.prompt.push(x);
                                });
                            }

                        });
                    }

                    delete condition["operation"];
                    delete condition["value"];
                });
            }

            if (transition.isEnabled === false) {
                let index = transitions.indexOf(transition);
                delete transitions[index];
            }

            if (transition.isEnabled) {
                delete transition.isEnabled;
            }
        });
        transitions = transitions.filter((n) => n);
        this.logger.log(
            "convertNextStateSkill#node#2" + node.info.label,
            transitions
        );
        return transitions;
    }

    nodeToState(node, endpoints, currentBot, entities, nluInfo) {
        if (!node || !node.info) {
            return undefined;
        }

        const info = node.info;
        const defaultCondition = {
            conditions: [{ intent: "ok" }],
            next_skill: "session-manager"
        };

        const transitions = JSON.parse(
            JSON.stringify(info.transitions ? info.transitions : [defaultCondition])
        );
        const withInput = info.withInput ? info.withInput : false;
        const state = {
            id: node.id,
            name: node.name,
            label: info.name,
            start: info.isStart,
            end: info.isEnd,
            runner: this.buildRunner(info, endpoints, currentBot, nluInfo.query_url),
            transitions: this.convertNextStateSkill(node, transitions, entities)
        };

        if (info.widget.type === "message" && withInput) {
            state.transitions = [
                { conditions: [{ intent: "ok" }], next_state: node.id + "-input" }
            ];
            const inputState = {
                id: node.id + "-input",
                name: node.name + " 的輸入",
                start: false,
                end: false,
                runner: { type: "input" },
                transitions: this.convertNextStateSkill(node, transitions, entities)
            };
            return { state, inputState };
        }

        return { state };
    }

    buildSkillStates(botId, skillId, flow) {
        const stateDiagram = flow.content.states[skillId];
        if (
            !stateDiagram ||
            !stateDiagram.nodes ||
            stateDiagram.nodes.length === 0
        ) {
            return [];
        }

        const states = [];
        stateDiagram.nodes.forEach((node) => {
            const theStates = this.nodeToState(
                node,
                flow.endpoints,
                flow.currentBot,
                flow.entities,
                flow.nluInfo,
            );
            states.push(theStates.state);
            if (theStates.inputState) {
                states.push(theStates.inputState);
            }
        });

        return states;
    }

    getLinks(serializedDiagram, stateId) {
        this.logger.log("getLinks#stateId:", stateId);
        this.logger.log("getLinks#links:", serializedDiagram.links);
        return serializedDiagram.links.filter((link) => {
            this.logger.log("getLinks#link.source:", link.source);
            this.logger.log("getLinks# same?", link.source === stateId);
            return link.source === stateId;
        });
    }

    renameSessionManager(bot) {
        this.logger.log("renameSessionManager", bot);
        const sid = bot.agent["session-manager"].id;
        bot.agent.skills.forEach((skill) => {
            if (skill.states) {
                skill.states.forEach((state) => {
                    state.transitions.forEach((transition) => {
                        if (transition["next_skill"] === sid) {
                            transition["next_skill"] = "session-manager";
                        }
                    });
                });
            }
        });
        return bot;
    }

    checkExistNluToken(flowContent) {
        let pass = true;

        for (let skillId in flowContent.states) {
            for (let i = 0; i < flowContent.states[skillId].nodes.length; i++) {
                if (flowContent.states[skillId].nodes[i].info.widget.type === "nlu") {
                    if (!flowContent.states[skillId].nodes[i].info.nlu_id) {
                        pass = false;
                        break;
                    }
                }
            }
        }
        return pass;
    }

    buildBot(botId, flow) {
        const logger = new Logger("buildBot");
        logger.debug("buildBot# botId:", botId);
        logger.debug("buildBot# flow:", flow);

        if (!flow || !flow.content || !flow.content.skills === 0) {
            return { agent: {} };
        }

        const skillDiagram = flow.content.skills;
        logger.debug("buildBot# skillDiagram#", skillDiagram);

        if (
            !skillDiagram ||
            !skillDiagram.nodes ||
            skillDiagram.nodes.length === 0
        ) {
            alert("Flow is empty");
            return { agent: {} };
        }

        const sessionManager = {
            id: skillDiagram.nodes[0].id,
            name: skillDiagram.nodes[0].name,
            states: this.buildSkillStates(botId, skillDiagram.nodes[0].id, flow)
        };

        if (sessionManager.states && sessionManager.states.length > 0) {
            if (!sessionManager.states.find((s) => s["start"] === true)) {
                sessionManager.states[0]["start"] = true;
            }
        }

        const skills = [];
        skillDiagram.nodes.forEach((node) => {
            if (node.id !== sessionManager.id) {
                const states = this.buildSkillStates(botId, node.id, flow);
                if (states && states.length > 0) {
                    if (!states.find((s) => s["start"] === true)) {
                        states[0]["start"] = true;
                    }
                }

                skills.push({
                    id: node.id,
                    name: node.name,
                    states
                });
            }
        });

        const bot = {
            agent: {
                "session-manager": sessionManager,
                skills,
                chinese_convert: store.getters.currentBot.config.chinese_convert
            }
        };

        console.log("booooot", bot);
        console.log("booooot", JSON.stringify(bot));

        return this.renameSessionManager(bot);
    }

    addNluData(config, nluUrl, nluToken) {
        config.agent["session-manager"]["nluUrl"] = nluUrl;
        config.agent["session-manager"]["nluToken"] = nluToken;
        return config;
    }

    sortTransitionsByConditionLen(config) {
        config.agent["session-manager"].states.forEach((state) => {
            let newTransitions = [];
            if (state.runner.type === "nlu") {
                let transitionLen = Math.max(
                    ...state.transitions.map((transition) => transition.conditions.length)
                );
                for (let i = transitionLen; i >= 0; i--) {
                    state.transitions.forEach((transition) => {
                        if (transition.conditions.length == i) {
                            newTransitions.push(transition);
                        }
                    });
                }
                state.transitions = newTransitions;
            }
        });
        config.agent.skills.forEach((skill) => {
            skill.states.forEach((state) => {
                let newTransitions = [];
                if (state.runner.type === "nlu") {
                    let transitionLen = Math.max(
                        ...state.transitions.map(
                            (transition) => transition.conditions.length
                        )
                    );
                    for (let i = transitionLen; i >= 0; i--) {
                        state.transitions.forEach((transition) => {
                            if (transition.conditions.length == i) {
                                newTransitions.push(transition);
                            }
                        });
                    }
                    state.transitions = newTransitions;
                }
            });
        });
        return config;
    }

    validateConfig(botId) {
        const bot = buildBot(botId);
        this.logger.log("validateBot#", bot);
        return post(
            "https://chatbot.65lzg.com/agent/validate",
            { "Content-Type": "text/plain" },
            bot
        ).then((result) => {
            return Promise.resolve(result);
        });
    }

    publishConfig(botId, config) {
        this.logger.log("publishConfig#botId", botId);
        this.logger.log("publishConfig#config", config);
        this.logger.log("publishConfig#config#string", JSON.stringify(config));
        return post(
            `https://chatbot.65lzg.com/agent/${botId}`,
            { "Content-Type": "text/plain" },
            config
        ).then((result) => {
            return Promise.resolve(result);
        });
    }

    publishDefaultBot(bot) {
        this.logger.log("publishDefaultBot", bot);
        return post(
            " https://chatbot.65lzg.com/agent/80yo0abFI7171kPx13AU6a0XkBvbaSUqDi3VcbBpiHUMTU2NzU2ODA5NTEzMg==",
            { "Content-Type": "text/plain" },
            sample
        ).then((result) => {
            return Promise.resolve(result);
        });
    }

    getBotState() {
        return get("https://chatbot.65lzg.com/agent/state", undefined, undefined)
            .then((result) => {
                this.logger.log("result#", result);
                return Promise.resolve(result);
            })
            .catch((err) => {
                return Promise.resolve({
                    "80yo0abFI7171kPx13AU6a0XkBvbaSUqDi3VcbBpiHUMTU2NzU2ODA5NTEzMg==_766451083_skill":
                        "a56667aa-6ada-437d-997f-b06d6036e36c",
                    "80yo0abFI7171kPx13AU6a0XkBvbaSUqDi3VcbBpiHUMTU2NzU2ODA5NTEzMg==_766451083_state":
                        "850ae0e2-444c-48da-a6dd-07c040dfb775"
                });
            });
    }

    flowPrepare(flow) {
        this.logger.log("flowPrepare#", flow);

        if (
            !flow ||
            !flow.content ||
            !flow.content.skills ||
            !flow.content.states
        ) {
            this.logger.log("flowPrepare# return - 1");
            return flow;
        }

        const { skills, states } = flow.content;
        if (!skills.nodes || skills.nodes.length === 0) {
            this.logger.log("flowPrepare# return - 2");
            delete flow.content["states"];
            return flow;
        }

        // remove unused states
        const toRemove = [];
        Object.keys(states).forEach((key) => {
            if (!skills.nodes.find((n) => n.id === key)) {
                this.logger.log("flowPrepare#toRemove#", key);
                toRemove.push(key);
            }
        });

        toRemove.forEach((id) => {
            this.logger.log("flowPrepare#removeSkillStates#", id);
            delete flow.content.states[id];
        });
        this.logger.log("flowPrepare#return, flow:", flow);
        return flow;
    }

    toSkillNode(skill, idx) {
        return {
            id: skill.id,
            color: "peru",
            extras: {},
            info: {},
            name: skill.name,
            ports: [
                {
                    id: uuidv1,
                    in: false,
                    label: "",
                    links: [],
                    name: "",
                    parentNode: skill.id,
                    selected: false
                }
            ],
            selected: false,
            type: "default",
            x: 100,
            y: 100 + idx * 100,
            _class: "DefaultNodeModel"
        };
    }

    toFlow(config) {
        const content = {
            skills: {
                id: uuidv1(),
                links: [],
                nodes: [],
                offsetX: 1,
                offsetY: 0,
                zoom: 100
            },
            states: {}
        };

        const sessionManager = config.agent["session-manager"];
        const sessionManagerNode = toSkillNode(sessionManager, 0);
        sessionManagerNode["id"] = "session-manager";
        content.skills.nodes.push(sessionManagerNode);
        let idx = 1;
        config.agent.skills.forEach((skill) => {
            content.skills.nodes.push(toSkillNode(skill, idx));
            idx += 1;
        });
        content.skills.nodes.forEach((skillNode) => {
            content.states[skillNode.id] = {};
        });

        return content;
    }

    objClone(obj) {
        try {
            return JSON.parse(JSON.stringify(obj));
        } catch (err) {
            return undefined;
        }
    }

    loadFlow(botId) {
        return getFlow(botId).then((result) => {
            if (result.status === "ok") {
                const flows = result.flows;
                if (!flows || flows.length === 0) {
                    return Promise.resolve({ content: {} });
                }
                return Promise.resolve(flows[0]);
            }
        });
    }

    getSilkIntents() {
        this.logger.log("getSilkIntents#");

        const intents = [
            { id: "intent-1", name: "ok", slots: [] },
            { id: "intent-2", name: "unknown", slots: [] }
        ];
        const nluIntents = NLU_INTENTS.split("\n");
        let index = 2;
        nluIntents.forEach((intentText) => {
            index++;
            let tokens = intentText.split(":");
            if (tokens.length > 1) {
                const name = tokens[0];
                const slotsText = tokens[1].replace(/e_/g, "");
                const slots = [];
                slotsText
                    .trim()
                    .split(",")
                    .forEach((slot) => {
                        slots.push({
                            name: slot,
                            prompts: [NLU_ENTITY_PROMPTS[slot]]
                        });
                    });
                intents.push({ id: "intent-" + index, name, slots });
            } else {
                intents.push({
                    id: "intent-" + index,
                    name: intentText.trim(),
                    slots: []
                });
            }
        });

        return intents;
    }

    getSilkEntities() {
        const entities = [];
        const nluSlots = NLU_SLOTS.split("\n");
        let index = 0;
        nluSlots.forEach((slotText) => {
            index++;
            let tokens = slotText.split(":");
            if (tokens.length > 1) {
                const name = tokens[0];
                const slotsText = tokens[1].replace(/e_/g, "");
                const slots = [];
                slotsText
                    .trim()
                    .split(",")
                    .forEach((slot) => {
                        slots.push({
                            name: slot
                        });
                    });
                entities.push({ id: "slot-" + index, name, slots: slots });
            } else {
                entities.push({
                    id: "slot-" + index,
                    name: slotText.trim(),
                    slots: []
                });
            }
        });
        return entities;
    }

    getSilkAPIResult() {
        this.logger.log("getSilkAPIResult:", API_RESULT);
        const apiResult = [];
        const apiResults = API_RESULT.split("\n");
        let index = 0;
        apiResults.forEach((intentText) => {
            index++;
            let tokens = intentText.split(":");
            if (tokens.length > 1) {
                const name = tokens[0];
                const slotsText = tokens[1].replace(/e_/g, "");
                const slots = [];
                slotsText
                    .trim()
                    .split(",")
                    .forEach((slot) => {
                        slots.push({
                            name: slot,
                            slot: API_RESULT_SLOT[slot]
                        });
                    });
                apiResult.push({ id: "intent-" + index, name, slots });
            } else {
                apiResult.push({
                    id: "intent-" + index,
                    name: intentText.trim(),
                    slots: []
                });
            }
        });
        return apiResult;
    }

    resetNodes(diagramModel) {
        const nodes = diagramModel.nodes;
        if (!nodes) {
            return;
        }

        Object.keys(nodes).forEach((key) => {
            nodes[key].setSelected(false);
        });
    }

    setSelectedNode(diagramModel, id) {
        if (!id) {
            return;
        }

        this.resetNodes(diagramModel);

        const nodes = diagramModel.nodes;
        if (!nodes) {
            return;
        }

        const key = Object.keys(nodes).find((k) => k === id);
        nodes[key].setSelected(true);
    }

    getUpdatedDiagramFlow(flow, selectedSkill, skillEngine, stateEngine) {
        if (!flow) {
            this.logger.debug("getUpdatedDiagramFlow# error! flow is undefined");
            return flow;
        }

        if (!selectedSkill) {
            return flow;
        }

        let updatedFlow = JSON.parse(JSON.stringify(flow));
        this.logger.log("getUpdatedDiagramFlow#", updatedFlow);
        if (!updatedFlow.content) {
            updatedFlow["content"] = {};
        }

        const skillDiagram = flowHelper.serialize(skillEngine);
        updatedFlow.content[`skills`] = skillDiagram;
        this.logger.debug("getUpdatedDiagramFlow# skillDiagram: ", skillDiagram);

        updatedFlow = this.getUpdatedStateDiagramFlow(
            updatedFlow,
            selectedSkill ? selectedSkill.id : undefined,
            stateEngine
        );
        return updatedFlow;
    }

    getUpdatedStateDiagramFlow(flow, selectedSkillId, stateEngine) {
        if (!flow) {
            return flow;
        }

        if (!stateEngine) {
            return flow;
        }

        let updatedFlow = JSON.parse(JSON.stringify(flow));

        if (!updatedFlow.content) {
            updatedFlow["content"] = {};
        }

        const statesDiagram = flowHelper.serialize(stateEngine);
        this.logger.debug(
            "getUpdatedStateDiagramFlow# statesDiagram: ",
            statesDiagram
        );

        if (!updatedFlow.content[`states`]) {
            updatedFlow.content[`states`] = {};
        }

        if (selectedSkillId) {
            updatedFlow.content[`states`][selectedSkillId] = statesDiagram;
        }

        let existSkillId = updatedFlow.content.skills.nodes.map((x) => x.id);

        Object.keys(updatedFlow.content.states).forEach((state) => {
            if (!existSkillId.has(state)) {
                delete updatedFlow.content.states[state];
            }
        });
        return updatedFlow;
    }

    getSkillNodes(skillEngine) {
        return this.getNodes(skillEngine);
    }

    getStateNodes(stateEngine) {
        return this.getNodes(stateEngine);
    }

    getNodes(engine) {
        const nodes =
            engine && engine.diagramModel ? engine.diagramModel.nodes : {};
        const nodesList = [];
        Object.keys(nodes).forEach((key) => {
            const node = nodes[key];
            node["key"] = key;
            nodesList.push(node);
        });
        return nodesList;
    }

    getSelectedSkillNode(selectedSkill, skillEngine) {
        if (!selectedSkill) {
            return undefined;
        }
        return skillEngine && skillEngine.diagramModel
            ? skillEngine.diagramModel.nodes[selectedSkill.id]
            : {};
    }

    getStatesDiagram(flow, skill) {
        if (
            !flow ||
            !skill ||
            !flow.content ||
            !flow.content.states ||
            !flow.content.states[skill.id]
        ) {
            return undefined;
        }
        return flow.content.states[skill.id];
    }

    getStatesDiagrambySkillId(flow, skillId) {
        if (
            !flow ||
            !skillId ||
            !flow.content ||
            !flow.content.states ||
            !flow.content.states[skillId]
        ) {
            return undefined;
        }
        return flow.content.states[skillId];
    }

    findNode(serializedDiagram, id) {
        if (!serializedDiagram) {
            return undefined;
        }
        return serializedDiagram.nodes.find((node) => node.id === id);
    }

    createSkillID(prio) {
        // prio: file to import
        let skillIDs = [];
        skillIDs[0] = uuidv4(); // session manager
        skillIDs[1] = uuidv4(); // session manager AI
        skillIDs[2] = uuidv4(); // unknown
        for (let i = 3; i <= prio.length + 2; i++) {
            skillIDs[i] = prio[i - 3].id;
        }
        return skillIDs;
    }

    buildUIConfig(skillIDs, prio) {
        let config = {};
        config["skills"] = {
            id: uuidv4(),
            offsetX: 0,
            offsetY: 0,
            zoom: 100,
            links: [],
            nodes: []
        };
        for (let i = 0; i <= prio.length + 2; i++) {
            let skillName = i < 3 ? `任务${i}` : prio[i - 3].name;
            let node = {
                id: skillIDs[i],
                _class: "DefaultNodeModel",
                selected: false,
                type: "default",
                x: 0,
                y: 0,
                name: skillName,
                color: "white",
                info: {
                    isSkill: true,
                    id: skillIDs[i],
                    name: skillName
                },
                ports: [
                    {
                        id: uuidv4(),
                        _class: "DefaultPortModel",
                        selected: false,
                        name: "",
                        parentNode: skillIDs[i],
                        links: [],
                        in: false,
                        label: ""
                    }
                ]
            };
            config["skills"]["nodes"].push(node);
        }
        config["states"] = {};
        let unknownTransitions = {
            entity: "",
            next_skill: skillIDs[1],
            conditions: [
                {
                    operation: "intent",
                    value: "unknown"
                }
            ]
        };
        let unknownTransitiontoUnknow = {
            entity: "",
            next_skill: skillIDs[2],
            conditions: [
                {
                    operation: "intent",
                    value: "unknown"
                }
            ]
        };
        for (let j = 0; j < skillIDs.length; j++) {
            let skillIndex = j - 3;
            config["states"][skillIDs[j]] = {
                id: uuidv4(),
                offsetX: 0,
                offsetY: 0,
                zoom: 100,
                links: [],
                nodes: []
            };
            if (j == 0) {
                // let sessionManagerTransitions = this.getSessionManagerTransitions();
                // sessionManagerTransitions.push(unknownTransitions);
                sessionManager.forEach((node) => {
                    if (node.info.widget.type == "input") {
                        node.info.transitions.push({
                            entity: "",
                            next_skill: skillIDs[1],
                            conditions: [
                                {
                                    operation: "intent",
                                    value: "unknown",
                                    id: "unknown"
                                }
                            ]
                        });
                    }
                });
                config["states"][skillIDs[j]].nodes = sessionManager;
            } else if (j == 1) {
                let sessionManagerTransitions = this.getSessionManagerTransitions(prio);
                sessionManagerTransitions.push(unknownTransitiontoUnknow);
                sessionManagerAI.forEach((node) => {
                    if (node.info.widget.type == "nlu") {
                        node.info.transitions = sessionManagerTransitions;
                    }
                });
                config["states"][skillIDs[j]].nodes = sessionManagerAI;
            } else if (j == 2) {
                let unknownSkill = this.createUnknownTransition(skillIDs[0]);
                config["states"][skillIDs[j]] = unknownSkill;
            } else {
                let tmpCount = [];
                for (let i = 0; i <= prio[skillIndex].states.length - 1; i++) {
                    tmpCount.push(prio[skillIndex].states[i].stage);
                    let unknownTransitionstoSM = [
                        {
                            entity: "",
                            next_skill: skillIDs[0],
                            conditions: [
                                {
                                    operation: "intent",
                                    value: "unknown"
                                }
                            ]
                        }
                    ];
                    //let transition = prio[skillIndex].states[i].transitions.length == 0 ? unknownTransitionstoSM : prio[skillIndex].states[i].transitions;
                    let transition = unknownTransitionstoSM;
                    if (prio[skillIndex].states[i].transitions.length !== 0) {
                        transition = prio[skillIndex].states[i].transitions;
                        if (prio[skillIndex].states[i].widget.type == "nlu") {
                            transition.push(unknownTransitions);
                        }
                    }
                    let node = {
                        id: prio[skillIndex].states[i].id,
                        _class: "DefaultNodeModel",
                        selected: false,
                        type: "default",
                        x: prio[skillIndex].states[i].stage * 230,
                        y: 10 + prio[skillIndex].states[i].Y * 120,
                        //"y": 10 + (tmpCount.filter(x => x == prio[skillIndex].states[i].stage).length - 1) * 200,
                        extras: {},
                        ports: [
                            {
                                id: uuidv4(),
                                _class: "DefaultPortModel",
                                selected: false,
                                name: "port01",
                                parentNode: prio[skillIndex].states[i].id,
                                links: [],
                                in: false,
                                label: prio[skillIndex].states[i].name
                            }
                        ],
                        name: prio[skillIndex].states[i].widget.name,
                        color: "white",
                        info: {
                            widget: prio[skillIndex].states[i].widget,
                            id: prio[skillIndex].states[i].id,
                            name: prio[skillIndex].states[i].name,
                            label: prio[skillIndex].states[i].name,
                            isStart: prio[skillIndex].states[i].start
                                ? prio[skillIndex].states[i].start
                                : false,
                            isEnd: prio[skillIndex].states[i].isEnd
                                ? prio[skillIndex].states[i].isEnd
                                : false,
                            transitions: transition,
                            text: prio[skillIndex].states[i].text
                                ? prio[skillIndex].states[i].text[0]
                                : ""
                        }
                    };
                    config["states"][skillIDs[j]].nodes.push(node);
                }
            }
        }
        return config;
    }

    getSessionManagerTransitions(config) {
        let sessionManagerTransitions = [];
        let allSessionManagerTransitions = [];
        let allSkills = [];
        config.forEach((skill) => {
            skill.states.forEach((state) => {
                if (state.start == true) {
                    state.transitions.forEach((transition) => {
                        allSessionManagerTransitions.push({
                            next_skill: skill.id,
                            conditions: transition.conditions,
                            logit: transition.logit === "AND" ? "AND" : "OR"
                        });
                    });
                }
            });
        });
        for (let allSessionManagerTransition of allSessionManagerTransitions) {
            if (allSessionManagerTransition.logit == "AND") {
                sessionManagerTransitions.push(allSessionManagerTransition);
            } else {
                if (allSkills.includes(allSessionManagerTransition.next_skill)) {
                    let skillIndex = sessionManagerTransitions.findIndex(
                        (element) =>
                            element.next_skill == allSessionManagerTransition.next_skill &&
                            element.logit == "OR"
                    );
                    let concatArray = sessionManagerTransitions[
                        skillIndex
                    ].conditions.concat(allSessionManagerTransition.conditions);
                    sessionManagerTransitions[skillIndex].conditions = concatArray;
                } else {
                    allSkills.push(allSessionManagerTransition.next_skill);
                    sessionManagerTransitions.push(allSessionManagerTransition);
                }
            }
        }

        return sessionManagerTransitions;
    }

    createUnknownTransition(sessionManagerID) {
        let unknowID = uuidv4();
        return {
            id: uuidv4(),
            offsetX: 0,
            offsetY: 0,
            zoom: 100,
            links: [],
            nodes: [
                {
                    id: unknowID,
                    _class: "DefaultNodeModel",
                    selected: true,
                    type: "default",
                    x: 0,
                    y: 10,
                    extras: {},
                    ports: [
                        {
                            id: uuidv4(),
                            _class: "DefaultPortModel",
                            selected: false,
                            name: "port01",
                            parentNode: unknowID,
                            links: [],
                            in: false,
                            label: " 不知道"
                        }
                    ],
                    name: "提示狀態",
                    color: "white",
                    info: {
                        widget: {
                            type: "message",
                            name: "提示狀態",
                            color: "peru",
                            dropColor: "white"
                        },
                        id: unknowID,
                        text: " 华华不懂你的意思",
                        name: " 不知道",
                        isStart: true,
                        transitions: [
                            {
                                entity: "",
                                next_skill: sessionManagerID,
                                conditions: [
                                    {
                                        operation: "intent",
                                        value: "unknown",
                                        id: "unknown"
                                    }
                                ]
                            }
                        ],
                        label: " 不知道"
                    }
                }
            ]
        };
    }

    arrayInclude(bigArray, smallArray) {
        for (let i = 0; i < bigArray.length; i++) {
            if (
                bigArray[i].includes(smallArray[0]) &&
                bigArray[i].includes(smallArray[1])
            ) {
                return true;
            }
        }
        return false;
    }

    addlink(skillConfig, sourceNodeId, targetNodeId, seslctedNodeId) {
        let sourcePort = skillConfig.nodes.filter(
            (node) => node.id === sourceNodeId
        )[0];
        let targetPort = skillConfig.nodes.filter(
            (node) => node.id === targetNodeId
        )[0];
        if (!sourcePort || !targetPort)
            return undefined
        let sourcePortId = sourcePort.ports[0].id;
        let targetPortId = targetPort.ports[0].id;
        let link = this.createLink(
            sourceNodeId,
            sourcePortId,
            targetNodeId,
            targetPortId,
            seslctedNodeId
        );
        return link;
    }

    createLink(
        sourceNodeId,
        sourcePortId,
        targetNodeId,
        targetPortId,
        seslctedNodeId
    ) {
        return {
            _class: "LinkModel",
            extras: {},
            id: uuidv4(),
            points: [
                {
                    _class: "PointModel",
                    id: uuidv4(),
                    selected: false,
                    x: 0,
                    y: 0
                },
                {
                    _class: "PointModel",
                    id: uuidv4(),
                    selected: false,
                    x: 0,
                    y: 0
                }
            ],
            selected:
                seslctedNodeId === sourceNodeId || seslctedNodeId === targetNodeId
                    ? true
                    : false,
            source: sourceNodeId,
            sourcePort: sourcePortId,
            target: targetNodeId,
            targetPort: targetPortId,
            type: "default"
        };
    }

    findStateLink(skill) {
        let pairs = [];
        skill.nodes.forEach((node) => {
            if (node.info.transitions) {
                node.info.transitions.forEach((transition) => {
                    if (transition.next_state) {
                        let tmpArray = [];
                        tmpArray.push(node.id);
                        tmpArray.push(transition.next_state);
                        if (!this.arrayInclude(pairs, tmpArray)) {
                            pairs.push(tmpArray);
                        }
                    }
                });
            }
        });
        return pairs;
    }

    findSkillLink(skills) {
        let pairs = [];
        for (let skillId in skills) {
            skills[skillId].nodes.forEach((node) => {
                if (node.info.transitions) {
                    node.info.transitions.forEach((transition) => {
                        if (transition.next_skill) {
                            let tmpArray = [];
                            tmpArray.push(skillId);
                            tmpArray.push(transition.next_skill);
                            if (!this.arrayInclude(pairs, tmpArray)) {
                                pairs.push(tmpArray);
                            }
                        }
                    });
                }
            });
        }
        return pairs;
    }

    findAllNodeId(skills) {
        let nodeId = [];
        for (let skillId in skills) {
            nodeId.push(skillId);
        }
        return nodeId;
    }

    showLinks(flow, selectNodeId) {
        if (!flow || !flow.content) {
            return;
        }
        let allNodesId = this.findAllNodeId(flow.content.states);
        allNodesId.forEach((skillId) => {
            let skill = flow.content.skills.nodes.find((node) => node.id === skillId);
            if (skill.info.showLink) {
                flow.content.states[skillId].links = [];
                let linkPairs = this.findStateLink(flow.content.states[skillId]);
                linkPairs.forEach((pair) => {
                    let link = this.addlink(
                        flow.content.states[skillId],
                        pair[0],
                        pair[1],
                        selectNodeId
                    );
                    if (link !== undefined) {
                        flow.content.states[skillId].links.push(link);
                    }
                });
            } else {
                flow.content.states[skillId].links = [];
            }
        });
        return flow;
    }

    addLinkDirect(flow, skillId, selectedState) {
        if (!flow || !flow.content || !(skillId in flow.content.states)) {
            return;
        }
        let nodes = flow.content.states[skillId].nodes;
        setTimeout(() => {
            flow.content.states[skillId].links.forEach((link) => {
                let sourceNode = nodes.find((node) => node.id === link.source);
                let targetNode = nodes.find((node) => node.id === link.target);
                if (document.querySelector(`[data-linkid="${link.id}"]`) !== null) {
                    // if (targetNode.info.transitions.find(transition => transition.next_state === sourceNode.id)) {
                    //     document.querySelector(`[data-linkid="${link.id}"]`).parentElement.querySelector("path").classList.add("alternate");
                    // }
                    if (selectedState && (selectedState.id === link.source || selectedState.id === link.target)) {
                        switch (selectedState.info.widget.type) {
                            case 'input':
                                document
                                    .querySelector(`[data-linkid="${link.id}"]`)
                                    .parentElement.querySelector("path")
                                    .classList.add("input");
                                break;
                            case 'message':
                                document
                                    .querySelector(`[data-linkid="${link.id}"]`)
                                    .parentElement.querySelector("path")
                                    .classList.add("message");
                                break;
                            case 'nlu':
                                document
                                    .querySelector(`[data-linkid="${link.id}"]`)
                                    .parentElement.querySelector("path")
                                    .classList.add("nlu");
                                break;
                            case 'api':
                                document
                                    .querySelector(`[data-linkid="${link.id}"]`)
                                    .parentElement.querySelector("path")
                                    .classList.add("api");
                                break
                        }
                    }


                    if (sourceNode.x > targetNode.x) {
                        document
                            .querySelector(`[data-linkid="${link.id}"]`)
                            .parentElement.querySelector("path")
                            .classList.add("reverse");
                    }
                }
            });
        }, 100);
    }
}

export const flowHelper = new FlowHelper();
