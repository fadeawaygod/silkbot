import React from 'react';

import { Button, Checkbox, Dropdown, Dialog, Switch } from 'element-react';
import 'element-theme-default';
import { Logger } from "../../utils/Logger";
import LinearLayout from "./LinearLayout";
import { flowHelper } from "./FlowHelper";

class SlotDialogSelector extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("SlotDialogSelector");
    this.logger.log("created#");
    this.state = {
      title: props.title,
      items: props.items,
      visible: props.visible,
      onCancel: props.closeDialog,
      conditions: [],
      condition: { id: '', operation: 'slot', value: '', slots: '' },
      entities: this.props.entities,
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.logger.log("componentWillReceiveProps#", nextProps);
    this.setup(nextProps);
  }

  setup(props) {
    this.setState({
      title: props.title ? props.title : this.state.title,
      items: props.items ? props.items : this.state.items,
      visible: props.visible,
      conditions: [],
      condition: { id: '', operation: 'slot', value: '', slots: '' },
      entities: this.props.entities
    })
  }

  findIntentSlot(intent, slotName) {
    const slots = intent.slots;
    return slots.find(slot => slot.name === slotName);
  }

  onSlotChanged(entity, slot) {

    const conditions = JSON.parse(JSON.stringify(this.state.conditions));
    this.logger.log("onSlotChanged# conditions:", conditions);
    this.logger.log("onSlotChanged# entity:", entity);
    this.logger.log("onSlotChanged# slot:", slot);

    let condition = conditions.find(c => c.slot === slot.name && c.value === entity.name);
    this.logger.log("onSlotChanged# condition:", condition);

    if (!condition) {
      condition = {
        id: entity.id,
        operation: "slot",
        value: entity.name,
        slot: slot.name
      }
      conditions.push(condition);
    }

    else {
      conditions.splice(conditions.findIndex(c => c.slot === slot.name && c.value === entity.name), 1);
    }

    this.setState({
      conditions
    })
  }

  hasSlot(entity, slot) {

    const condition = this.state.conditions.find(c => c.slot === slot.name && c.value === entity.name);
    return condition !== undefined && condition !== null
  }

  renderSlots(entity) {
    const slots = entity.slots;
    return slots.map((slot, idx) => {
      return <Checkbox key={`slot-${idx}`} onChange={this.onSlotChanged.bind(this, entity, slot)} checked={this.hasSlot(entity, slot)}>{slot.name}</Checkbox>
    })
  }

  onEntityClick(slot) {
    // const conditions = JSON.parse(JSON.stringify(this.state.conditions));

    // let condition = conditions.find(c => c.id === slot.id);

    // if (!condition) {
    //   condition = {
    //     id: slot.id,
    //     operation: "slot",
    //     value: slot.name,
    //     slot: ''
    //   };
    //   conditions.push(condition);
    // } else {
    //   conditions.splice(conditions.findIndex(c => c.id === slot.id), 1);
    // }
    // console.log("!!!!condition", conditions)

    // this.setState({
    //   conditions
    // })
  }

  renderItems() {
    const { conditions, entities } = this.state;
    if (!entities) {
      return
    }

    return entities.map((entity, idx) => {
      const condition = conditions.find(c => c.id === entity.id);
      return <LinearLayout key={`slot-${idx}`} orientation={'vertical'} align={"left"} style={{ marginTop: "20px" }}>
        <Button type={condition ? 'success' : 'default'} onClick={this.onEntityClick.bind(this, entity)} style={{ width: "100%", marginTop: "10px", textAlign: "left" }}>{idx}. {entity.name}</Button>
        <div key={`intent-slots-${idx}`} style={{ width: "100%", marginTop: "10px" }}>
          {this.renderSlots(entity)}
        </div>
      </LinearLayout>
    })
  }

  onCancel() {
    this.props.closeDialog();
  }

  onConfirm() {
    const conditions = flowHelper.objClone(this.state.conditions);


    if (!conditions) {
      alert("conditions clone error")
      this.props.closeDialog();
      return;
    }

    // conditions.forEach(c => {
    //   if (c.slots.length === 0) {
    //     delete c["slots"];
    //   }
    // });

    this.props.onConfirm(this.state.conditions);
    this.props.closeDialog();
  }

  render() {
    const { conditions } = this.state;
    return <Dialog
      title={this.state.title}
      size="small"
      visible={this.state.visible}
      onCancel={this.onCancel.bind(this)}
      lockScroll={false}
    >
      <Dialog.Body>
        <div style={{ minHeight: "400px", maxHeight: "500px", overflow: "scroll", padding: "30px" }}>
          {this.renderItems()}
        </div>
      </Dialog.Body>
      <Dialog.Footer className="dialog-footer">
        <Button onClick={this.onCancel.bind(this)}>取 消</Button>
        <Button type="primary" onClick={this.onConfirm.bind(this)}>确 定</Button>
      </Dialog.Footer>
    </Dialog>
  }
}

export default SlotDialogSelector;
