import React from 'react';

import { Button, Input } from 'element-react';
import 'element-theme-default';
import LinearLayout from "./LinearLayout";
import {flowHelper} from "./FlowHelper";
import {Logger} from "../../utils/Logger";

class SkillSetting extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("SkillSetting");
    this.state = {
      name: this.props.skill ? this.props.skill.name:'',
      skill: this.props.skill,
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.logger.log("componentWillReceiveProps#", nextProps.skill);
    this.setState({
      name: nextProps.skill ? nextProps.skill.name:'',
      skill: nextProps.skill,
    });
  }

  onInputChange(value) {
    this.setState({
      name: value
    });

    let skill = this.state.skill;
    flowHelper.setNodeName(skill, value);

    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }
  }

  removeSkill() {
    if (this.props.removeSkill) {
      this.props.removeSkill();
    }
  }

  removeLinks() {
    if (this.props.removeLinks) {
      this.props.removeLinks();
    }
  }

  render() {
    const {name, skill} = this.state;
    return <LinearLayout orientation={'vertical'} style={{position: "relative", width:"100%", background: "#cbc6d2", left: `0px`, padding: "10px"}} align={'top'}>
      <LinearLayout orientation={'horizontal'} align={'left'}>
        <div style={{width:"50px"}}>名稱: </div>
        <Input value={name ? name:''} onChange={this.onInputChange.bind(this)} />
      </LinearLayout>
      <LinearLayout orientation={'horizontal'} align={"left"} style={{marginTop:"10px"}}>
        <Button type={"danger"} onClick={this.removeSkill.bind(this)}>刪除</Button>
        <Button type={"danger"} style={{marginLeft:"10px"}} onClick={this.removeLinks.bind(this)}>刪除連線</Button>
      </LinearLayout>
      <LinearLayout orientation={'horizontal'} align={"left"} style={{marginTop:"10px", fontSize: "8pt"}}>
        {skill ? skill.id : ''}
      </LinearLayout>
    </LinearLayout>
  }
}

export default SkillSetting;
