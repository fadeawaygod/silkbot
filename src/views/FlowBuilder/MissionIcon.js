import React from 'react';
import './css/flow.less'
import robot_naughty from '@/assets/img/robot-naughty.svg';
import add_task from '@/assets/img/add-task.svg';
import message_state from '@/assets/img/message_state.svg';
import input_state from '@/assets/img/input_state.svg';
import api_state from '@/assets/img/api_state.svg';
import ai_state from '@/assets/img/ai_state.svg';
import message_state_hover from '@/assets/img/message_state_hover.svg';
import input_state_hover from '@/assets/img/input_state_hover.svg';
import api_state_hover from '@/assets/img/api_state_hover.svg';
import ai_state_hover from '@/assets/img/ai_state_hover.svg';

export default class MissionIcon extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hover: false,
        };
    }

    toggleOver() {
        this.setState(prevState => ({ hover: !prevState.hover }));
    }

    render() {
        const { onNodeClick, type } = this.props;
        const { hover } = this.state;
        let srcImage = add_task
        switch (type.type) {
            case 'message':
                srcImage = message_state;
                if (hover) {
                    srcImage = message_state_hover;
                }
                break;
            case 'input':
                srcImage = input_state;
                if (hover) {
                    srcImage = input_state_hover;
                }
                break;
            case 'nlu':
                srcImage = ai_state;
                if (hover) {
                    srcImage = ai_state_hover;
                }
                break;
            case 'api':
                srcImage = api_state;
                if (hover) {
                    srcImage = api_state_hover;
                }
                break;
        }

        return <img className="new-task-button" src={srcImage}
            draggable={true}
            onMouseEnter={this.toggleOver.bind(this)}
            onMouseLeave={this.toggleOver.bind(this)}
            onDragStart={event => {
                event.dataTransfer.setData('storm-diagram-node', JSON.stringify(this.props.type));
            }}
        />
    }
}
