import React from 'react';
import {
  DefaultNodeModel,
  DefaultPortModel,
} from 'storm-react-diagrams';

import './srd.css';
import FlowModel from "./FlowModel";
import { Logger } from "../../utils/Logger";
import 'element-theme-default';
import { Widgets } from "./data/StateWidgets";
import { flowHelper } from "./FlowHelper";
import './css/flow.less';


class StateEditor extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger('StateEditor');
    const { botId, skill, flow } = this.props;

    const diagramData = flowHelper.getStatesDiagram(flow, skill);
    this.logger.log("diagramData", diagramData);
    this.logger.log("diagramData#flow", flow);

    this.state = {
      widgets: Widgets,
      name: skill ? skill.name : '',
      skill,
      diagramData,
      dropNode: undefined,
      showLink: false
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { skill, botId, flow } = this.props;

    let diagramData = undefined;
    if (skill) {
      this.props.addStateLink(nextProps.flow)
      diagramData = flowHelper.getStatesDiagram(nextProps.flow, nextProps.skill);
    }
    this.setState({
      diagramData,
    })

    if (nextProps.botId !== botId || nextProps.skill !== skill) {
      if (nextProps.skill) {
        diagramData = flowHelper.getStatesDiagram(nextProps.flow, nextProps.skill);
        this.setState({
          name: nextProps.skill.name,
          skill: nextProps.skill,
          diagramData,
        })
      } else {
        this.setState({
          name: '',
          skill: undefined,
          diagramData: undefined
        })
      }
    }
  }

  componentDidUpdate() {
    const { skill, flow, selectedState } = this.props;
    if (skill && flow && flow.content) {
      flowHelper.addLinkDirect(flow, skill.id, selectedState);
      flowHelper.renderStateSetting();
    }
  }


  onNodeDrop(type, nodesCount) {
    this.logger.debug('onNodeDrop', `type:${type} nodesCount:${nodesCount}`);
    const { widgets, skill, diagramData } = this.state;
    const widget = widgets.find(w => w.type === type);
    let label = '';
    if (type === 'message') {
      label = '';
    }

    const node = new DefaultNodeModel(widget.name, widget.dropColor);
    node.addPort(new DefaultPortModel(false, 'port01', label));

    if (type === 'message') {
      node["info"] = { widget, id: node.id, text: '', name: label };
    }
    else {
      node["info"] = { widget, id: node.id, name: label };
    }
    flowHelper.addStateType(node.id, widget);
    this.setState({
      dropNode: node
    })
    return node;
  }

  linksUpdated(link, isAdded) {
    this.logger.debug("linksUpdated# isAdded(" + isAdded + ")", link);
    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }
  }

  nodesUpdated(node, isAdded) {
    this.logger.debug("nodesUpdated# isAdded(" + isAdded + ")", node);
    if (this.props.forceUpdateView && isAdded) {
      this.props.forceUpdateView();
    }
  }

  onNodeAdded(node) {
    this.logger.debug("onNodeAdded", node);
    if (this.props.onNodeAdded) {
      this.props.onNodeAdded(node);
    }
  }

  onNodeSelected(node, isSelected, diagramModel) {
    this.logger.debug(isSelected ? 'Selected' : 'Unselected', node);
    if (this.props.onStateSelected) {
      this.props.onStateSelected(node, isSelected, diagramModel.serializeDiagram());
    }
  }

  onLinkSelected(link, isSelected) {
    this.logger.debug(isSelected ? 'Selected' : 'Unselected', link);
    if (this.props.onLinkSelected) {
      this.props.onLinkSelected(link, isSelected);
    }
  }

  onDiagramModelUpdated(diagramModel) {
    this.props.onDiagramModelUpdated(diagramModel);
  }

  initDiagramEngine(engine) {
    if (this.props.initDiagramEngine) {
      this.props.initDiagramEngine(engine);
    }
  }

  onNodeMouseUp(oldX, oldY, newX, NewY) {
    if (this.props.skill.info.showLink) {
      if (this.props.saveFlowDiagram) {
        this.props.saveFlowDiagram();
      }
    }
    else {
      if (this.isSamePosition(oldX, oldY, newX, NewY) === false) {
        if (this.props.saveFlowDiagram) {
          this.props.saveFlowDiagram();
        }
      }
    }
  }

  isSamePosition(oldX, oldY, newX, newY) {
    if (oldX === newX && oldY === newY) {
      return true;
    }
    return false;
  }

  whetherShowLink() {
    this.setState({
      showLink: !this.state.showLink
    })
  }


  render() {
    const { widgets, diagramData, dropNode } = this.state;
    const { skill } = this.props;


    return <div className="fx fdc" style={{ width: "100%", flexGrow: "2" }}>
      <FlowModel
        diagramTag={'State'}
        widgets={widgets}
        init={this.initDiagramEngine.bind(this)}
        diagramData={skill ? diagramData : undefined}
        skill={skill ? skill : undefined}
        diagramDataLink={(skill && diagramData) ? diagramData.links : undefined}
        onNodeDrop={this.onNodeDrop.bind(this)}
        onNodeAdded={this.onNodeAdded.bind(this)}
        onNodeSelected={this.onNodeSelected.bind(this)}
        onLinkSelected={this.onLinkSelected.bind(this)}
        linksUpdated={this.linksUpdated.bind(this)}
        nodesUpdated={this.nodesUpdated.bind(this)}
        onNodeMouseUp={this.onNodeMouseUp.bind(this)}
        copyState={this.props.copyState.bind(this)}
        onNodeCopy={this.props.onNodeCopy.bind(this)}
        statetoCopy={this.props.statetoCopy}
        updateShowSkillLink={this.props.updateShowSkillLink.bind(this)}
        onDiagramModelUpdated={this.onDiagramModelUpdated.bind(this)}
      />
    </div>
  }
}

export default StateEditor;
