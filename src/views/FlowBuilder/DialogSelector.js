import React from 'react';

import { Button, Input, Dropdown, Dialog, Switch } from 'element-react';
import 'element-theme-default';
import { Logger } from "../../utils/Logger";
import LinearLayout from "./LinearLayout";

class DialogSelector extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("DialogSelector");
    this.logger.log("created#");
    this.state = {
      title: props.title,
      items: props.items,
      visible: props.visible,
      onCancel: props.closeDialog,
      onSelected: props.onSelected,
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.logger.log("componentWillReceiveProps#", nextProps);
    this.setup(nextProps);
  }

  setup(props) {
    this.setState({
      title: props.title ? props.title : this.state.title,
      items: props.items ? props.items : this.state.items,
      visible: props.visible,
      onCancel: props.closeDialog ? props.closeDialog : this.state.closeDialog,
      onSelected: props.onSelected ? props.onSelected : this.state.onSelected,
    })
  }

  onSelected(item) {
    if (this.state.onSelected) {
      this.state.onSelected(item)
    }
  }

  renderItems() {
    if (!this.state.items) {
      return '';
    }
    if (this.state.items[0].flow && this.state.items[0].test_result) {
      return this.state.items.map((item, idx) => {
        return this.props.getView(idx, item);
      })
    }
    else {
      return this.state.items.map((item, idx) => {
        return this.getView4StateAndSkill(idx, item)
      })
    }
  }


  getView4StateAndSkill(idx, item) {
    return <LinearLayout key={idx} orientation={'horizontal'} align={"left"}>
      <Button style={{ width: "100%", marginTop: "10px", textAlign: "left" }} onClick={this.onSelected.bind(this, item)}>{item.content} </Button>
    </LinearLayout>
  }

  onCancel() {
    this.props.closeDialog();
  }

  render() {
    this.logger.log(this.state.visible);
    return <Dialog
      title={this.state.title}
      visible={this.state.visible}
      onCancel={this.onCancel.bind(this)}
      lockScroll={false}
    >
      <Dialog.Body>
        <div style={{ height: "500px", overflow: "scroll" }}>
          {
            this.props.loading ? "加载中... 请稍后" : this.renderItems()
          }
        </div>
      </Dialog.Body>
    </Dialog>
  }
}

export default DialogSelector;
