import React from 'react';

import { Button, Input, Dropdown, Dialog, Switch } from 'element-react';
import 'element-theme-default';
import { Logger } from "../../utils/Logger";

class SavingProgress extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger("savingProgress Created!");
    this.visible = ''
    this.state = {
      content: '储存中... 请稍候'
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.show === true) {
      this.visible = ''
    }
    else {
      this.visible = 'hidden'
    }
  }

  render() {
    return (<div style={{ visibility: this.visible }}> {this.state.content} </div>)
  }
}

export default SavingProgress;
