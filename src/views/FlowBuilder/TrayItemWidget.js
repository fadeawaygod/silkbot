import React from 'react';
import './srd.css'

export default class TrayItemWidget extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return (
			<div
				style={{ background: this.props.color }}
				draggable={true}
				onDragStart={event => {
					event.dataTransfer.setData('storm-diagram-node', JSON.stringify(this.props.model));
				}}
				className="dialog-node"
			>
				{this.props.name}
			</div>
		);
	}
}
