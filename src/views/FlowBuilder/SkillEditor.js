import React from 'react';
import {
  DefaultNodeModel,
  DefaultPortModel,
} from 'storm-react-diagrams';

import FlowModel from "./FlowModel";
import { Logger } from "../../utils/Logger";
import LinearLayout from "./LinearLayout";
import { Button, Input } from "element-react";
import SkillComponent from "./SkillComponent"
import robot_naughty from '@/assets/img/robot-naughty.svg';
import add_task from '@/assets/img/add-task.svg';
import './css/flow.less';
import 'element-theme-default';

class SkillEditor extends React.Component {

  constructor(props) {
    super(props);
    this.logger = new Logger('SkillEditor');
    const { botId, flow } = props;
    const diagramData = flow && flow.content && flow.content.skills ? flow.content.skills : undefined;
    this.logger.log("diagramData", diagramData);
    this.logger.log("diagramData#flow", flow);
    this.state = {
      name: '',
      flow: flow,
      diagramData: diagramData ? diagramData : undefined,
      widgets: [{
        type: "skill",
        name: "任务",
        color: 'peru'
      }]
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {

    const { flow } = nextProps;
    if (flow) {
      const diagramData = flow && flow.content && flow.content.skills ? flow.content.skills : undefined;
      this.logger.log("componentWillReceiveProps#diagramData", diagramData);
      this.logger.log("componentWillReceiveProps#diagramData#flow", flow);
      this.setState({
        flow,
        diagramData: diagramData ? diagramData : undefined,
      })
    }

  }

  linksUpdated(link, isAdded) {
    this.logger.debug("node isAdded:" + isAdded, link);
    if (this.props.forceUpdateView) {
      this.props.forceUpdateView();
    }
  }

  nodesUpdated(node, isAdded) {
    this.logger.debug("node isAdded:" + isAdded, node);
    if (this.props.forceUpdateView && isAdded) {
      this.props.forceUpdateView();
    }
  }

  onNodeAdded(node) {
    this.logger.debug("onNodeAdded", node);
    if (this.props.onNodeAdded) {
      this.props.onNodeAdded(node);
    }
  }

  onNodeSelected(node, isSelected, diagramModel) {
    this.logger.debug(isSelected ? 'Selected' : 'Unselected', node);
    if (this.props.onSkillSelected) {
      this.props.onSkillSelected(node, isSelected, diagramModel.serializeDiagram());
    }

    if (isSelected) {
      this.setState({
        selectedSkill: node,
        name: node.name
      })
    } else {
      this.setState({
        selectedSkill: undefined,
        name: undefined
      })
    }
  }

  onLinkSelected(link, isSelected) {
    this.logger.debug(isSelected ? 'Selected' : 'Unselected', link);
    if (this.props.onLinkSelected) {
      this.props.onLinkSelected(node, isSelected);
    }
  }

  onNodeDrop(type, nodesCount) {
    this.logger.debug('onNodeDrop', `type:${type} nodesCount:${nodesCount}`);
    const name = '任务';
    let node = undefined;
    switch (type) {
      case 'skill':
        node = new DefaultNodeModel(name, 'white');
        node.addPort(new DefaultPortModel(false, "", ""));
        node["info"] = { isSkill: true, id: node.id, name: name };
        break;
    }
    return node;
  }

  onDiagramModelUpdated(diagramModel) {
    this.props.onDiagramModelUpdated(diagramModel);
  }

  onInputChange(value) {
    this.setState({
      name: value
    });
    this.props.onSkillNameChanged(value);
  }

  renderSkillSetting() {
    const { name } = this.state;

    return <LinearLayout orientation={'horizontal'} marginTop={"10px"} align={'left'}>
      任務
        <Input value={name} onChange={this.onInputChange.bind(this)} />
      <Button style={{ marginLeft: "10px" }}>新增</Button>
    </LinearLayout>
  }

  initDiagramEngine(engine) {
    if (this.props.initDiagramEngine) {
      this.props.initDiagramEngine(engine);
    }
  }

  onNodeMouseUp() {
    if (this.props.saveFlowDiagram) {
      this.props.saveFlowDiagram();
    }
  }

  render() {
    const { widgets, diagramData, name, flow } = this.state;
    return <div className="fx fdc" style={{ width: "100%", height: "0", overflow: "hidden" }}>
      <SkillComponent
        flow={flow}
        stateEngine={this.props.stateEngine}
        selectedSkill={this.props.selectedSkill}
      />
      <FlowModel
        diagramTag={'Skill'}
        init={this.initDiagramEngine.bind(this)}
        widgets={widgets}
        diagramData={diagramData}
        onNodeDrop={this.onNodeDrop.bind(this)}
        onNodeSelected={this.onNodeSelected.bind(this)}
        onLinkSelected={this.onLinkSelected.bind(this)}
        onNodeAdded={this.onNodeAdded.bind(this)}
        linksUpdated={this.linksUpdated.bind(this)}
        nodesUpdated={this.nodesUpdated.bind(this)}
        onNodeMouseUp={this.onNodeMouseUp.bind(this)}
        onDiagramModelUpdated={this.onDiagramModelUpdated.bind(this)}
      />
    </div>;
  }
}

export default SkillEditor;
