export default {
    web: {
        'bot_id': '',
        'passage': 'web',
        'credential': { 'callback_url': '', 'credentials': [] },
    },
    line: {
        'bot_id': '',
        'passage': 'line',
        'credential': {
            'channel_id': '',
            'channel_secret': '',
            'channel_access_token': ''
        },
    },
    messenger: {
        'bot_id': '',
        'passage': 'messenger',
        'credential': { 'token': '' },
    },
    telegram: {
        'bot_id': '',
        'passage': 'telegram',
        'credential': { 'token': '' },
    },
}