import router from "@/router";
const getters = {
  //cache
  user: state => state.cache.user,
  isLogin: state => state.cache.isLogin,
  currentPageInfo: state => state.cache.currentPageInfo,
  currentDialogCursor: state => state.cache.currentDialogCursor,
  //bot
  currentBot: state => [...state.botList_self, ...state.botList_share].find(bot => bot.id === router.currentRoute.params.bot_id),
  //botList
  botList: state => [...state.botList_self, ...state.botList_share],
  //UI
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  // token: state => "",
  // name: state => "silkrode",
}
export default getters
