import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import { mutationsBuilder, mutationFormat } from "@u/vuexHelper";
//modules
import app from './modules/app';
import cache from './modules/cache';
//api
import { getEntities } from "@/api/Entity";
import { getIntents, } from "@/api/Intent";
import { getBots } from "@/api/Bot";
import { getNotificationHistory } from "@/api/NotificationHistory";

Vue.use(Vuex)

const state = {
  isFirst: true,//首次進入
  notificationServiceReady: false,
  allowRole: [],
  infoShow: false,
  unreadNotificationHistory: [],
  routeTabs: [],
  //@mix/alert
  HttpStatusCode: -1,
  showMsgbox: false,
  isShield: false,
  pageLoading: false,
  //nav search
  navSearch_text: "",
  navSearch_callback: undefined,
  //bot
  currentEntities: [],
  currentIntents: [],
  currentBot: undefined,
  //=>botList
  botList_self: [],
  botList_share: [],
  //Breadcrumb
  pageInfo: undefined,
  //flowbuilder
  apiList_show: false,
  nluEndpoints_show: false,
  nluEndpoints: [],
  // NLU test
  nluTestString: "",
  //flow繁簡轉換
  chineseConvertShield_show: false
};
const mutations = mutationsBuilder(state);
mutations[mutationFormat("currentBot")] = function (state, bot) {
  bot = JSON.parse(JSON.stringify(bot));
  const currentBot = [...state.botList_self, ...state.botList_share].find(x => x.id === bot.id);
  bot.isOwner = currentBot.isOwner;
  bot.index = currentBot.index;

  let botList = bot.isOwner ? "botList_self" : "botList_share";
  Vue.set(state[botList], bot.index, bot);
}
mutations[mutationFormat("currentBotProperty")] = function (state, { property, value }) {
  let botList = this.getters.currentBot.isOwner ? "botList_self" : "botList_share";
  Vue.set(state[botList][this.getters.currentBot.index], property, value);
}
mutations[mutationFormat("targetBot")] = (state, bot) => {
  const targetBot = [...state.botList_self, ...state.botList_share].find(x => x.id === bot.id);
  if (!targetBot) return false;

  bot = JSON.parse(JSON.stringify(bot));
  bot.isOwner = targetBot.isOwner;
  bot.index = targetBot.index;

  let keyBeUpdated = bot.isOwner ? "botList_self" : "botList_share";
  Vue.set(state[keyBeUpdated], bot.index, bot);
}
mutations[mutationFormat("targetBotProperty")] = (state, { bot_id, property, value }) => {
  const targetBot = [...state.botList_self, ...state.botList_share].find(x => x.id === bot_id);
  if (!targetBot) return false;

  let botList = targetBot.isOwner ? "botList_self" : "botList_share";
  Vue.set(state[botList][targetBot.index], property, value);
}
const actions = {
  async updateEntities({ commit }, callback) {
    commit("SET_PAGE_LOADING", true);
    try {
      const res = await getEntities(this.getters.currentBot.id);
      commit("SET_CURRENT_ENTITIES", res.entities || []);
    }
    catch {
      commit("SET_CURRENT_ENTITIES", []);
    }
    finally {
      commit("SET_PAGE_LOADING", false);
      callback && callback();
    }
  },
  async updateIntents({ commit }, callback) {
    commit("SET_PAGE_LOADING", true);
    try {
      const res = await getIntents(this.getters.currentBot.id);
      commit("SET_CURRENT_INTENTS", res.intents || []);
    }
    catch {
      commit("SET_CURRENT_INTENTS", []);
    }
    finally {
      commit("SET_PAGE_LOADING", false);
      callback && callback();
    }
  },
  async updateMyBots({ commit }) {
    const result = await getBots(0, 0, true);
    commit("SET_BOT_LIST_SELF", result.bots.map((bot, index) => {
      bot.index = index;
      bot.isOwner = true;
      return bot;
    }))
  },
  async updateSharedByOthersBots({ commit }) {
    const result = await getBots(0, 0, false);
    commit("SET_BOT_LIST_SHARE", result.bots.map((bot, index) => {
      bot.index = index;
      bot.isOwner = false;
      return bot;
    }))
  },
  resetBots({ commit }) {
    commit("SET_BOT_LIST_SELF", [])
    commit("SET_BOT_LIST_SHARE", [])
  },
  async updateUnreadNotificationHistory({ commit }, callback) {
    try {
      const res = await getNotificationHistory(false);
      commit("SET_UNREAD_NOTIFICATION_HISTORY", res.notification_history || []);
      callback && callback();
    }
    catch {
      commit("SET_UNREAD_NOTIFICATION_HISTORY", []);
    }
  }
}
const store = new Vuex.Store({
  state,
  mutations,
  modules: {
    app,
    cache
  },
  getters,
  actions
})

export default store