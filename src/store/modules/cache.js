import { mutationsBuilder, cache_stateObj } from "@u/vuexHelper";

let state = new cache_stateObj();
//@mix/VuexCache
const cacheObj = JSON.parse(sessionStorage.getItem("CACHE"));
if (cacheObj != null && typeof cacheObj === 'object' && Object.keys(cacheObj).length) {
  for (let key in state) {
    if (typeof cacheObj[key] !== "undefined") {
      state[key] = cacheObj[key];
    }
  }
}
const mutations = mutationsBuilder(state);

export default {
  namespaced: true,
  state,
  mutations
}