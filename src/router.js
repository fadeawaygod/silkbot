import Vue from "vue";
import Router from "vue-router";
import store from "@/store";
import { ROLE } from "@u/model";
const { admin, group_admin, editor } = ROLE;
Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/activate",
      name: "activate",
      component: require("./views/activate/index.vue").default,
      meta: { next: true },
      hidden: true
    },
    {
      path: "/ocr",
      name: "ocr",
      component: require("./views/ocr/index.vue").default,
      meta: { next: true },
      hidden: true
    },
    {
      path: "/forgetPassword",
      name: "forgetPassword",
      component: require("./views/forgetPassword/forgetPassword").default,
      meta: { next: true },
      hidden: true
    },
    {
      path: "/resetPassword",
      name: "resetPassword",
      component: require("./views/forgetPassword/resetPassword.vue").default,
      meta: { next: true },
      hidden: true
    },
    {
      path: "/diagram",
      name: "diagram",
      component: require("./views/FlowBuilder/FlowBuilder.vue").default,
      meta: { next: true },
      hidden: true
    },
    {
      path: "/",
      name: "login",
      alias: "/login",
      component: require("./views/Login.vue").default,
      hidden: true
    },
    {
      path: "/signup",
      name: "signUp",
      component: require("./views/signUp.vue").default,
      meta: { next: true },
      hidden: true
    },
    {
      path: "/index",
      name: "index",
      component: require("./views/layout/index.vue").default,
      children: [
        {
          path: "/group",
          name: "group",
          component: require("./views/group/index.vue").default,
          meta: { title: "router.group", icon: "group", role: [admin] },
          main: true,
          children: [
            {
              path: "/group/:mode",
              name: "group_cu",
              component: require("./views/group/group_cu.vue").default,
              meta: { role: [admin] }
            }
          ]
        },
        {
          path: "/user",
          name: "user",
          component: require("./views/user/index.vue").default,
          meta: { title: "router.user", icon: "user", role: [admin, group_admin] },
          main: true,
          children: [
            {
              path: "/user/:mode",
              name: "user_cu",
              component: require("./views/user/user_cu.vue").default,
              meta: { role: [admin] }
            }
          ]
        },
        {
          path: "/bot",
          name: "bot",
          component: require("./views/bot/index.vue").default,
          meta: { title: "router.bot", icon: "bot", role: [admin, editor], tab: ["", "bot"] }
        },
        {
          path: "/bot/:bot_id/auth",
          name: "bot_auth",
          component: require("./views/bot/auth/authManagement.vue").default,
          meta: { title: "router.bot", role: [admin, editor] },
          hidden: true
        },
        {
          path: "/bot/:bot_id/intent",
          name: "bot_intent",
          component: require("./views/intent/index.vue").default,
          meta: { title: "bot.intent", icon: "intent", role: [admin, editor], tab: ["bot"] },
          main: true,
          children: [
            {
              path: "/bot/:bot_id/intent/utterance",
              name: "bot_utterance",
              meta: { role: [admin, editor], tab: ["bot"] }
            }
          ]
        },
        {
          path: "/bot/:bot_id/entity",
          name: "bot_entity",
          component: require("./views/entity/index.vue").default,
          meta: { title: "bot.entity", icon: "entity", role: [admin, editor], tab: ["bot"] },
          main: true,
          children: [
            {
              path: "/bot/:bot_id/entity/entry",
              name: "bot_entry",
              meta: { role: [admin, editor], tab: ["bot"] }
            }
          ]
        },
        {
          path: "/bot/:bot_id/dialog",
          name: "bot_dialog",
          main: true,
          redirect: "/bot/:bot_id/dialog/dialogList",
          component: require("./views/dialog/index.vue").default,
          meta: { title: "bot.dialog", icon: "dialog", role: [admin, editor], tab: ["bot"] },
          children: [
            {
              path: "/bot/:bot_id/dialog/dialogList",
              name: "dialogList",
              component: require("./views/dialog/dialogList.vue").default,
              meta: { role: [admin, editor], tab: ["bot"] }
            },
            {
              path: "/bot/:bot_id/dialog/dialogLabeling",
              name: "dialogLabeling",
              component: require("./views/dialog/dialogLabeling.vue").default,
              meta: { role: [admin, editor], tab: ["bot"] }
            }
          ]
        },
        {
          path: "/bot/:bot_id/flow",
          name: "bot_flow",
          component: require("./views/FlowBuilder/FlowBuilder.vue").default,
          meta: { title: "bot.flowManagement", icon: "form", role: [admin, editor], tab: ["bot"] },
          main: true,
          children: [
            {
              path: "/bot/:bot_id/flow/version_history",
              name: "versionHistory",
              component: require("./views/FlowBuilder/components/versionHistory.vue").default,
              meta: { role: [admin, editor], tab: ["bot"] }
            },
            {
              path: "/bot/:bot_id/flow/import_config",
              name: "chooseImportConfig",
              component: require("./views/FlowBuilder/components/chooseImportConfig.vue").default,
              meta: { role: [admin, editor], tab: ["bot"] }
            }
          ]
        },
        {
          path: "/bot/:bot_id/integration",
          name: "bot_integration",
          component: require("./views/integration/index.vue").default,
          meta: {
            title: "bot.integration",
            icon: "integration",
            role: [admin, editor],
            tab: ["bot"]
          }
        },
        {
          path: "/bot/:bot_id/analysis",
          name: "bot_analysis",
          component: require("./views/analysis/index.vue").default,
          meta: { title: "bot.analysis", icon: "bar-chart", role: [admin, editor], tab: ["bot"] },
          redirect: "/bot/:bot_id/analysis/usage",
          main: true,
          children: [
            {
              path: "/bot/:bot_id/analysis/usage",
              name: "usage",
              component: require("./views/analysis/usage.vue").default,
              meta: { role: [admin, editor], tab: ["bot"] }
            },
            {
              path: "/bot/:bot_id/analysis/trainLog",
              name: "trainLog",
              component: require("./views/analysis/trainLog.vue").default,
              meta: { role: [admin, editor], tab: ["bot"] }
            },
            {
              path: "/bot/:bot_id/analysis/publicOpinionAnalysis",
              name: "publicOpinionAnalysis",
              component: require("./views/analysis/publicOpinionAnalysis.vue").default,
              meta: { role: [admin, editor], tab: ["bot"] }
            }
          ]
        },
        {
          path: "/bot/:bot_id/customerServiceManagement",
          name: "customerServiceManagement",
          component: require("./views/customerServiceManagement/index.vue").default,
          meta: {
            title: "bot.customerServiceManagement",
            icon: "customerService",
            role: [admin, editor],
            tab: ["bot"]
          }
        },
        {
          path: "/bot/unauthorization",
          name: "bot_unauthorization",
          component: require("./views/bot/unauthorization.vue").default,
          meta: { role: [admin, editor] },
          hidden: true
        },
        {
          path: "/notificationHistory",
          name: "notificationHistory",
          component: require("./views/notificationHistory/index.vue").default,
          meta: {
            title: "router.messageCenter",
            icon: "notification",
            role: [admin, group_admin, editor]
          }
        }
      ]
    }
  ]
});
router.beforeEach((to, from, next) => {
  if (store.state.cache.isLogin && to.name === "login") {
    if (store.state.isFirst) next({ path: "/index" });
    else next(false);
    return false;
  }
  if ((to.meta || {}).next) {
    next();
    return false;
  }
  if (!store.state.cache.isLogin && to.name !== "login") {
    next({ path: "/" });
    return false;
  }
  next();
});
export default router;
