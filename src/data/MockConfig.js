export const ConfigTea = {
    "agent": {
        "session-manager": {
            "states": [
                {
                    "id": "state-user-input",
                    "start": true,
                    "runner": {
                        "type": "input"
                    },
                    "transitions": [
                        {
                            "conditions": [{
                                "intent": "ok"
                            }],
                            "next_state": "state-nlu"
                        }
                    ]
                },
                {
                    "id": "state-nlu",
                    "runner": {
                        "id": "ec_faq.csv",
                        "type": "nlu",
                        "url": "http://127.0.0.1:9999/rule/nlu"
                    },
                    "transitions": [
                        {
                            "conditions": [{
                                "intent": "買茶",
                                "slots": 0
                            }],
                            "next_skill": "Skill_買茶"
                        },
                        {
                            "conditions": [{
                                "intent": "unknown",
                                "slots": 0
                            }],
                            "next_skill": "skill_unknown"
                        },
                        {
                            "conditions": [{
                                "intent": "FAQ_運費計算",
                                "slots": 0
                            }],
                            "next_skill": "skill_delivery"
                        },
                        {
                            "conditions": [{
                                "intent": "FAQ_貨運公司",
                                "slots": 0
                            }],
                            "next_skill": "Skill_貨運公司"
                        },
                        {
                            "conditions": [{
                                "intent": "FAQ_退換貨",
                                "slots": 0
                            }],
                            "next_skill": "Skill_退換貨"
                        },
                        {
                            "conditions": [{
                                "intent": "FAQ_付款方式",
                                "slots": 0
                            }],
                            "next_skill": "Skill_付款方式"
                        },
                        {
                            "conditions": [{
                                "intent": "FAQ_茶葉保存",
                                "slots": 0
                            }],
                            "next_skill": "Skill_茶葉保存"
                        },
                        {
                            "conditions": [{
                                "intent": "FAQ_茶葉選購",
                                "slots": 0
                            }],
                            "next_skill": "Skill_茶葉選購"
                        }
                    ]
                }
            ]
        },
        "skills": [
            {
                "id": "Skill_買茶",
                "states": [
                    {
                        "id": "state-0",
                        "start": true,
                        "runner": {
                            "type": "message",
                            "prompt": "老闆您好，你想喝什麼茶?"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_state": "state-1"
                            }
                        ]
                    },
                    {
                        "id": "state-1",
                        "runner": {
                            "type": "message",
                            "prompt": "我們有\n珍珠奶茶\n紅茶\n鮮奶茶\n百香雙響炮\n隨便你點茶"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_state": "state-input-tea"
                            }
                        ]
                    },
                    {
                        "id": "state-input-tea",
                        "runner": {
                            "type": "input"
                        },
                        "transitions": [
                            {
                                "conditions": [{
                                    "intent": "ok"
                                }],
                                "next_state": "state-tea-nlu"
                            }
                        ]
                    }, {
                        "id": "state-tea-nlu",
                        "runner": {
                            "id": "ec_faq.csv",
                            "type": "nlu",
                            "url": "http://127.0.0.1:9999/rule/nlu"
                        },
                        "transitions": [
                            {
                                "conditions": [{
                                    "intent": "茶"
                                }],
                                "next_state": "state-ask-sugar-and-ice"
                            },
                            {
                                "conditions": [{
                                    "intent": "unknown"
                                }],
                                "next_state": "state-tea-unknown"
                            }
                        ]
                    }, {
                        "id": "state-tea-unknown",
                        "runner": {
                            "type": "message",
                            "prompt": "老闆不好意思我不懂您說什麼耶!"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_state": "state-1"
                            }
                        ]
                    }, {
                        "id": "state-ask-sugar-and-ice",
                        "runner": {
                            "type": "message",
                            "prompt": "好的，請問甜度冰塊?"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_state": "state-input-sugar-and-ice"
                            }
                        ]
                    }, {
                        "id": "state-input-sugar-and-ice",
                        "runner": {
                            "type": "input"
                        },
                        "transitions": [
                            {
                                "conditions": [{
                                    "intent": "ok"
                                }],
                                "next_state": "state-sugar-ice-nlu"
                            }
                        ]
                    }, {
                        "id": "state-sugar-ice-nlu",
                        "runner": {
                            "id": "ec_faq.csv",
                            "type": "nlu",
                            "url": "http://127.0.0.1:9999/rule/nlu"
                        },
                        "transitions": [
                            {
                                "conditions": [{
                                    "intent": "飲料屬性"
                                }],
                                "next_state": "state-finish"
                            },
                            {
                                "conditions": [{
                                    "intent": "unknown"
                                }],
                                "next_state": "state-re-ask-sugar-and-ice"
                            }
                        ]
                    }, {
                        "id": "state-finish",
                        "runner": {
                            "type": "message",
                            "prompt": "好的，老闆，已經收到您的訂單，我們正在努力趕工中! (對話結束)"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_skill": "session-manager"
                            }
                        ]
                    }, {
                        "id": "state-re-ask-sugar-and-ice",
                        "runner": {
                            "type": "message",
                            "prompt": "老闆不好意思我不懂您說什麼耶，你可以輸入 半糖少冰 無糖少冰..."
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_state": "state-input-sugar-and-ice"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "skill_unknown",
                "states": [
                    {
                        "id": "state-0",
                        "start": true,
                        "runner": {
                            "type": "message",
                            "prompt": "老闆您好，我看不懂您問的問題，您可以問我 (運費怎麼算、貨運是哪家、退換貨、付款方式、茶葉如何保存、茶葉怎麼選)"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "entities": [{
                                            "entity": "time",
                                            "prompt": "ask time",
                                            "ask_state": "ask_time_message_runner",
                                        },
                                        {
                                            "entity": "money",
                                            "message": "ask money",
                                            "ask_state": "xx_message_runner"
                                        }],
                                        "intent": "ok"
                                    }
                                ],
                                "next_skill": "session-manager"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "skill_delivery",
                "states": [
                    {
                        "id": "state-0",
                        "start": true,
                        "runner": {
                            "type": "message",
                            "prompt": "親，每筆訂單的運費為新台幣150元。 若單筆訂單金額達新台幣1,200元(含)以上則免運費(限台灣本島)。"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_skill": "session-manager"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "Skill_貨運公司",
                "states": [
                    {
                        "id": "state-0",
                        "start": true,
                        "runner": {
                            "type": "message",
                            "prompt": "親，負責配送的公司是 [台灣宅配通股份有限公司] 並可選擇貨到付款方式，購物金額上限為3 萬元。"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_skill": "session-manager"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "Skill_退換貨",
                "states": [
                    {
                        "id": "state-0",
                        "start": true,
                        "runner": {
                            "type": "message",
                            "prompt": "親，收到商品後請立即開啟檢視是否正確與完整，若有商品出貨錯誤、與訂購內容不符、品質異常等狀況，請保留出貨明細單與發票，於七日內與我們聯絡 (客服專線：(02) 2871-7811)，我們將儘速為您辦理退換貨。為維護商品的衛生安全，商品一經拆封後，除非商品本身有瑕疵(如，真空包裝破損、有異物)，恕不接受退換貨。退回之商品請保留完整的包裝及附件品，以利我們確認問題及保障您的權益。商品退／換貨時，請事先來電告知，請不要自行寄還給我們喔！若未事先通知，自行將商品寄回，恕我們將不予受理。"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_skill": "session-manager"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "Skill_付款方式",
                "states": [
                    {
                        "id": "state-0",
                        "start": true,
                        "runner": {
                            "type": "message",
                            "prompt": "親，我們共有\n1. [信用卡線上刷卡]\n2. [ATM轉帳]\n3. [超商代收]\n4. [貨到付款]"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_skill": "session-manager"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "Skill_茶葉保存",
                "states": [
                    {
                        "id": "state-0",
                        "start": true,
                        "runner": {
                            "type": "message",
                            "prompt": "茶忌潮濕、蒸鬱，喜乾燥清涼，請置放於通風處。從罐中取茶時，最好用茶勺，避免用手抓茶，因手常有汗水或其他不潔氣味，容易影響茶葉原味。\n\n如果真空包裝保存良好，茶葉其實可長久存放，若真空包無破損跡象、並且無曝曬在陽光下或高溫下，茶葉是可以飲用的。"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_skill": "session-manager"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "Skill_茶葉選購",
                "states": [
                    {
                        "id": "state-0",
                        "start": true,
                        "runner": {
                            "type": "message",
                            "prompt": "茶葉百百種，到底要如何找到適合自己的茶葉也是一門學問。\n但其實只要知道茶葉的基本屬性，你就可以找到適合自己飲用的茶葉。想要更了解嗎? 請至https://www.kingping.com.tw/%E8%8C%B6%E5%93%81%E6%8C%87%E5%8D%97/"
                        },
                        "transitions": [
                            {
                                "conditions": [
                                    {
                                        "intent": "ok"
                                    }
                                ],
                                "next_skill": "session-manager"
                            }
                        ]
                    }
                ]
            }
        ]
    }
};
