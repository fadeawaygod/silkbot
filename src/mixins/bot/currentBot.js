export default {
  computed: {
    currentBot() {
      return this.$store.getters.currentBot;
    },
  },
  watch: {
    currentBot: {
      handler(val, val0) {
        if (val) {
          if (val.id === (val0 || {}).id) return false;
          this.init && this.init();
        }
      },
      deep: true,
      immediate: true
    }
  }
}