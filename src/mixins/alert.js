export default {
  data() {
    return {
      current_m_error: undefined,
      current_m_error_top: undefined,
      NEM: true,
      errCodeMessageMapping: {
        10001: this.$tp("warn.unabledToTrain"),
        11000: this.$tp("warn.integrationNameDuplicate"),
        13006: this.$tp("warn.dilaogNameDuplicated"),
        14002: this.$tp("warn.accountAlreadyExist"),
        50004: this.$tp("warn.nameDuplicate"),
        50005: this.$tp("warn.uidDuplicate"),
        50013: this.$tp("warn.mailDuplicate"),
        50015: this.$tp("warn.mailAddressIsNotExist"),
        50020: this.$tp("warn.linkInvalid"),
        50021: this.$tp("warn.linkExpired"),
      }
    };
  },
  computed: {
    HttpStatusCode() {
      return this.$store.state.HttpStatusCode;
    }
  },
  mounted() {
    setTimeout(() => {
      this.NEM = false;
    }, 3000);
  },
  methods: {
    confirm(name) {
      return new Promise((resolve, reject) => {
        this.$confirm(this.$tp("warn.confirmDeleteWithName", { name }), this.$tp("warn.confirm"), {
          confirmButtonText: this.$tp("label.ok"),
          cancelButtonText: this.$tp("label.cancel"),
          type: "warning"
        }).then(() => {
          resolve();
        });
      });
    },
    m_error(msg, error_code) {
      if (this.NEM) return false;
      if (this.current_m_error) this.current_m_error.close();
      this.current_m_error = this.$notify.error({
        message: this.errorCodeToText(error_code) || msg,
        position: "bottom-right"
      });
    },
    m_error_top(msg, error_code) {
      if (this.NEM) return false;
      if (this.current_m_error_top) this.current_m_error_top.close();
      this.current_m_error_top = this.$message({
        showClose: true,
        message: this.errorCodeToText(error_code) || msg,
        type: 'error',
      });
    },
    m_success(msg, ex) {
      this.$notify({
        message: msg,
        type: "success",
        position: "bottom-right"
      });
    },
    errorCodeToText(error_code, parameter) {
      if (error_code === 10002) {
        return this.$tp("warn.utteranceNotEnough", { name: (parameter || {}).intent_name });
      }
      if (error_code === 10003) {
        return this.$tp("warn.labeledEntityNotEnough", { name: (parameter || {}).entity_name });
      }
      return this.errCodeMessageMapping[error_code];
    }
  },
  watch: {
    HttpStatusCode: {
      handler(val) {
        if (this.$te(`http.${val}`)) {
          this.$root.m_error_top(this.$tp(`http.${val}`));
          this.$store.commit("SET_HTTP_STATUS_CODE", -1);
        }
      }
    }
  }
};
