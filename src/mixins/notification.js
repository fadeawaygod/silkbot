export default {
  methods: {
    notificationToText(notification) {
      switch (notification.event) {
        case "bot_deploy_status":
          return this._botDeployStatusNotificationToText(notification);
        case "bot_train_status":
          return this._botTrainStatusNotificationToText(notification);
        case "bot_auth":
          return this._botAuthNotificationToText(notification);
        case "bot_event":
          return this._botEventToText(notification);
        case "update_nlu_data":
          return this._updateNluDataToText(notification);
        case "update_importing_dialog_status":
          return this._updateImportingDialogStatusToText(notification);
        default:
          console.log("unknown notification event", notification.event);
          return "unknown notification event"
      }
    },
    _botDeployStatusNotificationToText(notification) {
      const content = notification.content || {}
      switch (content.status) {
        case "ready":
          return this.$tp("description.botStatusReady", {
            name: content.bot_name
          })
        case "failed":
          return this.$tp("description.botStatusDeployFailed", { name: content.bot_name, err: content.failed_log })
        case "deploying":
          return this.$tp("description.botStatusDeploying", {
            name: content.bot_name
          })
        case "offline":
          return this.$tp("description.botStatusOffline", {
            name: content.bot_name
          })
        default:
          return this._botEventToText(notification)
      }
    },
    _botTrainStatusNotificationToText(notification) {
      const content = notification.content || {}
      switch (content.status) {
        case "failed":
          return this.$tp("description.botStatusTrainFailed", {
            name: content.bot_name,
            err: content.error_code ? this.$root.errorCodeToText(content.error_code, content.parameters) : content.failed_log
          })
        case "untrained":
          return this.$tp("description.botStatusUntrained", {
            name: content.bot_name
          })
        case "training":
          return this.$tp("description.botStatusTraining", {
            name: content.bot_name
          })
        case "trained":
          return this.$tp("description.botStatusTrained", {
            name: content.bot_name
          })

        default:
          return this._botEventToText(notification)
      }
    },
    _botAuthNotificationToText(notification) {
      const content = notification.content
      switch ((content || {}).type) {
        case "create":
          return this.$tp("description.botAuthCreate", {
            name: content.bot_name
          })
        case "update":
          return this.$tp("description.botAuthChange", {
            name: content.bot_name
          })
        case "delete":
          return this.$tp("description.botAuthDelete", {
            name: content.bot_name
          })
        default:
          console.log("unknown bot auth notification", notification);
      }
    },
    _botEventToText(notification) {
      return this.$tp(`botEvent.${notification.content.id}`)

    },
    _updateNluDataToText(notification) {
      return this.$tp('description.nluDataChanged')
    },
    _updateImportingDialogStatusToText(notification) {
      const content = notification.content
      switch ((content || {}).status) {
        case "start":
          return this.$tp("description.importDialogStart", {
            name: content.bot_name
          })
        case "success":
          return this.$tp("description.importDialogSuccess", {
            name: content.bot_name
          })
        case "failed":
          return this.$tp("description.importDialogFailed", {
            name: content.bot_name
          })
        default:
          console.log("unknown notification", notification);
      }
    }
  }
}