import { mutationFormat, cache_stateObj } from "@u/vuexHelper";
let readySync = true;
export default {
  computed: {
    cache() {
      return this.$store.state.cache;
    }
  },
  methods: {
    cacheInit() {
      readySync = false;
      setTimeout(() => {
        let initialValue = new cache_stateObj();
        for (let prop in initialValue) {
          this.$store.commit(`cache/${mutationFormat(prop)}`, initialValue[prop]);
        }
        readySync = true;
      }, 0)
    },
    syncTo() {
      sessionStorage.setItem("CACHE", JSON.stringify(this.cache));
    }
  },
  watch: {
    cache: {
      handler() {
        if (readySync) this.syncTo();
      },
      deep: true
    }
  }
}
