//mixins
import publicVue from "@mix/publicVue";
export default {
  mixins: [publicVue],
  computed: {
    App() {
      return this.getVue('App');
    }
  }
}