export default {
  computed: {
    navSearch_text() {
      return this.$store.state.navSearch_text;
    }
  },
  mounted() {
    this.$store.commit("SET_NAV_SEARCH_CALLBACK", () => {
      this.navSearch_callback();
      // this.$store.commit("SET_NAV_SEARCH_TEXT", "");
    });
  },
  beforeDestroy() {
    this.$store.commit("SET_NAV_SEARCH_CALLBACK", undefined);
    this.$store.commit("SET_NAV_SEARCH_TEXT", "");
  },
  methods: {
    navSearch_callback() {
      //implement
    },
    resetNavSearchText() {
      this.$store.state.navSearch_text = ''
    }
  }
}