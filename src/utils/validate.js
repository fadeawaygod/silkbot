/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */

const MIN_PASSWORD_LENGTH = 6;

const MAX_PASSWORD_LENGTH = 20;

export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path);
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ["admin", "editor"];
  return valid_map.indexOf(str.trim()) >= 0;
}
export function validMailAddress(mail) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(mail).toLowerCase());
}

export function validatePassword(password) {
  if (password.length < MIN_PASSWORD_LENGTH) {
    throw new PasswordTooShortError();
  } else if (password.length > MAX_PASSWORD_LENGTH) {
    throw new PasswordTooLongError();
  } else if (!/[a-z]/.test(password)) {
    throw new PasswordWithoutLowerCaseLetterError();
  } else if (!/[A-Z]/.test(password)) {
    throw new PasswordWithoutUpperCaseLetterError();
  } else if (!/[0-9]/.test(password)) {
    throw new PasswordWithoutNumberError();
  }

  for (const c of password) {
    if (!/^[a-z0-9!@#$%^&*()_+=-]+$/i.test(c)) throw new PasswordWithIllegalCharError(c);
  }
}

export function PasswordTooShortError() { }
export function PasswordTooLongError() { }
export function PasswordWithoutLowerCaseLetterError() { }
export function PasswordWithoutUpperCaseLetterError() { }
export function PasswordWithoutNumberError() { }
export function PasswordWithIllegalCharError(char) {
  this.char = char;
}
