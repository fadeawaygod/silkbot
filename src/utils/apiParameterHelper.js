import { convertDateBeginToTimeStamp } from "@/utils/time";

const YESTERDAY_INDEX = -1;
const TODAY_INDEX = 0;
const THIS_WEEK_INDEX = 1;
const CUSTOM_DATE_RANGE_INDEX = 2;

export function getRequestTimeStampParameters(timeUnitIndex, customDateRange) {
  let startDate, endDate;
  if (timeUnitIndex == TODAY_INDEX) {
    startDate = new Date();
    endDate = new Date();
    endDate.setDate(endDate.getDate() + 1);
  } else if (timeUnitIndex == THIS_WEEK_INDEX) {
    startDate = new Date();
    startDate.setDate(startDate.getDate() - 6);
    endDate = new Date();
    endDate.setDate(endDate.getDate() + 1);
  } else if (timeUnitIndex == CUSTOM_DATE_RANGE_INDEX) {
    if (customDateRange.length != 2) return;
    startDate = new Date(customDateRange[0].getTime());
    endDate = new Date(customDateRange[1].getTime());
    endDate.setDate(endDate.getDate() + 1);
  } else if (timeUnitIndex == YESTERDAY_INDEX) {
    startDate = new Date();
    startDate.setDate(startDate.getDate() - 1);
    endDate = new Date();
  }

  let startTimeStamp = convertDateBeginToTimeStamp(startDate);
  let endTimeStamp = convertDateBeginToTimeStamp(endDate);
  return { startTimeStamp, endTimeStamp };
}