import { time2Text } from "./time";
import { isDebug } from "../settings";


export class Logger {

    constructor(tag) {
        this.tag = tag;
    }

    log(...args) {
        if (!isDebug()) {
            return;
        }

        const len = args.length;
        switch (len) {
            case 1:
                console.log(`${time2Text(Date.now())} [${this.tag}] ${args[0]}`);
                break;
            case 2:
                console.log(`${time2Text(Date.now())} [${this.tag}] ${args[0]}`, args[1]);
                break;
        }
    }

    debug(...args) {

        if (!isDebug()) {
            return;
        }

        const len = args.length;
        switch (len) {
            case 1:
                console.log(`[DEBUG] ${time2Text(Date.now())} [${this.tag}] ${args[0]}`);
                break;
            case 2:
                console.log(`[DEBUG] ${time2Text(Date.now())} [${this.tag}] ${args[0]}`, args[1]);
                break;
        }
    }

    info(...args) {
        const len = args.length;
        switch (len) {
            case 1:
                console.log(`[INFO] ${time2Text(Date.now())} [${this.tag}] ${args[0]}`);
                break;
            case 2:
                console.log(`[INFO] ${time2Text(Date.now())} [${this.tag}] ${args[0]}`, args[1]);
                break;
        }
    }

    err(...args) {
        const len = args.length;
        switch (len) {
            case 1:
                console.error(`[ERROR] ${time2Text(Date.now())} [${this.tag}] ${args[0]}`);
                break;
            case 2:
                console.error(`[ERROR] ${time2Text(Date.now())} [${this.tag}] ${args[0]}`, args[1]);
                break;
        }
    }
    error(...args) {
        const len = args.length;
        switch (len) {
            case 1:
                console.error(`[ERROR] ${time2Text(Date.now())} [${this.tag}] ${args[0]}`);
                break;
            case 2:
                console.error(`[ERROR] ${time2Text(Date.now())} [${this.tag}] ${args[0]}`, args[1]);
                break;
        }
    }
}
