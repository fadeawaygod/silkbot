
export function isEmptyOrStartWithSpace(str) {
  return str === undefined || str === "" || str.startsWith(" ")
};
