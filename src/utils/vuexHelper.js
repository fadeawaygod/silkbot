import store from "@/store";

export function mutationsBuilder(stateObj) {
  let mutations = {};
  Object.keys(stateObj).forEach(el => {
    mutations[mutationFormat(el)] = (state, val) => {
      state[el] = val;
    }
  })
  return mutations;
}
export function mutationFormat(val) {
  return "SET_" + val.replace(/\B(?=[A-Z])+/g, '_').toUpperCase();
}
export function cache_stateObj() {
  return {
    user: {},
    isLogin: false,
    currentPageInfo: {},
    currentDialogCursor: {
      index: -1,
      skip: 0,
      limit: 10,
    }
  }
};

export function getNluEndpoints() {
  return JSON.parse(JSON.stringify(store.state.nluEndpoints));
};