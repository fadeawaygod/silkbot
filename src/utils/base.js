[String, Array].forEach(el => {
  el.prototype.has = function (val) {
    if (Array.isArray(val)) return val.map(x => this.valueOf().indexOf(x) > -1).indexOf(true) > -1;
    return this.valueOf().indexOf(val) > -1;
  }
})
String.prototype.isJSON = function () {
  try {
    let result = true;
    if (typeof JSON.parse(this.valueOf()) !== "object") {
      result = false;
    }
    if (!JSON.parse(this.valueOf())) {
      result = false;
    }
    return result;
  }
  catch (ex) {
    return false;
  }
}
console.__proto__.dev = function () {
  if (process.env.NODE_ENV === 'development') console.log(...arguments);
}