export function deepClone(obj) {
  try {
    return JSON.parse(JSON.stringify(obj));
  }
  catch {
    return undefined;
  }
}
export function injectTo(obj1, obj2) {//obj1 --> obj2
  for (const key in obj1) {
    if (!isUndefined(obj2[key])) {
      obj2[key] = deepClone(obj1[key] || "");
    }
  }
}
export function fetchFrom(obj1, obj2) {//obj1 <-- obj2
  for (const key in obj1) {
    if (!isUndefined(obj2[key])) {
      obj1[key] = deepClone(obj2[key] || "");
    }
  }
}
export function isSuccess(val) {
  return /^(ok)$/.test(val);
}
export function isUndefined(val) {
  return typeof val === "undefined";
}
export function isNumber(val) {
  return typeof val === "number";
}
export function isObject(val) {
  return typeof val === "object";
}
export function isEmpty(obj) {

  if (obj == null) return true;
  if (obj.length > 0) return false;
  if (obj.length === 0) return true;
  if (typeof obj !== "object") return true;
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}
export function downloadJson(data, fileName) {
  const dataStr = JSON.stringify(data);
  const blob = new Blob([dataStr], { type: "text/plain" });
  const e = document.createEvent("MouseEvents"),
    a = document.createElement("a");
  a.download = fileName + ".json";
  a.href = window.URL.createObjectURL(blob);
  a.dataset.downloadurl = ["text/json", a.download, a.href].join(":");
  e.initEvent(
    "click",
    true,
    false,
    window,
    0,
    0,
    0,
    0,
    0,
    false,
    false,
    false,
    false,
    0,
    null
  );
  a.dispatchEvent(e);
}
export function downloadFile(dataStr, fileName, format) {
  const blob = new Blob([dataStr], { type: "text/plain" });
  const e = document.createEvent("MouseEvents"),
    a = document.createElement("a");
  a.download = `${fileName}.${format}`;
  a.href = window.URL.createObjectURL(blob);
  a.dataset.downloadurl = ["text/plain", a.download, a.href].join(":");
  e.initEvent(
    "click",
    true,
    false,
    window,
    0,
    0,
    0,
    0,
    0,
    false,
    false,
    false,
    false,
    0,
    null
  );
  a.dispatchEvent(e);
}
export function objectTrimAll(obj) {
  for (let key in obj) {
    if (typeof obj[key] === 'string') {
      obj[key] = obj[key].trim();
    }
  }
}
export function DOM(selecor) {
  return document.querySelectorAll(selecor);
}