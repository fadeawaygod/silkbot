//const
export const ROLE = {
  admin: 'admin',
  group_admin: 'group_admin',
  editor: 'editor',
  // user: 'user'
}

//model
export function allowRole(roles) {
  let result = [];
  const allowRole_rule = {
    admin: {
      only: [],
      not: [ROLE.admin]
    },
    group_admin: {
      only: [],
      not: [ROLE.admin, ROLE.group_admin]
    }
  };
  for (let role of roles) {
    if (allowRole_rule[role]) {
      let temp_result = Object.keys(ROLE);
      if (allowRole_rule[role].only.length)
        temp_result = allowRole_rule[role].only;
      else if (allowRole_rule[role].not.length)
        temp_result = temp_result.filter(x => ![x].has(allowRole_rule[role].not))

      result.push(...temp_result);
    }
  }
  return [...new Set(result)];
}
export function USER() {
  return {
    uid: "",
    password: "",
    name: "",
    nick_name: "",
    mail_address: "",
    role: [],
    group: "",
    phone: "",
    avatar_url: ""
  }
}