import { version } from './version'

export function getVersion() {
  return version;
};
// const ENV = "fut";
export function getConfig(env) {
  // env = "local";

  if (env === "local") {
    return {
      authHost: 'chatbot.65lzg.com',
      host: 'localhost:5000',
      fileHost: 'chatbot.65lzg.com',
      fileHostHttpProtocol: 'https',
      hostHttpProtocol: 'http',
      notificationHost: 'chatbot.65lzg.com',
      gateway: 'localhost:8787',
      host_pocFile: 'https://chatbot.65lzg.com/customer_service/integration/resource?package=POC_IMAGE&filename=',
      FbGraphApiVersion: '8.0',
      csHubUrl: 'https://cshub.65lzg.com',
    }
  }

  return {
    authHost: 'chatbot.65lzg.com',
    host: 'chatbot.65lzg.com',
    hostHttpProtocol: 'https',
    fileHost: 'chatbot.65lzg.com',
    fileHostHttpProtocol: 'https',
    notificationHost: 'chatbot.65lzg.com',
    gateway: 'chatbot.65lzg.com',
    host_pocFile: 'https://chatbot.65lzg.com/customer_service/integration/resource?package=POC_IMAGE&filename=',
    FbGraphApiVersion: '8.0',
    csHubUrl: 'https://cshub.65lzg.com',
  }
};

export function getNluInfo() {
  return {

  }
}

export function isDebug() {
  return true;
};

