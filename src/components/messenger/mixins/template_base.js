//2021-02-02
import { speechToText, stopRecorder, stopSpeak } from "@js/speech/SpeechAPI";
import { getConfig } from "@/settings";
import { mapState } from "vuex";
const { host_pocFile } = getConfig();

const uploadImageRegExp = /\[\S+\]\((jpeg|jepg|png|jpg)\)\(\S*\)$/;
export default {
  props: {
    enable: {
      type: Boolean,
      default: true
    },
    //tts
    speak_enable: {
      type: Boolean,
      default: true
    },
    send_enable: {
      type: Boolean,
      default: true
    },
    msg_box_class: String,
    isLoading: {
      type: Boolean,
      default: false
    },
    inputMsg: String,
    msgList: Array,
    send: Function,
    sendImage: Function,
    updateBinding: Function,
    getLastHistory: Function //上拉加載事件
  },
  data() {
    return {
      container: undefined,
      inputMsg_self: "",
      msgIndex: undefined,
      show_endbtn: false,
      isGetLastHistory: false, //加載中
      scrollType: "end",
      moreHistory: true,
      //頂部顯示狀態
      moreHistory_show: false, //沒有更多訊息
      //upload image
      imageUrl: "", //base64
      imageFormData: undefined,
      imageType: "", //副檔名
      preview_show: false,
      zoomImageUrl: "",
      //speechToText
      speechToText: {
        click: 0,
        autoSend: true,
        guide_show: true
      },
      listener: {
        onStart: () => {
          this.listening = true;
        },
        onComplete: text => {
          if (this.speechToText.autoSend) this.send_self(text);
          else this.inputMsg_self = text;

          this.event_speechToText("init");
        },
        onError: result => {
          if (ENV_IS_DEV) console.log(result);
          this.event_speechToText("init");
        }
      },
      listening: false
    };
  },
  computed: {
    ...mapState(["showMsgbox"]),
    msgList_user() {
      return this.msgList.filter(x => x.from_user && x.message.type === "text" && x.message.text);
    },
    msgList_sort() {
      return this.msgList.map(x => {
        if (typeof x.message === "string") {
          x.message = {
            type: "text",
            text: x.message
          }
        }
        if (x.message.type === "text") {
          if (uploadImageRegExp.test(x.message.text)) {
            x.message.text = x.message.text.replace(/.*\[\S+\]\((jpeg|jepg|png|jpg)\)\(/, "").replace(/\)$/, "");

            x.message.type = "image";
            x.image_url = x.message.text.has("http") ? x.message.text : `${host_pocFile}${x.message.text}`;
          }
        }
        return x;
      }).sort((a, b) => a.updated - b.updated);
    },
    operation_enable() {
      //Interface
      //return Boolean;
    }
  },
  created() {
    //SpeechAPI
    stopSpeak();
    window.onbeforeunload = function (e) {
      stopSpeak();
    };
  },
  mounted() {
    this.inputMsg_self = this.inputMsg;
    this.container = this.$el.querySelector("#container");
    this.container.addEventListener("scroll", () => {
      //置底按鈕
      this.show_endbtn =
        this.container.offsetHeight + this.container.scrollTop + 10 <
        this.container.scrollHeight;
      //置頂加載
      if (
        this.container.scrollHeight > this.container.offsetHeight &&
        this.getLastHistory &&
        this.moreHistory &&
        this.container.scrollTop < 10 &&
        !this.isGetLastHistory &&
        this.msgList.length
      ) {
        this.isGetLastHistory = true; //加載中
        this.scrollType = this.msgList[0].updated;
        this.getLastHistory()
          .then(res => {
            this.moreHistory = res;
            this.scrollTo();
            //沒有更多訊息
            this.sp_moreHistory_show();
          })
          .finally(() => {
            this.isGetLastHistory = false; //加載中
          });
      }
      //沒有更多訊息
      this.sp_moreHistory_show();
    });
    window.addEventListener("resize", () => {
      //沒有更多訊息
      this.sp_moreHistory_show();
    });
  },
  beforeDestroy() {
    stopSpeak();//SpeechAPI
    if (this.close) this.close();
  },
  methods: {
    sp_moreHistory_show() {
      //沒有更多訊息
      this.moreHistory_show =
        this.container.scrollHeight > this.container.offsetHeight &&
        !this.moreHistory &&
        this.container.scrollTop === 0;
    },
    event_speechToText(type) {
      if (type === "init") {
        this.listening = false;
        this.speechToText.click = 0;
      }
      else if (type === "button") {
        if (this.listening) this.event_speechToText("stop");
        else this.event_speechToText("click");
      }
      else if (type === "click") {
        if (this.speechToText.click === 0) {
          setTimeout(() => {
            this.event_speechToText("check");
          }, 300);
        }
        this.speechToText.click++;
      }
      else if (type === "check") {
        if (this.speechToText.click === 1) {
          this.event_speechToText("start");
        }
        else if (this.speechToText.click > 1) {
          this.speechToText.autoSend = !this.speechToText.autoSend;
          this.event_speechToText("init");
        }
      }
      else if (type === "start") {
        if (!this.operation_enable) return false;
        stopSpeak();//SpeechAPI
        speechToText(this.listener);
      }
      else if (type === "stop") {
        if (!this.operation_enable) return false;
        if (this.listening) {
          stopRecorder();
          this.event_speechToText("init");
        }
      }
    },
    send_self(inputMsg) {
      if (!this.operation_enable) return false;
      this.scrollType = "end";
      if (this.sendImage && this.imageUrl && this.imageType) {
        this.sendImage(this.imageType, this.imageFormData).then(() => {
          this.imageType = this.imageUrl = "";
          this.imageFormData = undefined;
        });
      }
      if (typeof inputMsg === "object") inputMsg = undefined;
      this.send(inputMsg);
      this.msgIndex = this.msgList_user.length;
    },
    scrollTo(scrollType) {
      if (scrollType) this.scrollType = scrollType;
      if (this.scrollType === "end") {
        this.$nextTick(() => {
          this.container.scrollTop = this.container.scrollHeight;
        })
      } else
        this.container.scrollTop = this.container.querySelector(
          `#mid_${this.scrollType}`
        ).offsetTop;
    },
    getLastMsg() {
      if (typeof this.msgIndex === "undefined")
        this.msgIndex = this.msgList_user.length;
      if (this.msgIndex === 0) return false;
      this.msgIndex -= 1;
      this.inputMsg_self =
        ((this.msgList_user[this.msgIndex] || {}).message || {}).text || "";
    },
    getNextMsg() {
      if (typeof this.msgIndex === "undefined")
        this.msgIndex = this.msgList_user.length;
      if (this.msgIndex === this.msgList_user.length) return false;
      this.msgIndex += 1;
      this.inputMsg_self =
        ((this.msgList_user[this.msgIndex] || {}).message || {}).text || "";
    },
    previewImg(type, e) {
      const _target = {
        upload: "target",
        paste: "clipboardData"
      }[type];
      if (!e[_target].files.length) return false;
      //FormData
      let formData = new FormData();
      formData.append("file_to_upload", e[_target].files[0]);
      this.imageFormData = formData;
      //get file
      this.imageType = e[_target].files[0].name.replace(/^.*\./gm, "");
      const reader = new FileReader();
      reader.onload = e2 => {
        this.imageUrl = e2.target.result;
      };
      reader.readAsDataURL(e[_target].files[0]);
      e[_target].value = "";
      setTimeout(() => {
        document.getElementById("input").focus();
      }, 0);
    },
    uploadImge(e) {
      this.previewImg("upload", e);
    },
    pasteImage(e) {
      this.previewImg("paste", e);
    },
    //template message
    action_msgList(action) {
      if (action.type === "message") {
        this.send_self(action.text);
      }
      else if (action.type === "postback") {
        this.send_self(action.data);
      }
      else if (action.type === "uri") {
        window.open(action.uri);
      }
    }
  },
  watch: {
    msgList: {
      handler() {
        if (this.msgList.length === 0) this.scrollType = "end";
        if (this.scrollType === "end") this.scrollTo();
      },
      deep: true
    },
    inputMsg() {
      this.inputMsg_self = this.inputMsg;
    },
    inputMsg_self() {
      if (this.inputMsg_self === "") this.msgIndex = this.msgList_user.length;
      this.updateBinding("inputMsg", this.inputMsg_self);
    },
    imageUrl() {
      this.scrollTo("end");
    },
    showMsgbox(val) {
      if (!val) stopSpeak();
    },
    speak_enable(val) {
      if (!val) stopSpeak();
    },
  }
};