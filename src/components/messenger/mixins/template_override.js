export default {
  props: {
    msg_box_class: {
      type: String,
      default: "bs"
    }
  },
  computed: {
    operation_enable() {
      //override
      return !this.isLoading;
    }
  }
}