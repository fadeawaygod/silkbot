import axiosProxy from "./config";
const { hostHttpProtocol, host } = getConfig();
import { getConfig } from "@/settings";

export function send(token, user_id, group_id, message) {
  return axiosProxy({
    baseURL: `${hostHttpProtocol}://${getConfig("fut").gateway}`,
    url: '/gateway/passage/demo/send_message',
    method: "post",
    params: {
      token
    },
    data: {
      user_id,
      group_id,
      message
    }
  })
}

export function history({ token, user_id, bot_id, skip, limit }) {
  return axiosProxy({
    url: '/api/v1/editor/chathistory',
    method: "get",
    headers: {
      Authorization: `Bearer ${token}`
    },
    params: {
      user_id,
      bot_id,
      source_from: "demo",
      skip: skip || 0,
      limit
    }
  })
}

export function integrationsToken(token, bot_id) {
  return axiosProxy({
    url: '/api/v1/editor/integrations',
    method: "get",
    headers: {
      Authorization: `Bearer ${token}`
    },
    params: {
      bot_id,
      source_from: "demo"
    }
  })
}
