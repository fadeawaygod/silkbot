import axios from 'axios';
import { getConfig } from "@/settings";
import store from '@/store';

const timeout = 60000;
const { hostHttpProtocol, host } = getConfig();
const axiosProxy = axios.create({
  baseURL: `${hostHttpProtocol}://${host}`,
  timeout
})
axiosProxy.defaults.timeout = timeout;
axiosProxy.defaults.headers.post['Content-Type'] = 'application/json';
axiosProxy.defaults.headers.patch['Content-Type'] = 'application/json';

axiosProxy.interceptors.request.use(request => {
  return request
}, function (error) {
  return Promise.reject(error);
});
axiosProxy.interceptors.response.use(
  response => {
    return response.data
  },
  error => {
    const status = ((error || {}).response || {}).status;//HttpStatusCode
    console.dev(error.response);
    if (status === 401) {
      store.commit("cache/SET_IS_LOGIN", false);
      store.commit("SET_HTTP_STATUS_CODE", status);
    }
    else if (status === 403) {
      store.commit("SET_HTTP_STATUS_CODE", status);
    }
    return Promise.reject(error)
  }
)

export default axiosProxy
