import axiosProxy from './ChatbotAPIHttpClient';

export function getUtterances(intentId, skip, limit, keyword = '') {
  let params = {
    intent_id: intentId,
    show_len: true,
    skip,
    limit
  }

  if (keyword) params.keyword = keyword

  return axiosProxy({
    url: '/utterances',
    method: 'get',
    params: params
  })
};

export function createUtterance(intent_ids, utterance) {
  return axiosProxy({
    url: '/utterances',
    method: 'post',
    data: {
      "intent_ids": intent_ids,
      "text": utterance.text,
      "entities": utterance.entities
    }
  })
}

export function removeUtterance(utterance) {
  return axiosProxy({
    url: `/utterances/${utterance.id}`,
    method: 'delete'
  })
};

export function updateUtterance(utterance) {
  let data = {}
  if (utterance.intent_ids) data["intent_ids"] = utterance.intent_ids
  if (utterance.text) data["text"] = utterance.text
  if (utterance.entities) data["entities"] = utterance.entities
  return axiosProxy({
    url: `/utterances/${utterance.id}`,
    method: 'patch',
    data
  })
}

export function importUtterances(botId, data) {
  return axiosProxy({
    url: `/bots/${botId}/import_utterances`,
    method: 'post',
    data
  })
}