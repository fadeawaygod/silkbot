import { post } from "./HttpClient";
import { getConfig } from "../settings";
import axiosProxy from "./ChatbotAPIHttpClient";

const ENV = "fut";
const URL_REFRESH_TOKEN = `https://${getConfig(ENV).authHost}/auth/refreshToken`;
const URL_ACTIVATE = `https://${getConfig(ENV).authHost}/auth/activate`;
const URL_VALIDATE_PASSWORD_CODE = `https://${getConfig(ENV).authHost}/auth/validatePasswordCode`;

export function activate(code) {
  return post(URL_ACTIVATE, undefined, { code }).then(result => {
    return Promise.resolve(result);
  });
}

export function validatePasswordCode(code) {
  return post(URL_VALIDATE_PASSWORD_CODE, { "Content-Type": "application/json" }, { code }).then(
    result => {
      return Promise.resolve(result);
    }
  );
}

export function resetPassword(uid, code, password) {
  return axiosProxy({
    url: "/reset_password",
    method: "post",
    data: {
      uid: uid,
      token: code,
      password: password,
    }
  });
}

export function resetPasswordMail(email) {
  return axiosProxy({
    url: "/reset_password_mail",
    method: "post",
    data: {
      mail_address: email,
    }
  });
}

export function signIn(userId, password, clientId) {
  return axiosProxy({
    url: "/signin",
    method: "post",
    data: {
      uid: userId,
      password: password,
      client_id: clientId,
    }
  });
}
export function signUp(userId, mail, password) {
  return axiosProxy({
    url: "/signup",
    method: "post",
    data: {
      uid: userId,
      password: password,
      mail_address: mail
    }
  });
}
export function checkUser(userId, mail) {
  return axiosProxy({
    url: "/check_user",
    method: "get",
    params: {
      uid: userId,
      mail_address: mail
    }
  });
}
export function activateUser(userId, token) {
  return axiosProxy({
    url: "/user_activation",
    method: "post",
    data: {
      uid: userId,
      token: token
    }
  });
}

export function resendActivationMail(uid) {
  return axiosProxy({
    url: "/activation_mail",
    method: "post",
    data: {
      uid: uid,
    }
  });
}

export function refreshToken() {
  const user = getUser();
  return post(
    URL_REFRESH_TOKEN,
    {
      Authorization: `Bearer ${user.access_token}`,
      "Content-Type": "application/json"
    },
    { refreshToken: user.refreshToken }
  ).then(resp => {
    user["access_token"] = resp.access_token;
    user["refreshToken"] = resp.refreshToken;
    localStorage.setItem("user", JSON.stringify(user));
    return Promise.resolve(user);
  });
}

export function getUser() {
  const userText = localStorage.getItem("user");
  return userText ? JSON.parse(userText) : {};
}

export function signOut() {
  return axiosProxy({
    url: "/signout",
    method: "post"
  });
}

export function getRole(roles) {
  if (!roles || roles.length === 0) {
    return "unknown";
  }

  if (roles.find(role => role === "admin") !== undefined) {
    return "admin";
  }

  if (roles.find(role => role === "editor") !== undefined) {
    return "editor";
  }

  if (roles.find(role => role === "sys_admin") !== undefined) {
    return "sys_admin";
  }

  if (roles.find(role => role === "group_admin") !== undefined) {
    return "group_admin";
  }

  if (roles.find(role => role === "user") !== undefined) {
    return "user";
  }

  return "unknown";
}
