import axiosProxy from './ChatbotAPIHttpClient';

export function exchangeLongLivedAccessToken(userAccessToken, pageIds) {
  let params = {
    user_access_token: userAccessToken,
    page_ids: pageIds.join(',')
  }
  return axiosProxy({
    url: '/fb/long_lived_access_token',
    method: 'get',
    params
  })
}









