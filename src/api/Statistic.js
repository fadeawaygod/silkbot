import axiosProxy from './ChatbotAPIHttpClient';
const DEFAULT_PRIORITY = 10;

export function getStatistic(botID, startTime, endTime, show_day_statistic, integrationPassage, integrationName) {
  let params = { start_time: startTime, end_time: endTime }

  if (show_day_statistic) params.show_day_statistic = show_day_statistic
  if (integrationPassage) params.passage = integrationPassage
  if (integrationName) params.integration_name = integrationName
  return axiosProxy({
    url: `/bots/${botID}/statistic`,
    method: 'get',
    params: params
  })
};

export function getTrainLog(botID, skip, limit, show_len, startTime, endTime) {
  let params = {}
  if (skip) params.skip = skip
  if (limit) params.limit = limit
  if (show_len) params.show_len = show_len
  if (startTime) params.start_time = startTime
  if (endTime) params.end_time = endTime

  return axiosProxy({
    url: `/bots/${botID}/train_log`,
    method: 'get',
    params: params
  })
};

export function getSentimentAnalysis(botID, startTime, endTime) {
  let params = { start_date_timestamp: startTime, end_date_timestamp: endTime }
  return axiosProxy({
    url: `/bots/${botID}/sentiment_analysis`,
    method: 'get',
    params: params
  })
};
export function getIntentUsage(botID, startTime, endTime) {
  let params = { start_time: startTime, end_time: endTime }

  return axiosProxy({
    url: `/bots/${botID}/intent_usage`,
    method: 'get',
    params: params
  })
};