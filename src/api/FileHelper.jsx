import axios from 'axios'


export function uploadFile(url, file, onUploadProgressCallback) {

    if (!url) {
        return Promise.reject("url cannot be undefined");
    }

    if (!file) {
        return Promise.reject("file cannot be undefined");
    }

    //const data = new FormData();
    console.log("uploadFile#", file);
    //data.append('file', file, file.name);

    return axios
        .post(url, file, {
            onUploadProgress: onUploadProgressCallback,
            headers: {
                "Content-Type": "multipart/form-data"
            }
        })
        .then(res => {
            if (res.status === 200 && res.data.status === 'ok') {
                return Promise.resolve(res.data.url);
            } else {
                return Promise.reject("upload failed!");
            }
        })
};

export function removeFile(url) {
    return axios.delete(url)
};