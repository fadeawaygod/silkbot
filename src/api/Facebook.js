import axios from 'axios'
import { getConfig } from '../settings';

const axiosProxy = axios.create({
  baseURL: `https://graph.facebook.com/v${getConfig('fut').FbGraphApiVersion}/`,
})
axiosProxy.defaults.timeout = 3000;

export function getAccount(accessToken) {
  return axiosProxy({
    url: '/me/accounts',
    method: 'get',
    params: { access_token: accessToken }
  })
};