import axiosProxy from './ChatbotAPIHttpClient';


export function getBotAuth(botId) {
  return axiosProxy({
    url: `/bots/${botId}/auth`,
    method: 'get'
  })
};

export function createBotAuth(botId, userId, permission) {
  return axiosProxy({
    url: `/bots/${botId}/auth`,
    method: 'post',
    data: {
      "user_id": userId,
      "permission": permission,
    }
  })
}

export function removeBotAuth(botId, userId) {
  return axiosProxy({
    url: `/bots/${botId}/auth/${userId}`,
    method: 'delete'
  })
}

export function updateBotAuth(botId, userId, permission) {
  console.log();

  const url = `/bots/${botId}/auth/${userId}`
  return axiosProxy({
    url,
    method: 'patch',
    data: permission
  })
}
