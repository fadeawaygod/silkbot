import { deleteRequest, get as getRequest, post } from './HttpClient';
import { getUser } from './Auth';
import axiosProxy from './ChatbotAPIHttpClient';
import { getConfig } from '../settings';

const URL_MESSAGE = 'http://35.234.18.71/api/v1/bot/messages';

export function syncMessages(agentId) {
  const url = URL_MESSAGE + `?agent_id=${agentId}&start_position=0&limit=100&is_ascending=false`;
  const user = getUser();
  const headers = { 'Authorization': `Bearer ${user.token}` };
  return getRequest(url, headers, undefined).then(response => {
    const messages = response.history ? response.history : [];
    messages.sort((a, b) => a.update_time - b.update_time);
    updateMessages(agentId, messages);
    return Promise.resolve(messages);
  })
}

export function getMessages(agentId) {

  const jsonText = localStorage.getItem(`message_${agentId}`);
  const messageSet = jsonText ? JSON.parse(jsonText) : [];

  const found = messageSet.find(obj => {
    return obj.agentId === agentId
  });

  if (found) {
    return found.messages;
  }
  return [];
};

export function updateMessages(agentId, messages) {

  const jsonText = localStorage.getItem(`message_${agentId}`);
  const messageSet = jsonText ? JSON.parse(jsonText) : [];

  const found = messageSet.find(obj => {
    return obj.agentId === agentId
  });

  if (found) {
    found.messages = messages
  } else {
    messageSet.push({
      agentId: agentId,
      messages: messages
    })
  }
  localStorage.setItem(`message_${agentId}`, JSON.stringify(messageSet));
}

export function sendMessage(agent, user, message) {
  let url = `${URL_MESSAGE}/${agent.id}`;
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${user.token}`,
  };
  const body = { message };
  return post(url, headers, body).then((response) => {
    console.log("sendMessage#response:", response);
    let messages = getMessages(agent.id);
    if (!messages) {
      messages = [];
    }
    messages.push({
      message: message,
      update_time: Date.now(),
      read: false,
      sender: user.id,
    });

    updateMessages(agent.id, messages);
    return Promise.resolve()
  })
};

export function addBotMessage(agentId, userId, message, created) {
  console.log("addBotMessage#", message);
  let messages = getMessages(agentId);
  if (!messages) {
    messages = [];
  }

  if (messages.find(_message => _message.update_time === created)) {
    // console.error("addBotMessage#duplicated message:", message + ", " + created);
    return;
  }

  messages.push({
    message: message,
    update_time: created,
    read: false,
    sender: agentId,
  });
  updateMessages(agentId, messages);
};

export function clearMessages(agent, user) {
  let url = `${URL_MESSAGE}`;
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${user.token}`,
  };

  const body = { agent_id: agent.id };
  return deleteRequest(url, headers, body).then((response) => {
    console.log("clearMessages#response:", response);
    localStorage.removeItem(`message_${agent.id}`);
    return Promise.resolve()
  })
};

export function getChatHistory(message) {
  return axiosProxy({
    url: '/chathistory',
    method: 'get',
    params: {
      user_id: message.userID,
      bot_id: message.botID,
      source_from: message.sourceFrom,
      limit: message.limit,
      skip: message.skip
    }
  })
};

//上傳圖片
export function pocFile(formData) {
  return axiosProxy({
    baseURL: `https://${getConfig().host}`,
    url: "/pocFile",
    method: "post",
    headers: {
      ["Content-Type"]: "multipart/form-data"
    },
    data: formData
  })
}