import axiosProxy from './ChatbotAPIHttpClient';

export function getGroups(skip, limit, keyword = '', show_len = 'false') {
  let params = { skip, limit, show_len }
  if (keyword) params.keyword = keyword
  return axiosProxy({
    url: '/groups',
    method: 'get',
    params: params,
  })
};

export function createGroup(groupName) {
  return axiosProxy({
    url: '/groups',
    method: 'post',
    data: {
      "name": groupName
    }
  })
}

export function removeGroup(group) {
  return axiosProxy({
    url: `/groups/${group.id}`,
    method: 'delete'
  })
};

export function updateGroup(group) {
  let data = { "name": group.name }
  return axiosProxy({
    url: `/groups/${group.id}`,
    method: 'patch',
    data
  })
}