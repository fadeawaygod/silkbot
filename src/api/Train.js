import axiosProxy from './ChatbotAPIHttpClient';

export function train(botID) {
  let data = { "bot_id": botID }

  return axiosProxy({
    url: '/train',
    method: 'post',
    data
  })
}
