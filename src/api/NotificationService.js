import { Parse } from "parse"
import { getConfig } from "../settings";
import { Logger } from "../utils/Logger";


class NotificationService {

  constructor() {
    this.logger = new Logger("NotificationService")
    this.connection = undefined;
    this.logger.log("NotificationService is created!");
    this.listeners = [];
  }

  equals(obj1, obj2) {
    if (obj1 === obj2) {
      return true;
    }

    let ret = true;
    Object.keys(obj1).forEach(key => {
      if (!obj2[key]) {
        ret = false;
      } else {
        if (obj2[key] !== obj1[key]) {
          ret = false;
        }
      }
    })
    return ret;
  }

  findListener(subscription) {
    return this.listeners.find(listenerObj => listenerObj.subscription === subscription);
  }

  findListenerIndex(subscription) {
    return this.listeners.findIndex(listenerObj => listenerObj.subscription === subscription);
  }

  addListener(subscription, listener) {
    if (this.findListener(subscription)) {
      this.logger.log("subscription is existed, subscription:", subscription);
      return;
    }

    this.logger.log("subscription is added, subscription:", subscription);
    this.listeners.push({
      subscription,
      listener
    })
  }

  removeListener(subscription) {
    const index = this.findListenerIndex(subscription);
    if (index === -1) {
      this.logger.log("subscription is not existed");
      return;
    }

    this.listeners.splice(index, 1);
    this.logger.log("subscription has been removed");
  }

  init(onOpenedCallback) {
    console.log("init");

    if (this.connection) {
      if (onOpenedCallback) {
        onOpenedCallback();
      }
      this.logger.log("init already.");
      return;
    }

    const host = getConfig('fut').notificationHost;
    const hostInfo = {
      applicationId: "silkrode-chatbot",
      serverURL: `wss://${host}/realtime`,
      javascriptKey: 'live-app-key-silkrode',
    };

    Parse.initialize("silkrode-chatbot", 'live-app-key-silkrode');
    Parse.serverURL = `https://${host}/realtime/realtime`;
    const realTimeConn = new Parse.LiveQueryClient(hostInfo);

    realTimeConn.open();
    realTimeConn.on("open", () => {
      this.logger.log("#init# server connection is open successfully");
      this.connection = realTimeConn;
      if (onOpenedCallback) {
        onOpenedCallback();
      }
    });

    realTimeConn.on("error", (obj) => {
      this.logger.error("on error:", obj)
    }
    );
  }

  subscribe(listener, filter) {

    if (!this.connection) {
      this.logger.error("subscribe# error: connection is undefined");
      return undefined;
    }

    this.logger.log("subscribe# filter:", filter);

    const query = new Parse.Query("notification");
    Object.keys(filter).forEach(key => {
      if(Array.isArray(filter[key]))
        query.containedIn(key, filter[key]);
      else
        query.equalTo(key, `${filter[key]}`);
    });

    const subscription = this.connection.subscribe(query);

    if (subscription) {
      this.addListener(subscription, listener);
    }

    subscription.on("open", (data) => {
      this.logger.log("subscription opened", data);
      const listenerObj = this.findListener(subscription);
      listenerObj && listenerObj.listener.onOpen(data);
    });

    subscription.on("create", (data) => {
      const obj = data.toJSON();
      this.logger.log("subscription created", obj);
      const listenerObj = this.findListener(subscription);
      listenerObj && listenerObj.listener.onCreate(obj);
    });

    subscription.on("delete", (data) => {
      const obj = data.toJSON();
      this.logger.log("subscription deleted", obj);
      const listenerObj = this.findListener(subscription);
      listenerObj && listenerObj.listener.onDelete(obj);
    });

    subscription.on("update", (newData, oldData) => {
      const newObj = newData.toJSON();
      const oldObj = oldData.toJSON();
      this.logger.log("subscription updated new obj:", newObj);
      this.logger.log("subscription updated old obj:", oldObj);
      const listenerObj = this.findListener(subscription);
      listenerObj && listenerObj.listener.onUpdate(newObj, oldObj);
    });
    return subscription;
  }

  unsubscribe(subscription) {
    if (!this.connection) {
      this.logger.debug("unsubscribe# connection cannot be undefined!");
      return;
    }

    if (!subscription) {
      this.logger.debug("unsubscribe# subscription cannot be undefined!");
      return;
    }

    this.logger.log("unsubscribe#", subscription);
    subscription.unsubscribe();
    this.removeListener(subscription);
  }
}

export const notificationService = new NotificationService();
