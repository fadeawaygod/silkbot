import axiosProxy from './ChatbotAPIHttpClient';


export function getNotificationHistory(isChecked, skip, limit, type, startTime, endTime) {
  let params = { is_checked: isChecked, skip, limit }
  if (type && type !== 'all') {
    params.event = type
  }
  if (startTime) {
    params.start_time = startTime
  }
  if (endTime) {
    params.end_time = endTime
  }

  return axiosProxy({
    url: '/notification_history',
    method: 'get',
    params: params
  })
};
export function updateNotificationHistory(notification) {
  let data = { is_checked: notification.is_checked }
  return axiosProxy({
    url: `/notification_history/${notification.id}`,
    method: 'patch',
    data
  })
};
