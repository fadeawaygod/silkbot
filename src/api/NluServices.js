import axiosProxy from './ChatbotAPIHttpClient';

export function analyzeUtterance(nluToken, queryString, threshold) {
  let params = {
    "nlu_token": nluToken,
    "query_string": queryString,
    threshold
  }

  return axiosProxy({
    url: '/nlu_services/analyze_utterance',
    method: 'get',
    params
  })
}

export function findNluEndpoint(token_id, accessors) {
  let params = { token_id, accessors }
  return axiosProxy({
    url: '/find_endpoint',
    method: 'get',
    params: params
  })
};

export function getNluEndpoints() {
  return axiosProxy({
    url: '/endpoint',
    method: 'get'
  })
}

export function createNluEndpoints(data) {
  return axiosProxy({
    url: '/endpoint',
    method: 'post',
    data
  })
}

export function updateNluEndpoints(data) {
  return axiosProxy({
    url: '/endpoint',
    method: 'patch',
    data
  })
}

export function deleteNluEndpoints(id) {
  return axiosProxy({
    url: '/endpoint',
    method: 'delete',
    data: { id }
  })
}

export function getNluInfo() {
  return axiosProxy({
    url: '/nlu_info',
    method: 'get'
  })
}