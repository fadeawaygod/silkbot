import axiosProxy from './ChatbotAPIHttpClient';

export function getIntegrations(botID) {
  return axiosProxy({
    url: '/integrations',
    method: 'get',
    params: { bot_id: botID }
  })
};

export function createIntegration(integration) {
  let data = {}
  if (integration.bot_id) data["bot_id"] = integration.bot_id
  if (integration.credential) data["credential"] = integration.credential
  if (integration.passage) data["passage"] = integration.passage
  return axiosProxy({
    url: `/integrations`,
    method: 'post',
    data
  })
}
export function updateIntegration(integration) {
  let data = {}
  if (integration.credential) data["credential"] = integration.credential
  if (integration.is_enable !== undefined) data["is_enable"] = integration.is_enable
  return axiosProxy({
    url: `/integrations/${integration.id}`,
    method: 'patch',
    data
  })
}