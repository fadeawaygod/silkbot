import axiosProxy from './ChatbotAPIHttpClient';

export function getCSAuth(botID, role, skip, limit) {
  let params = {
    role
  }
  if (skip !== undefined) {
    params.skip = skip
  }
  if (limit !== undefined) {
    params.limit = limit
  }
  return axiosProxy({
    url: `/bots/${botID}/cs_auth`,
    method: 'get',
    params: params
  })
};

export function updateCSAuth(botID, uid, name, password) {
  let data = {
    uid: uid,
    name: name,
  }
  if (password && password !== '') {
    data.password = password
  }
  return axiosProxy({
    url: `/bots/${botID}/cs_auth`,
    method: 'patch',
    data
  })
}