import axiosProxy from './ChatbotAPIHttpClient';

const initialBotConfig = {
  "agent": {
    "chinese_convert": "none"
  }
}

export function getBots(skip, limit, isOwner, keyword = '', show_len = 'false') {
  let params = { skip, limit, show_len, is_owner: isOwner }
  if (keyword) params.keyword = keyword
  return axiosProxy({
    url: '/bots',
    method: 'get',
    params: params
  })
};

export function getBot(bot) {
  return axiosProxy({
    url: `/bots/${bot.id}`,
    method: 'get'
  })
};

export function createBot(bot) {
  return axiosProxy({
    url: '/bots',
    method: 'post',
    data: {
      "name": bot.name,
      "description": bot.description,
      "config": initialBotConfig
    }
  })
}

export function removeBot(bot) {
  return axiosProxy({
    url: `/bots/${bot.id}`,
    method: 'delete'
  })
};

export function updateBot(bot) {
  let data = {}
  if (bot.name) data["name"] = bot.name;
  data["description"] = bot.description;
  if (bot.config) data["config"] = bot.config;
  if (bot.accessors) data["accessors"] = bot.accessors;
  if (bot.intent_threshold) data["intent_threshold"] = bot.intent_threshold;
  if (bot.nlu_endpoint) data["nlu_endpoint"] = bot.nlu_endpoint;

  return axiosProxy({
    url: `/bots/${bot.id}`,
    method: 'patch',
    data
  })
}

export function setUsingDefaultNLU(botId, use_fixed_nlu) {
  let data = { use_fixed_nlu }
  return axiosProxy({
    url: `/bots/${botId}`,
    method: 'patch',
    data
  })
}

export function getFlow(botId) {
  return axiosProxy({
    url: `/flow?bot_id=${botId}`,
    method: 'get'
  })
}

export function createFlow(botId) {
  let data = {
    bot_id: botId
  };
  return axiosProxy({
    url: `/flow`,
    method: 'post',
    data
  })
}

export function updateFlow(flowId, data) {
  console.log("updateFlow#flowId", flowId)
  console.log("updateFlow#data", data)
  return axiosProxy({
    url: `/flow/${flowId}`,
    method: 'patch',
    data
  })
}

export function deleteFlow(flowId) {
  return axiosProxy({
    url: `/flow/${flowId}`,
    method: 'delete'
  })
}

export function importBot(botId, data) {
  return axiosProxy({
    url: `/bots/${botId}/import`,
    method: 'post',
    data
  })
}

export function exportBot(botId) {
  return axiosProxy({
    url: `/bots/${botId}/export`,
    method: 'get'
  })
}

export function testFlow(botId, flowDiagram, flowConfig) {
  let data = {
    flow: flowDiagram,
    config: flowConfig
  };
  return axiosProxy({
    url: `/config_test/${botId}`,
    method: 'post',
    data
  })
}

export function getFlowHistory(botId, skip, limit) {
  let params = { skip, limit }

  return axiosProxy({
    url: `/config_test/${botId}`,
    method: 'get',
    params: params
  }).then(response => {
    if (response.status === "ok") {
      return Promise.resolve(response);
    } else {
      return Promise.resolve([]);
    }
  }).catch(err => {
    return Promise.resolve([]);
  })
}

export function removeFlowHistory(botId, flowHistoryId) {
  return axiosProxy({
    url: `/config_test/${botId}`,
    method: 'delete',
    data: { id: flowHistoryId }
  }).then(response => {
    if (response.status === "ok") {
      return Promise.resolve(true);
    } else {
      return Promise.resolve(false);
    }
  }).catch(err => {
    return Promise.resolve(false);
  })
}