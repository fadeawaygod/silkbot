import axiosProxy from './ChatbotAPIHttpClient';

export function deploy(botID, configHistoryData, nluUrl = "", nluToken = "", skipNlu = false) {
  let data = { "bot_id": botID, "config_history_data": configHistoryData }
  if (nluUrl !== "") {
    data.custom_nlu_url = nluUrl
  };
  if (nluToken !== "") {
    data.custom_nlu_token = nluToken
  };
  if (skipNlu !== "") {
    data.skip_nlu = skipNlu
  };
  return axiosProxy({
    url: '/deployment',
    method: 'post',
    data
  })
}









