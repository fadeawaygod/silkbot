import axios from 'axios'
import { getUser } from './Auth';
import { getConfig } from '../settings';
import store from '@/store';

const axiosProxy = axios.create({
  baseURL: `${getConfig('fut').hostHttpProtocol}://${getConfig('fut').host}/api/v1/editor/`,
})
axiosProxy.defaults.timeout = 30000;
axiosProxy.defaults.headers.post['Content-Type'] = 'application/json';
axiosProxy.defaults.headers.patch['Content-Type'] = 'application/json';

axiosProxy.interceptors.request.use(request => {
  request.headers.Authorization = `Bearer ${getUser().access_token}`
  return request
}, function (error) {
  return Promise.reject(error);
});
axiosProxy.interceptors.response.use(
  response => {
    if (response.data.status != "ok")
      throw response.data.error

    return response.data
  },
  error => {
    const status = ((error || {}).response || {}).status;//HttpStatusCode
    console.dev(error.response);
    if (status === 401) {
      store.commit("cache/SET_IS_LOGIN", false);
      store.commit("SET_HTTP_STATUS_CODE", status);
    }
    else if (status === 403) {
      store.commit("SET_HTTP_STATUS_CODE", status);
    }
    return Promise.reject(error)
  }
)

export default axiosProxy