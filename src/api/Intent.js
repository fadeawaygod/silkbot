import axiosProxy from './ChatbotAPIHttpClient';
const DEFAULT_PRIORITY = 10;

export function getIntents(botId, skip, limit, keyword = '', show_len = 'false', minUtterance = 0, maxUtterance = 0) {
  let params = { bot_id: botId }

  if (skip) params.skip = skip
  if (limit) params.limit = limit
  if (show_len) params.show_len = show_len
  if (keyword) params.keyword = keyword
  if (minUtterance > 0) {
    params.min_utterance = minUtterance
  }
  if (maxUtterance > 0) {
    params.max_utterance = maxUtterance
  }
  return axiosProxy({
    url: '/intents',
    method: 'get',
    params: params
  })
};
export function getIntent(intentId) {
  return axiosProxy({
    url: `/intents/${intentId}`,
    method: 'get',
  })
};

export function createIntent(botID, intentName) {
  return axiosProxy({
    url: '/intents',
    method: 'post',
    data: {
      "name": intentName,
      "bot_id": botID,
      "priority": DEFAULT_PRIORITY
    }
  })
}

export function removeIntent(intent) {
  return axiosProxy({
    url: `/intents/${intent.id}`,
    method: 'delete'
  })
};

export function updateIntent(intent) {
  let data = {}
  if (intent.name) data["name"] = intent.name
  if (intent.priority) data["priority"] = intent.priority
  if (intent.entities) data["entities"] = intent.entities
  return axiosProxy({
    url: `/intents/${intent.id}`,
    method: 'patch',
    data
  })
}

export function importIntents(botId, data) {
  return axiosProxy({
    url: `/bots/${botId}/import_intents`,
    method: 'post',
    headers: { 'Content-Type': 'text/tab-separated-values' },
    data
  })
}





