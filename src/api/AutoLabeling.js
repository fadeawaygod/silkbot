import axiosProxy from './ChatbotAPIHttpClient';

export function autoLabelUtterance(botId, utteranceIds) {
  return axiosProxy({
    url: '/auto_labeling',
    method: 'post',
    data: {
      bot_id: botId,
      utterance_ids: utteranceIds
    }
  })
}
