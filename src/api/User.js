import { getUser } from './Auth';
import axiosProxy from './ChatbotAPIHttpClient';

export function getSelf() {
  const user = getUser();
  return axiosProxy({
    url: `/users/${user.uid}`,
    method: 'get',
  })
};


export function getUsers(group, role, skip, limit, keyword = '', show_len = false) {
  let params = { role, skip, limit }
  if (group) params.group = group
  if (role) params.role = role
  if (keyword) params.keyword = keyword
  if (show_len) params.show_len = 'true'
  return axiosProxy({
    url: '/users',
    method: 'get',
    params: params,
  })
};

export function createUser(user) {
  return axiosProxy({
    url: '/users',
    method: 'post',
    data: {
      "uid": user.uid,
      "password": user.password,
      "name": user.name,
      "nick_name": user.nick_name,
      "mail_address": user.mail_address,
      "role": user.role,
      "group": user.group,
      "phone": user.phone,
      "avatar_url": user.avatar_url
    }
  })
}

export function removeUser(user) {
  console.log(user)
  return axiosProxy({
    url: `/users/${user.uid}`,
    method: 'delete'
  })
};


export function updateUser(user) {
  let data = {}
  if (user.password && user.password !== "") data["password"] = user.password
  if (user.name) data["name"] = user.name
  if (user.nick_name) data["nick_name"] = user.nick_name
  if (user.mail_address) data["mail_address"] = user.mail_address
  if (user.role) data["role"] = user.role
  if (user.group) data["group"] = user.group
  if (user.phone) data["phone"] = user.phone
  if (user.avatar_url) data["avatar_url"] = user.avatar_url

  return axiosProxy({
    url: `/users/${user.uid}`,
    method: 'patch',
    data
  })
}