import axiosProxy from './ChatbotAPIHttpClient';

export function getEntities(botId, skip, limit, isSystem, keyword = '', show_len = 'false') {
  let params = { bot_id: botId, skip, limit, show_len }
  if (keyword) params.keyword = keyword
  if (isSystem != undefined) {
    params.is_sys = isSystem
  }
  return axiosProxy({
    url: '/entities',
    method: 'get',
    params: params
  })
};

export function createEntity(botID, entityName) {
  return axiosProxy({
    url: '/entities',
    method: 'post',
    data: {
      "bot_id": botID,
      "name": entityName
    }
  })
}

export function removeEntity(entity) {
  return axiosProxy({
    url: `/entities/${entity.id}`,
    method: 'delete'
  })
};

export function updateEntity(entity) {
  let data = {}
  if (entity.name) data["name"] = entity.name
  if (entity.entries) data["entries"] = entity.entries
  if (entity.prompts) data["prompts"] = entity.prompts
  if (entity.is_enabled !== undefined) data["is_enabled"] = entity.is_enabled
  return axiosProxy({
    url: `/entities/${entity.id}`,
    method: 'patch',
    data
  })
}

export function importEntities(botId, data) {
  return axiosProxy({
    url: `/bots/${botId}/import_entities`,
    method: 'post',
    data
  })
}