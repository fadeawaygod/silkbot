import axiosProxy from './ChatbotAPIHttpClient';

export function importDialog(botId, data) {
  return axiosProxy({
    url: `/bots/${botId}/import_dialog`,
    method: 'post',
    data
  })
}
export function getDialogs(botId, skip, limit, sortBy, isAsc, status, minCommentCount, keyword) {
  let params = { bot_id: botId, skip, limit, show_len: true }
  if (sortBy) params.sort_by = sortBy;
  if (isAsc) params.is_asc = isAsc;
  if (status) params.status = status;
  if (minCommentCount > 0) params.min_comment = minCommentCount;
  if (keyword && keyword !== "") params.keyword = keyword;

  return axiosProxy({
    url: `/dialogs`,
    method: 'get',
    params: params
  })
}

export function getDialog(dialogId) {
  return axiosProxy({
    url: `/dialogs/${dialogId}`,
    method: 'get',
  })
}

export function exportDialogs(botId) {
  return axiosProxy({
    url: `/bots/${botId}/export_dialog`,
    method: 'get'
  })
}

export function updateDialog(dialog) {
  let data = {}
  if (dialog.name) data["name"] = dialog.name
  if (dialog.status) data["status"] = dialog.status
  if (dialog.labeler) data["labeler"] = dialog.labeler
  if (dialog.utterances) data["utterances"] = dialog.utterances
  if (dialog.start_labeling_time) data["start_labeling_time"] = dialog.start_labeling_time
  if (dialog.labeling_total_time) data["labeling_total_time"] = dialog.labeling_total_time
  if (dialog.comment_count != undefined) data["comment_count"] = dialog.comment_count
  return axiosProxy({
    url: `/dialogs/${dialog.id}`,
    method: 'patch',
    data
  })
}


