import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
Vue.config.productionTip = false;
//base
import base from '@u/base';
//vuera
import { VuePlugin } from 'vuera';
Vue.use(VuePlugin);
//element ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
//UI
import '@/icons'; // icon
import '@/styles/index.scss'; // global css
//mixins
import alert from '@mix/alert';
//i18n
import i18n from "@/lang";
Vue.prototype.$tp = function () {
  if (Array.isArray(arguments[0]))
    return arguments[0].map(x => {
      if (!Array.isArray(x)) x = [x];
      return this.$tp(...x)
    }).join('');
  else
    return this.$t(...arguments).replace(/\{(.(?!\}))+.\}/gm, '');
}
//VueDragscroll
import VueDragscroll from 'vue-dragscroll';
Vue.use(VueDragscroll);
//vue-clipboard2
import VueClipboard from 'vue-clipboard2';
Vue.use(VueClipboard);

import VueApexCharts from 'vue-apexcharts'
Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)
//notificationService
import { notificationService } from "@/api/NotificationService";
notificationService.init(() => { store.state.notificationServiceReady = true; });
//vue id
Vue.prototype.$id = "";
//css
import "@css/style.less";
import "@css/layout.less";
new Vue({
  mixins: [alert],
  data: {
    publicList: [],
    notificationService
  },
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
