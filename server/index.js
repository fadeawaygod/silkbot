const express = require('express');
const app = express();
const router = express.Router();
const http = require("http");

app.use(express.static("./dist"))
app.get("*", function (req, res) {
    res.sendfile(__dirname + '/dist/index.html');
})

const httpServer = http.createServer(app);
const server = httpServer.listen(8080)
console.log("listening... 8080");
