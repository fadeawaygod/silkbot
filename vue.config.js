const path = require('path');
const webpack = require('webpack');
const GoogleFontsPlugin = require("@beyonk/google-fonts-webpack-plugin")

function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  productionSourceMap: false,
  css: {
    loaderOptions: {
      postcss: {
        sourceMap: process.env.NODE_ENV === 'development'
      }
    }
  },
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        moment: 'moment',
        $: 'jquery'
      }),
      new GoogleFontsPlugin({
        fonts: [
          { family: "Noto Sans TC" },
        ]
      }),
      new webpack.DefinePlugin({
        ENV_IS_DEV: process.env.NODE_ENV === 'development'
      })
    ],
    resolve: {
      alias: {
        '@c': '@/components',
        '@css': '@/assets/css',
        '@img': '@/assets/img',
        '@js': '@/assets/js',
        '@mix': '@/mixins',
        '@u': '@/utils',
        '@v': '@/views',
        '@ui': '@/components/ui',
        '@w': '@/watchOnce',
      }
    }
  },
  chainWebpack(config) {
    config.devtool = 'eval';
    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()
  }
}