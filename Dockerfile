FROM node:12.13-alpine

LABEL Name=SilkbotClient
EXPOSE 8080

WORKDIR /src
COPY . /src

RUN apk add --no-cache git

RUN yarn install && yarn cache clean
RUN . ./version.sh
RUN yarn build
RUN mkdir -p /app/dist

RUN cp -r dist /app
COPY server/package.json /app/package.json
COPY server/index.js /app/index.js

WORKDIR /app
RUN rm -rf /src
RUN yarn install && yarn cache clean

CMD ["node", "index.js"]
